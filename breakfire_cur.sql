-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2020 at 07:14 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `breakfire_cur`
--

-- --------------------------------------------------------

--
-- Table structure for table `ac_installations`
--

CREATE TABLE `ac_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `amc`
--

CREATE TABLE `amc` (
  `id` int(10) UNSIGNED NOT NULL,
  `amc_cost` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `amc`
--

INSERT INTO `amc` (`id`, `amc_cost`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 500, 1, NULL, NULL, NULL),
(2, 1000, 1, NULL, NULL, NULL),
(3, 1500, 1, NULL, NULL, NULL),
(4, 2000, 1, NULL, NULL, NULL),
(5, 2500, 1, NULL, NULL, NULL),
(6, 3000, 1, NULL, NULL, NULL),
(7, 3500, 1, NULL, NULL, NULL),
(8, 4000, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `approve_drawings`
--

CREATE TABLE `approve_drawings` (
  `id` int(10) UNSIGNED NOT NULL,
  `jobcodeid` int(11) NOT NULL,
  `project_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `approve_drawings`
--

INSERT INTO `approve_drawings` (`id`, `jobcodeid`, `project_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'dgdfg', '2020-09-01 13:17:14', '2020-09-01 13:17:14', NULL),
(2, 5, 'DFGD4535', '2020-09-01 13:34:34', '2020-09-01 13:34:34', NULL),
(3, 5, 'DDF754', '2020-09-01 13:34:34', '2020-09-01 13:37:52', NULL),
(4, 5, 'dgdfg', '2020-09-05 10:02:02', '2020-09-05 10:02:02', NULL),
(5, 5, 'PK7854', '2020-09-06 01:53:23', '2020-09-06 01:53:23', NULL),
(6, 6, 'BF-PJ-121444', '2020-10-08 12:31:48', '2020-10-08 12:31:48', NULL),
(7, 7, 'BF-PJ-12211165', '2020-10-08 12:32:41', '2020-10-08 12:32:41', NULL),
(8, 8, 'BF-PJ-1232225', '2020-10-08 12:33:21', '2020-10-08 12:33:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `approve_drawing_details`
--

CREATE TABLE `approve_drawing_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `approve_drawing_id` int(11) NOT NULL,
  `from_dtd` date NOT NULL,
  `to_dtd` date DEFAULT NULL,
  `status_msg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `approve_drawing_details`
--

INSERT INTO `approve_drawing_details` (`id`, `approve_drawing_id`, `from_dtd`, `to_dtd`, `status_msg`, `status_percent`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2020-08-31', '2020-09-01', 'content content content content', '45', '2020-09-01 13:17:14', '2020-09-01 15:12:49', NULL),
(2, 1, '2020-09-01', '2020-10-22', 'text text text text', '79', '2020-09-01 13:17:14', '2020-09-01 15:12:49', NULL),
(3, 2, '2020-09-03', '2020-09-04', 'Loren Ipsum Loren Ipsum Loren Ipsum', '43', '2020-09-01 13:34:34', '2020-09-01 13:34:34', NULL),
(4, 3, '2020-09-03', '2020-09-04', 'Loren Ipsum Loren Ipsum Loren Ipsum', '43', '2020-09-01 13:34:34', '2020-09-05 09:18:46', NULL),
(5, 1, '2020-09-30', '2020-10-01', '6778978', '66', '2020-09-01 14:36:21', '2020-09-01 15:12:49', NULL),
(6, 1, '2020-09-16', '2020-09-17', 'fdgdfg', '56', '2020-09-01 15:12:49', '2020-09-01 15:12:49', NULL),
(7, 3, '2020-09-05', '2020-09-18', '6778978', '66', '2020-09-05 09:18:46', '2020-09-05 09:18:46', NULL),
(8, 4, '2020-09-11', '2020-09-24', 'asd dfg ghjk', '34', '2020-09-05 10:02:02', '2020-09-05 10:02:02', NULL),
(9, 5, '2020-09-06', '2020-09-12', 'content content content content', '10', '2020-09-06 01:53:23', '2020-09-06 01:53:23', NULL),
(10, 6, '2020-10-08', '2020-10-14', 'asd dfg ghjk', '45', '2020-10-08 12:31:48', '2020-10-08 12:31:48', NULL),
(11, 7, '2020-10-08', '2020-10-15', 'bffhfggf', '50', '2020-10-08 12:32:41', '2020-10-08 12:32:41', NULL),
(12, 8, '2020-10-15', '2020-11-03', 'dfgdf', '34', '2020-10-08 12:33:21', '2020-10-08 12:33:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2020-10-05 18:08:42', '2020-10-05 18:08:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `custid` int(11) NOT NULL,
  `cust_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cust_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_name` int(11) DEFAULT NULL,
  `service_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `boqdetails`
--

CREATE TABLE `boqdetails` (
  `id` int(10) UNSIGNED NOT NULL,
  `ecnumber` int(11) DEFAULT NULL,
  `boq_type` tinyint(4) DEFAULT NULL COMMENT '1:Enquiry, 2:COmplain',
  `sales_representive_id` int(2) NOT NULL DEFAULT 0,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quotation_dtd` date DEFAULT NULL,
  `company_project_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_code_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quotation_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boq_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pump_capacity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `service_type_details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_projects` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emirates` int(11) NOT NULL DEFAULT 0,
  `emiratescity` int(11) NOT NULL DEFAULT 0,
  `sales_representive_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boq_visit_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boq_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `material_cost` decimal(10,2) NOT NULL DEFAULT 0.00,
  `labour_cost` decimal(10,2) DEFAULT 0.00,
  `amc` decimal(10,2) DEFAULT 0.00,
  `total_cost` decimal(10,2) DEFAULT 0.00,
  `profit` decimal(10,2) DEFAULT 0.00,
  `profit_amt` decimal(10,2) NOT NULL DEFAULT 0.00,
  `quotation_price` decimal(10,2) DEFAULT 0.00,
  `need_discount` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1:Yes, 0:No',
  `discount_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vat_percent` decimal(10,2) NOT NULL DEFAULT 0.00,
  `vat_amt` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_invoice` decimal(10,2) NOT NULL DEFAULT 0.00,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1: BOQ Submitted, 2: BOQ Reviewed, 3: Approved, 4: BOQ Rejected, 5: Payment Received',
  `invoice_sent` tinyint(4) NOT NULL DEFAULT 0,
  `boq_createdby` int(11) DEFAULT NULL,
  `boq_created_dtd` datetime DEFAULT NULL,
  `boq_reviewedby` int(11) DEFAULT NULL,
  `boq_reviewed_dtd` datetime DEFAULT NULL,
  `boq_approvedby` int(11) DEFAULT NULL,
  `boq_approved_dtd` datetime DEFAULT NULL,
  `boq_rejectedby` int(11) DEFAULT NULL,
  `boq_rejected_dtd` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boqdetails`
--

INSERT INTO `boqdetails` (`id`, `ecnumber`, `boq_type`, `sales_representive_id`, `contact_person`, `quotation_dtd`, `company_project_name`, `contact_person_name`, `contact_person_phone`, `site_info`, `job_code_no`, `quotation_no`, `boq_email`, `pump_capacity`, `service_type`, `service_type_details`, `location_projects`, `emirates`, `emiratescity`, `sales_representive_name`, `boq_visit_type`, `boq_details`, `comments`, `material_cost`, `labour_cost`, `amc`, `total_cost`, `profit`, `profit_amt`, `quotation_price`, `need_discount`, `discount_price`, `vat_percent`, `vat_amt`, `total_invoice`, `status`, `invoice_sent`, `boq_createdby`, `boq_created_dtd`, `boq_reviewedby`, `boq_reviewed_dtd`, `boq_approvedby`, `boq_approved_dtd`, `boq_rejectedby`, `boq_rejected_dtd`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 11, 'SK Gon', '2020-10-04', 'UKHUB LTS', NULL, '971559007972', 'NEW INSTALLATION OF PRODUCT', 'BF-BOQ-11', NULL, 'tester@admin.com', 'UAE, Ajan FIRE AREA', 1, 'Supply', 'UAE Play Grv', 2, 128, 'Estimator SK1', 'project_estimation', NULL, NULL, '792.00', '500.00', '0.00', '792.00', '40.00', '316.80', '1108.80', 1, '0.00', '5.00', '55.44', '1164.24', 3, 0, 11, '2020-10-04 07:05:38', NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-04 01:38:36', '2020-10-04 01:38:36', NULL),
(2, 0, 0, 3, 'SYED', '2020-10-05', 'TEST COMP LTD', NULL, '971559007972', 'TEST PD', 'BF-BOQ-12', NULL, 'syed@admin.com', NULL, 1, 'Supply', 'UAE S', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 0, 3, '2020-10-05 22:06:48', 3, '2020-10-05 22:06:48', 3, '2020-10-05 22:06:48', NULL, NULL, '2020-10-05 16:36:48', '2020-10-05 16:36:48', NULL),
(3, 0, 0, 3, 'SYED', '2020-10-05', 'TEST COMP LTD', NULL, '971559007972', 'TEST PD', 'BF-BOQ-13', NULL, 'syed@admin.com', NULL, 1, 'Supply', 'UAE S', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 0, 3, '2020-10-05 22:09:07', 3, '2020-10-05 22:09:07', 3, '2020-10-05 22:09:07', NULL, NULL, '2020-10-05 16:39:07', '2020-10-05 16:39:07', NULL),
(4, 0, 0, 3, 'SYED', '2020-10-05', 'TEST COMP LTD', NULL, '971559007972', 'TEST PD', 'BF-BOQ-14', NULL, 'syed@admin.com', NULL, 1, 'Supply', 'UAE S', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 0, 3, '2020-10-05 22:16:42', 3, '2020-10-05 22:16:42', 3, '2020-10-05 22:16:42', NULL, NULL, '2020-10-05 16:46:42', '2020-10-05 16:46:42', NULL),
(5, 0, 0, 3, 'SYED', '2020-10-05', 'TEST COMP LTD', NULL, '971559007972', 'TEST PD', 'BF-BOQ-15', NULL, 'syed@admin.com', NULL, 1, 'Supply', 'UAE S', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 0, 3, '2020-10-05 22:17:13', 3, '2020-10-05 22:17:13', 3, '2020-10-05 22:17:13', NULL, NULL, '2020-10-05 16:47:13', '2020-10-05 16:47:13', NULL),
(6, 0, 0, 3, 'SYED', '2020-10-05', 'TEST COMP LTD', NULL, '971559007972', 'TEST PD', 'BF-BOQ-16', 'BF-BOQ-16', 'syed@admin.com', NULL, 1, 'Supply', 'UAE S', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 1, 3, '2020-10-05 22:19:29', 3, '2020-10-05 22:19:29', 3, '2020-10-05 22:19:29', NULL, NULL, '2020-10-05 16:49:29', '2020-10-05 16:49:45', NULL),
(7, 0, 0, 3, 'SYED', '2020-10-05', 'TEST COMP LTD', NULL, '971559007972', 'TEST PD', 'BF-BOQ-17', 'BF-BOQ-17', 'syed@admin.com', NULL, 1, 'Supply', 'UAE S', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 1, 3, '2020-10-05 22:21:12', 3, '2020-10-05 22:21:12', 3, '2020-10-05 22:21:12', NULL, NULL, '2020-10-05 16:51:12', '2020-10-05 16:51:23', NULL),
(8, 0, 0, 3, 'SYED', '2020-10-05', 'TEST COMP LTD', NULL, '971559007972', 'TEST PD', 'BF-BOQ-18', 'BF-BOQ-18', 'syed@admin.com', NULL, 1, 'Supply', 'UAE S', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 1, 3, '2020-10-05 22:23:27', 3, '2020-10-05 22:23:27', 3, '2020-10-05 22:23:27', NULL, NULL, '2020-10-05 16:53:27', '2020-10-05 16:53:37', NULL),
(9, 0, 0, 3, 'SYED', '2020-10-05', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-19', NULL, 'tester@admin.com', NULL, 1, 'Supply', 'required', 2, 139, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 1, '0.00', '5.00', '0.00', '0.00', 1, 0, 3, '2020-10-05 22:23:44', NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-05 17:01:47', '2020-10-05 17:01:47', NULL),
(10, 0, 0, 3, 'SYed', '2020-10-05', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-20', NULL, 'tester@admin.com', NULL, 1, 'Supply', 'UAE Play Gr', 2, 0, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 1, '0.00', '5.00', '0.00', '0.00', 1, 0, 3, '2020-10-05 22:32:04', NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-05 17:07:39', '2020-10-05 17:07:39', NULL),
(11, 0, 0, 3, 'SYED', '2020-10-05', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-21', NULL, 'tester@admin.com', NULL, 1, 'Supply', 'required', 2, 140, 'Mang. Syed', 'project_estimation', NULL, NULL, '2.00', '500.00', '0.00', '2.00', '40.00', '0.80', '2.80', 1, '0.00', '5.00', '0.14', '2.94', 1, 0, 3, '2020-10-05 22:43:46', NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-05 17:14:12', '2020-10-05 17:14:12', NULL),
(12, 0, 0, 3, 'SYED', '2020-10-05', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-22', 'BF-BOQ-22', 'tester@admin.com', NULL, 1, 'Supply', 'required', 2, 127, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 0, '0.00', '5.00', '0.00', '0.00', 3, 1, 3, '2020-10-05 22:45:12', 3, '2020-10-05 22:45:12', 3, '2020-10-05 22:45:12', NULL, NULL, '2020-10-05 17:15:12', '2020-10-05 17:15:23', NULL),
(13, 0, 0, 3, 'SYED', '2020-10-05', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-23', NULL, 'tester@admin.com', NULL, 1, 'Supply', 'required', 2, 0, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 1, '0.00', '5.00', '0.00', '0.00', 1, 0, 3, '2020-10-05 22:45:30', NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-05 17:23:32', '2020-10-05 17:23:32', NULL),
(14, 0, 0, 3, 'SYED', '2020-10-05', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-24', NULL, 'tester@admin.com', NULL, 1, 'Supply', 'vhghjgjh', 2, 138, 'Mang. Syed', 'project_estimation', NULL, NULL, '0.00', '500.00', '0.00', '0.00', '40.00', '0.00', '0.00', 1, '0.00', '5.00', '0.00', '0.00', 1, 0, 3, '2020-10-05 22:53:44', NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-05 17:25:19', '2020-10-05 17:25:19', NULL),
(15, 0, 0, 3, 'SYed', '2020-10-07', 'UKHUB', NULL, '1591591591', 'WER456789', 'BF-BOQ-25', 'BF-BOQ-25', 'tester@admin.com', NULL, 1, 'Supply', 'gdfgdf', 2, 145, 'Mang. Syed', 'project_estimation', NULL, NULL, '6.00', '500.00', '0.00', '6.00', '40.00', '2.40', '8.40', 0, '0.00', '5.00', '0.42', '8.82', 3, 1, 3, '2020-10-07 03:08:28', 3, '2020-10-07 03:08:28', 3, '2020-10-07 03:08:28', NULL, NULL, '2020-10-06 21:38:28', '2020-10-06 21:42:03', NULL),
(16, 8, 2, 3, 'SYed', '2020-10-08', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-26', 'BF-BOQ-26', 'tester@admin.com', 'UAE', 1, 'Supply', 'UAE', 2, 126, 'Mang. Syed', 'project_estimation', NULL, NULL, '1.00', '500.00', '0.00', '1.00', '40.00', '0.40', '1.40', 0, '0.00', '5.00', '0.07', '1.47', 3, 1, 3, '2020-10-08 16:02:48', 3, '2020-10-08 16:02:48', 3, '2020-10-08 16:02:48', NULL, NULL, '2020-10-08 10:32:48', '2020-10-08 10:33:10', NULL),
(17, 0, 0, 3, 'SYED', '2020-10-08', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-27', 'BF-BOQ-27', 'tester@admin.com', 'UAE', 1, 'Supply', 'UAE Play Gr', 2, 0, 'Mang. Syed', 'project_estimation', NULL, NULL, '2.00', '500.00', '0.00', '2.00', '40.00', '0.80', '2.80', 0, '0.00', '5.00', '0.14', '2.94', 3, 1, 3, '2020-10-08 18:52:24', 3, '2020-10-08 18:52:24', 3, '2020-10-08 18:52:24', NULL, NULL, '2020-10-08 13:22:24', '2020-10-08 13:22:48', NULL),
(18, 0, 0, 3, 'SYED', '2020-10-09', 'UKHUB', NULL, '1591591591', 'sdfsdf', 'BF-BOQ-28', 'BF-BOQ-28', 'tester@admin.com', 'UAE', 1, 'Supply', 'UAE Play Gr', 2, 0, 'Mang. Syed', 'project_estimation', NULL, 'cbfghjhk', '6.00', '500.00', '0.00', '6.00', '40.00', '2.40', '8.40', 0, '0.00', '5.00', '0.42', '8.82', 3, 1, 3, '2020-10-09 02:49:29', 3, '2020-10-09 02:49:29', 3, '2020-10-09 02:49:29', NULL, NULL, '2020-10-08 21:19:29', '2020-10-08 21:19:54', NULL),
(19, 11, 2, 3, 'TESTER', '2020-10-13', 'UKHUB', NULL, '1591591591', 'TEST PD', 'BF-BOQ-29', 'BF-BOQ-29', 'tester@admin.com', NULL, 1, 'Supply', 'sfd ddffdg', 2, 0, 'Mang. Syed', 'project_estimation', NULL, NULL, '6.00', '1.00', '0.00', '56.00', '40.00', '22.40', '78.40', 0, '0.00', '5.00', '3.92', '82.32', 3, 1, 3, '2020-10-13 03:07:21', 3, '2020-10-13 03:07:21', 3, '2020-10-13 03:07:21', NULL, NULL, '2020-10-12 21:37:21', '2020-10-12 21:38:35', NULL),
(20, 11, 2, 3, 'TESTER', '2020-10-13', 'ggggg', NULL, '971555222446', 'gfgfgg', 'BF-BOQ-30', 'BF-BOQ-30', 'test@gmail.com', NULL, 1, 'Supply', 'sfd ddffdg', 2, 0, 'Mang. Syed', 'project_estimation', NULL, NULL, '4.00', '1.00', '0.00', '54.00', '40.00', '21.60', '75.60', 0, '0.00', '5.00', '3.78', '79.38', 3, 1, 3, '2020-10-13 03:12:02', 3, '2020-10-13 03:12:02', 3, '2020-10-13 03:12:02', NULL, NULL, '2020-10-12 21:42:02', '2020-10-12 21:42:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `boqdetailsprod`
--

CREATE TABLE `boqdetailsprod` (
  `id` int(10) UNSIGNED NOT NULL,
  `boqdetails_id` int(11) NOT NULL,
  `prod_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,0) NOT NULL DEFAULT 0,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveredqty` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boqdetailsprod`
--

INSERT INTO `boqdetailsprod` (`id`, `boqdetails_id`, `prod_id`, `price`, `service`, `new`, `deliveredqty`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '9', '3', '1', '1', 0, 1, '2020-10-04 01:38:36', '2020-10-04 01:38:36', NULL),
(2, 1, '12', '789', '1', '1', 0, 1, '2020-10-04 01:38:36', '2020-10-04 01:38:36', NULL),
(3, 11, '1', '2', '1', '1', 0, 1, '2020-10-05 17:14:12', '2020-10-05 17:14:12', NULL),
(4, 15, '1', '2', '1', '1', 0, 1, '2020-10-06 21:38:29', '2020-10-06 21:38:29', NULL),
(5, 15, '3', '4', '1', '1', 0, 1, '2020-10-06 21:38:29', '2020-10-06 21:38:29', NULL),
(6, 16, '11', '1', '1', '1', 0, 1, '2020-10-08 10:32:49', '2020-10-08 10:32:49', NULL),
(7, 17, '1', '2', '1', '1', 0, 1, '2020-10-08 13:22:25', '2020-10-08 13:22:25', NULL),
(8, 18, '1', '2', '1', '1', 0, 1, '2020-10-08 21:19:30', '2020-10-08 21:19:30', NULL),
(9, 18, '3', '4', '1', '1', 0, 1, '2020-10-08 21:19:30', '2020-10-08 21:19:30', NULL),
(10, 19, '1', '2', '1', '1', 0, 1, '2020-10-12 21:37:21', '2020-10-12 21:37:21', NULL),
(11, 19, '3', '4', '1', '1', 0, 1, '2020-10-12 21:37:21', '2020-10-12 21:37:21', NULL),
(12, 20, '4', '2', '1', '1', 0, 1, '2020-10-12 21:42:03', '2020-10-12 21:42:03', NULL),
(13, 20, '5', '2', '1', '1', 0, 1, '2020-10-12 21:42:03', '2020-10-12 21:42:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `boqdetailstype`
--

CREATE TABLE `boqdetailstype` (
  `id` int(10) UNSIGNED NOT NULL,
  `boqdetails_id` int(11) NOT NULL,
  `boqtype_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boqtype_msg` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boqtype_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boqdetailstype`
--

INSERT INTO `boqdetailstype` (`id`, `boqdetails_id`, `boqtype_id`, `boqtype_msg`, `boqtype_img`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '3', NULL, '', 1, '2020-10-04 01:38:36', '2020-10-04 01:38:36', NULL),
(2, 1, '4', NULL, '', 1, '2020-10-04 01:38:36', '2020-10-04 01:38:36', NULL),
(3, 2, '3', NULL, '', 1, '2020-10-05 16:36:48', '2020-10-05 16:36:48', NULL),
(4, 3, '3', NULL, '', 1, '2020-10-05 16:39:07', '2020-10-05 16:39:07', NULL),
(5, 4, '3', NULL, '', 1, '2020-10-05 16:46:42', '2020-10-05 16:46:42', NULL),
(6, 5, '3', NULL, '', 1, '2020-10-05 16:47:14', '2020-10-05 16:47:14', NULL),
(7, 6, '3', NULL, '', 1, '2020-10-05 16:49:29', '2020-10-05 16:49:29', NULL),
(8, 7, '3', NULL, '', 1, '2020-10-05 16:51:12', '2020-10-05 16:51:12', NULL),
(9, 8, '3', NULL, '', 1, '2020-10-05 16:53:27', '2020-10-05 16:53:27', NULL),
(10, 10, '3', NULL, '', 1, '2020-10-05 17:07:40', '2020-10-05 17:07:40', NULL),
(11, 10, '4', NULL, '', 1, '2020-10-05 17:07:40', '2020-10-05 17:07:40', NULL),
(12, 15, '3', NULL, '', 1, '2020-10-06 21:38:28', '2020-10-06 21:38:28', NULL),
(13, 15, '4', NULL, '', 1, '2020-10-06 21:38:29', '2020-10-06 21:38:29', NULL),
(14, 16, '3', 'fwerwe', '', 1, '2020-10-08 10:32:49', '2020-10-08 10:32:49', NULL),
(15, 16, '4', 'Maintanance', '', 1, '2020-10-08 10:32:49', '2020-10-08 10:32:49', NULL),
(16, 16, '5', 'werw', '', 1, '2020-10-08 10:32:49', '2020-10-08 10:32:49', NULL),
(17, 16, '6', 'cgfchfhfg', '', 1, '2020-10-08 10:32:49', '2020-10-08 10:32:49', NULL),
(18, 17, '3', NULL, '', 1, '2020-10-08 13:22:25', '2020-10-08 13:22:25', NULL),
(19, 17, '4', NULL, '', 1, '2020-10-08 13:22:25', '2020-10-08 13:22:25', NULL),
(20, 17, '5', NULL, '', 1, '2020-10-08 13:22:25', '2020-10-08 13:22:25', NULL),
(21, 17, '6', NULL, '', 1, '2020-10-08 13:22:25', '2020-10-08 13:22:25', NULL),
(22, 17, '7', NULL, '', 1, '2020-10-08 13:22:25', '2020-10-08 13:22:25', NULL),
(23, 17, '8', NULL, '', 1, '2020-10-08 13:22:25', '2020-10-08 13:22:25', NULL),
(24, 18, '14', 'cdsfsdf', '', 1, '2020-10-08 21:19:30', '2020-10-08 21:19:30', NULL),
(25, 18, '15', 'dfgfdgffhfgh', '', 1, '2020-10-08 21:19:30', '2020-10-08 21:19:30', NULL),
(26, 18, '16', NULL, '', 1, '2020-10-08 21:19:30', '2020-10-08 21:19:30', NULL),
(27, 19, '14', NULL, '', 1, '2020-10-12 21:37:21', '2020-10-12 21:37:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `boqtype`
--

CREATE TABLE `boqtype` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_eng` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_arabic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boqtype`
--

INSERT INTO `boqtype` (`id`, `name_eng`, `name_arabic`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Fire Pump', 'مضخة حريق', 1, NULL, NULL, NULL),
(4, 'Sprinkler System', 'نظام الرش', 1, NULL, NULL, NULL),
(5, 'Fire Alam System', 'نظام انذار الاحرائق', 1, NULL, NULL, NULL),
(6, 'Hydrant System', 'نظام صنبور', 1, NULL, NULL, NULL),
(7, 'Deluge System', 'نظام طوفان', 1, NULL, NULL, NULL),
(8, 'Foam System', 'نظام الرغوة', 1, NULL, NULL, NULL),
(9, 'Fire House Reel', 'بكرة بيت النار', 1, NULL, NULL, NULL),
(10, 'Fire Extinguisher', 'طفاية حريق', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Honda', 1, '2020-08-13 10:16:45', '2020-08-16 22:39:26', '2020-08-16 22:39:26'),
(4, 'AI Rayan', 1, '2020-08-13 10:18:56', '2020-08-13 12:31:50', NULL),
(5, 'Brand X', 1, '2020-08-16 22:08:05', '2020-08-16 22:08:05', NULL),
(6, 'Brand Y', 1, '2020-08-16 22:18:14', '2020-08-16 22:18:14', NULL),
(7, 'Brand Z', 1, '2020-08-16 22:38:52', '2020-08-16 22:38:52', NULL),
(8, 'Brand XZ', 1, '2020-08-18 21:24:17', '2020-08-18 21:24:17', NULL),
(9, 'Brand PK', 1, '2020-08-18 21:25:34', '2020-08-18 21:25:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `carrierno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carriers` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `carrierno`, `carriers`, `fname`, `lname`, `emailid`, `phone_number`, `message`, `department`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, 'Tester', 'bcvnvn', NULL, NULL, 'vbnvnbvn', NULL, '2020-09-21 19:59:50', '2020-09-21 19:59:50', NULL),
(2, NULL, NULL, 'Tester', 'zxcfxgf', NULL, NULL, 'xgddy', NULL, '2020-09-21 20:01:35', '2020-09-21 20:01:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(14, 'Fire Pump', 0, '2020-09-29 12:32:37', '2020-10-09 01:25:09', NULL),
(15, 'Sprinkler System', 0, '2020-09-29 12:32:48', '2020-10-09 01:26:23', NULL),
(16, 'Fire Alam System', 0, '2020-09-29 12:45:10', '2020-10-09 01:26:41', NULL),
(17, 'Hydrant System', 0, '2020-10-09 01:27:02', '2020-10-09 01:27:02', NULL),
(18, 'Deluge System', 0, '2020-10-09 01:34:03', '2020-10-09 01:34:03', NULL),
(19, 'Foam System', 0, '2020-10-09 01:35:00', '2020-10-09 01:35:00', NULL),
(20, 'Fire House Reel', 0, '2020-10-09 01:35:12', '2020-10-09 01:35:12', NULL),
(21, 'Fire Extinguisher', 0, '2020-10-09 01:35:22', '2020-10-09 01:35:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categoriesbk`
--

CREATE TABLE `categoriesbk` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categoriesbk`
--

INSERT INTO `categoriesbk` (`id`, `category_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CONTROL PANEL', 1, NULL, NULL, NULL),
(2, 'CON DEVICES', 1, NULL, NULL, NULL),
(3, 'ADD DEVICES', 1, NULL, NULL, NULL),
(4, 'FACP BATTERIES', 1, NULL, NULL, NULL),
(5, 'FIRE PUMP BATTERIES', 1, NULL, NULL, NULL),
(6, 'EMERGENCY LIGHT SYSTEM', 1, NULL, NULL, NULL),
(7, 'FIRE CABLES', 1, NULL, NULL, NULL),
(8, 'FIRE EXTINGUISHERS', 1, NULL, NULL, NULL),
(9, 'SPRINKLERS', 1, NULL, NULL, NULL),
(10, 'F.H.R/B.I /CABINETS & ACC', 1, NULL, NULL, NULL),
(11, 'VALVES & ACCE', 1, NULL, NULL, NULL),
(12, 'PUMP', 1, NULL, NULL, NULL),
(13, 'TTC', 0, '2020-09-27 03:21:06', '2020-09-27 03:21:06', NULL),
(14, 'TTCjgfgjhgkmk', 0, '2020-09-27 03:21:27', '2020-09-27 03:21:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cctv_installations`
--

CREATE TABLE `cctv_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `title`, `status`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Certificate 1', '1', NULL, '2020-08-22 00:47:55', '2020-08-22 00:47:55', NULL),
(2, 'Certificate 2', '1', NULL, '2020-08-22 00:48:20', '2020-08-22 00:48:20', NULL),
(3, 'Certificate 3', '1', NULL, '2020-08-22 00:48:46', '2020-08-22 00:48:46', NULL),
(4, 'Certificate 4', '1', NULL, '2020-08-22 00:49:07', '2020-08-22 00:49:07', NULL),
(5, 'Certificate 5', '1', NULL, '2020-08-22 00:49:27', '2020-08-22 00:49:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cleaning_installations`
--

CREATE TABLE `cleaning_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE `complains` (
  `id` int(10) UNSIGNED NOT NULL,
  `estimator_id` int(11) NOT NULL DEFAULT 0,
  `boq_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comp_no` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visitid` int(11) DEFAULT NULL,
  `cmp_date` datetime NOT NULL,
  `cmp_purpose` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_owner_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT 0,
  `service_type_details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `emirates` int(11) DEFAULT 0,
  `emiratescity` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contactuses`
--

CREATE TABLE `contactuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `cont_no` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactuses`
--

INSERT INTO `contactuses` (`id`, `cont_no`, `first_name`, `last_name`, `email`, `phone`, `attachments`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, NULL, 'Tester', 'fdgdgfg', 'tester@admin.com', '1591591591', NULL, 'ggj fh h fhh fg', '2020-10-01 23:08:52', '2020-10-01 23:08:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `content_management_systems`
--

CREATE TABLE `content_management_systems` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_management_systems`
--

INSERT INTO `content_management_systems` (`id`, `title`, `status`, `content`, `title_ar`, `content_ar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Chairman Message', '1', 'We BRAKE Fire as a specialized company owns professional and qualified cadres, and because of our feeling of responsibility and as a respond for the trust was given to us by accreditation our company by the civil Defense Department, we took care since launching our company to submit best services and solutions to contribute in implementing highest safety measures according to accreditation demands , terms and laws in order to achieve our desired target in protecting properties and souls of people , and to keep the community safe of dangers .<br><br>\r\n\r\nOur company BRAKE Fire Keeps us with growth and development in the United Arab Emirates in the fields of security and safety, and it took upon itself full commitment of performing and high quality jobs in an attractive environment looking for human being comfort and stability as a high priority which copes with local and international markets within studies and researches that contribute of achieving highest level of satisfaction of serving customers and to maintain of basis values and principles which some of them are :<br><br>\r\n\r\n<ul><li>Integrity responsibility and professionalism</li><li>Innovation and continuous technological development .</li><li>providing the community of distinguished and long term services for customers.</li></ul>', 'كلمة المدير العام', 'نحن  بريك فاير شركة متخصصة وذات كفاءات وكوادر مؤهلة  وشعوراً منا بالمسؤولية وتلبيةً للثقة التي مٌنحت لنا بإعتمادنا من قبل إدارة الدفاع المدني حرصنا منذ تأسيسنا على تقديم أفضل الحلول والخدمات لنساهم في تطبيق أعلى معايير السلامة طبقاً للقوانين والإشتراطات والمتطلبات المعتمدة لتحقيق الهدف المنشود في حماية الأرواح والممتلكات والمحافظة على مجتمع آمن من الأخطار .<br><br>\r\nلقد واكبت بريك فاير التطور والنماء في دولة الإمارات العربية المتحدة في مجالات الأمن والسلامة وأخذت على عاتقها الإلتزام التام بتحقيق أعمال ذات جودة عالية ومثالية في بيئة جاذبة تسعى لراحة واستقرار العنصر البشري كأولولية قصوى وبما يتوائم مع السوق المحلية والعالمية عبر الأبحاث والدراسات التي تساهم في تحقيق أعلى معدل رضا لخدمة العملاء والمحافظة على  القيم والمبادئ الأساسية لنا  ومنها:<br><br>\r\n\r\n<ul><li>النزاهة والإحترافية والمسؤولية.</li><li>الإبتكار والتطور التكنولوجي المستمر.</li><li>تقديم خدمات مميزة للمجتمع وطويلة الأمد للعملاء.</li></ul><p> </p>', '2020-08-22 01:17:16', '2020-09-15 12:57:53', NULL),
(2, 'About Us', '1', '<p>&nbsp;</p><p>We BRAKE Fire as a specialized company owns professional and qualified cadres, and because of our feeling of responsibility and as a respond for the trust was given to us by accreditation our company by the civil Defense Department, we took care since launching our company to submit best services and solutions to contribute in implementing highest safety measures according to accreditation demands , terms and laws in order to achieve our desired target in protecting properties and souls of people , and to keep the community safe of dangers .</p><p>&nbsp;</p><p>&nbsp;</p><p>Our company BRAKE Fire Keeps us with growth and development in the United Arab Emirates in the fields of security and safety, and it took upon itself full commitment of performing and high quality jobs in an attractive environment looking for human being comfort and stability as a high priority which copes with local and international markets within studies and researches that contribute of achieving highest level of satisfaction of serving customers and to maintain of basis values and principles which some of them are :</p><p>&nbsp;</p><p>&nbsp;</p><ul><li>Integrity responsibility and professionalism</li><li>Innovation and continuous technological development .</li><li>providing the community of distinguished and long term services for customers.</li></ul><p>&nbsp;</p>', 'من نحن', '<p>&nbsp;</p><p>نحن &nbsp;بريك فاير شركة متخصصة وذات كفاءات وكوادر مؤهلة &nbsp;وشعوراً منا بالمسؤولية وتلبيةً للثقة التي مٌنحت لنا بإعتمادنا من قبل إدارة الدفاع المدني حرصنا منذ تأسيسنا على تقديم أفضل الحلول والخدمات لنساهم في تطبيق أعلى معايير السلامة طبقاً للقوانين والإشتراطات والمتطلبات المعتمدة لتحقيق الهدف المنشود في حماية الأرواح والممتلكات والمحافظة على مجتمع آمن من الأخطار .</p><p>&nbsp;</p><p>&nbsp;</p><p>لقد واكبت بريك فاير التطور والنماء في دولة الإمارات العربية المتحدة في مجالات الأمن والسلامة وأخذت على عاتقها الإلتزام التام بتحقيق أعمال ذات جودة عالية ومثالية في بيئة جاذبة تسعى لراحة واستقرار العنصر البشري كأولولية قصوى وبما يتوائم مع السوق المحلية والعالمية عبر الأبحاث والدراسات التي تساهم في تحقيق أعلى معدل رضا لخدمة العملاء والمحافظة على &nbsp;القيم والمبادئ الأساسية لنا &nbsp;ومنها:</p><p>&nbsp;</p><p>&nbsp;</p><ul><li>النزاهة والإحترافية والمسؤولية.</li><li>الإبتكار والتطور التكنولوجي المستمر.</li><li>تقديم خدمات مميزة للمجتمع وطويلة الأمد للعملاء.</li></ul><p>&nbsp;</p>', '2020-08-22 01:06:10', '2020-08-22 01:06:30', NULL),
(4, 'Our vision', '1', '<span class=\"title\">Security Systems of UAE ranked one of the top security companies in the country by UAE.</span>\r\n<div class=\"text\">To be the pioneers in the field of safety and security systems, and providing technical & creative solutions to protect properties and lives.</div>', 'رؤيتنا', '<span class=\"title\">صنفت أنظمة الأمن في دولة الإمارات العربية المتحدة كواحدة من أفضل شركات الأمن في الدولة من قبل دولة الإمارات العربية المتحدة.</span>\r\n<div class=\"text\"><p>أن نكــون الــرواد فــي مجال أنظمة الأمن والسلامة وتقديــم الحلــول الفنية والإبداعية لحمايــة الأرواح والممتلــكات .</p></div>', '2020-08-22 02:53:38', '2020-09-20 00:51:02', NULL),
(5, 'Our Message', '1', '<h4>Our packages ensure that you feel safe and secure.</h4>\r\n<span class=\"title\">We offer high-quality services at affordable prices</span>\r\n<div class=\"text\">In our pursuit to be distinguished and quality performance we submit our services and professional and qualified cadres and capabilities in the fields of designing , supplying , installing and maintenance security and safety systems .</p><p>Also we work efficiently and effectively to achieve a reality in protecting individuals and properties of our customers in order to achieve our aspirations for continuous , progress and prosperity .</div>', 'رسالتنا', '<h4>تضمن حزمنا أنك تشعر بالأمان والأمان.</h4>\r\n<span class=\"title\">نحن نقدم خدمات عالية الجودة بأسعار معقولة</span>\r\n<div class=\"text\">ســعيا للامتيــاز وجــودة الأداء نوظــف خبراتنــا وكوادرنا المؤهلة وإمكانياتنا فــي مجــال تصميم وتوريد وتركيب وصيانة أنظمــة الأمــن والســلامة ونعمــل بكفــاءة و فعاليــة لنحقق لعملائنــا واقعــاً ملموساً فى حماية الافراد و الممتلكات و تحقيقا لتطلعاتنــا بالنمــو والازدهــار المســتدام.</p><p>&nbsp;</div>', '2020-08-22 02:59:17', '2020-09-08 12:45:36', NULL),
(6, 'Elements of Distinction', '1', '<p>BRAKE Fire Company got extensive experience and proven track record of accomplishments , because it implemented various projects and buildings of different levels and styles in all sectors.</p><p>It managed its projects with high level of accuracy , sufficient , flexibility and adaptable to other business activities in all different stages for each project , in accreditation of implemented plans for buildings and firms, providing consultants and advices in planning area, designing and Installing of fire fighting systems .</p><p><strong>Distinguished Cadres</strong><br>BRAKE Fire is distinguished by its qualified cadres of engineers, skilled technicians, professional administrators and highly professional experts who passed acceptance examinations, implemented and supervised various projects and are being trained and qualified included in cooperative and professional team work continuously.</p><p><strong>ADVANCED Systems and global agencies</strong><br>BRAKE Fire got an exclusive global agencies, local and international distribution agreements for advanced equipment and systems, and signed contracts with the oldest and largest companies which have global accreditations standards, and have been approved by ( NFPA ) and other accreditation bodies and which is identical with local system for economic ministry and civil defense department in the ministry of Interior.</p><p><strong>Governmental Credits and broad Client base</strong><br>BRAKE Fire is a certified company in level (A+) and with an un limited floors in installing and maintenance of all fire fighting systems and meets all requirements which include work cadres , equipment , workshops , transport vehicles and experts certificates . Also it has a wide customers base of construction , consultant , real estate companies and commercial licenses .</p><p><strong>Quality implementation and quick accomplishment</strong><br>BRAKE Fire committed of quality and occupational safety of meeting the demands of its customers in completing jobs in specific times for constructing projects maintenance, emergency, training, co-operation services, and preparing evacuation plans, systems tests and making periodic visits by implementing a program meets with standards and targets of ISO 45001- &nbsp;2018 &nbsp;&amp; iso 14001 2015 &amp; &nbsp;iso 9001 - &nbsp;2015</p><p><strong>Direct, &nbsp;&amp; ONLINE , post Sale Services</strong><br>BRAKE Fire offers direct sale services through its spread galleries and its special application on its website: break fire supplies, with delivering services .</p><p>BRAKE Fire is interested so much about after sales services and strives to provide its customers with modern and new techniques and systems, and to respond emergency cases within our emergency team, who work for 24 hours a day .</p><p><strong>Community training, consulting and awareness</strong></p><p>BRAKE Fire shares in training buildings guards and supervisors, and to submit consultant with services for real-estate companies and owners to evaluate the buildings and how fire fighting system works, and to contribute in community awareness , cooperation and sponsorship within civil defense department and other private and governmental authorities .</p><p>&nbsp;</p>', 'عناصر التميز', '<p>&nbsp;</p><p><strong>الخبرة العريضة</strong><br>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>لدى بريك فاير خبرات واسعة وسجل حافل بالإنجازات فقد نفذت مشاريع متعددة بأنواعها وأبنية ذات مستويات وأنماط مختلفة في كافة القاطاعات وقد أدارت مشاريعها بدقة عالية ومرونة كافية وموائمة لسير الأعمال الأخرى في المراحل المختلفة لكل مشروع و إعتماد المخططات التنفيذية للمباني والمنشآت وتقديم الإستشارات في مجال التخطيط والتصميم والتركيب والصيانة لأنظمة مكافحة الحريق.</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>الكوادر المتميزة</strong><br>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>تتميز بريك فاير بكوادرها المؤهلة من المهندسين والفنيين المهرة والإداريين المتخصصين والخبراء ذات المهنية العالية والذين اجتازوا إختبارات القبول ونفذوا وأشرفوا على مشاريع متنوعة حيث تواصل تدريبهم وتأهيلهم ضمن فريق عمل متعاون ومحترف.</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>وكالات عالمية وأنظمة متطورة</strong><br>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>لدى بريك فاير وكالات حصرية وإتفاقات توزيع محلي ودولي للمواد والمعدات والأنظمة المتطورة والتي يتم التعاقد معها بعناية كبيرة مع أعرق واكبر الشركات ذات الإعتمادات والمعايير العالمية والمتفق عليها من قبل منظمة NFPA &nbsp;والهيئات الإعتمادية الأخرى والمتطابقة مع الأنظمة المحلية لوزارة الإقتصاد والإدارة العامة للدفاع المدني في وزارة الداخلية .</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>إعتمادات حكومية وقاعدة عملاء واسعة</strong><br>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>بريك فاير شركة معتمدة بمستوى درجة أولى A+ &nbsp;غير محددة الطوابق في تركيب وصيانة جميع أنظمة الإطفاء ومستوفية جميع المتطلبات من كادر عمل ومعدات وورش ووسائل نقل وشهادات خبره كما أن لدى بريك فاير قاعدة عملاء واسعة من أكبر شركات المقاولات والإستشارات والعقارات &nbsp;والمؤسسات والرخص التجارية.</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>جودة التنفيذ وسرعة الإنجاز</strong><br>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>بريك فاير تلتزم معايير الجــودة والسلامة المهنية كما أنها تلبي طلب عملائها بالإنجاز في الأوقات المحدده بالنسبة للمشاريع الإنشائية وخدمات الصيانة والطوارئ والتدريب والتعاون في عمل خطط الإخلاء وفحص الأنظمة والقيام بالزيارات الدورية عن طريق برنامج يتطابق مع أهداف ومعايير iso 45001- &nbsp;2018 &nbsp; و iso 14001 2015 &nbsp;و &nbsp;iso 9001 - &nbsp;2015</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>خدمات بيع مباشر وإلكتروني وما بعد البيع</strong><br>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>تقدم بريك فاير خدمات بيع مباشر عبر معارضها المنتشرة وعبر نافذة متجرها &nbsp;الخاص في موقعها الإلكتروني</p><p>&nbsp;</p><p>&nbsp;</p><p>brake fire supplies &nbsp;مع خدمات التوصيل كذلك تولي بريك فاير إهتمام خاص لخدمات ما بعد البيع وتسعى لتزويد عملائها بجميع التقنيات والأنظمة الجديدة وتلبية النداء وفي حالات الطوارئ من خلال فريق طوارئ يعمل على مدار 24 ساعة .</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>تدريب وإستشارات وتوعية مجتمعية</strong><br>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>تساهم بريك فاير في تدريب حراس المباني والقائمون عليها وتقديم خدمات إستشارية للشركات العقارية والملاك لتقييم المباني و عن كيفية عمل أنظمة الحريق وكذلك المساهمة في التوعية المجتمعية والتعاون والرعاية عن طريق إدارة الدفاع المدني أو الجهات الحكومية والخاصة.</p><p>&nbsp;</p>', '2020-08-22 03:04:28', '2020-09-08 12:48:30', NULL),
(7, 'Certification', '1', '<h4 style=\"margin-bottom:1px;\">Certification</h4>\r\n<div style=\"margin-bottom:20px;\" class=\"fb-share-button\" data-href=\"http://brakefireae.com/\" data-layout=\"button_count\" data-size=\"small\"><a target=\"_blank\" href=\"https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbrakefireae.com%2F&src=sdkpreparse\" class=\"fb-xfbml-parse-ignore\">Share</a></div>\r\n<span class=\"title\">We offer high-quality services at affordable prices</span>', 'شهادة', '<h4>شهادة</h4>\r\n<span class=\"title\">نحن نقدم خدمات عالية الجودة بأسعار معقولة</span>', '2020-09-12 09:05:36', '2020-09-26 01:05:09', NULL),
(8, 'Download', '1', '<span class=\"title\">We offer high-quality services at affordable prices</span>', 'التحميلات', '<span class=\"title\">نحن نقدم خدمات عالية الجودة بأسعار معقولة</span>', '2020-09-12 09:51:12', '2020-09-20 00:49:41', NULL),
(9, 'OUR PROJECTS', '1', 'For the greater part of us, our house isn\'t only a place, it\'s the place we bring up our kids, invest energy with family and periodically\r\nresign. It speaks to one of the best ventures a significant number of us make in our lifetimes, yet the vast majority', 'مشاريعنا', 'بالنسبة للجزء الأكبر منا ، فإن منزلنا ليس مجرد مكان ، إنه المكان الذي نربي فيه أطفالنا ، ونستثمر الطاقة مع العائلة ونستقيل بشكل دوري. إنه يتحدث عن واحدة من أفضل المشاريع التي يقوم بها عدد كبير منا في حياتنا ، ومع ذلك فإن الغالبية العظمى منها.', '2020-09-15 13:10:05', '2020-09-15 13:10:05', NULL),
(10, 'Testimonial', '1', '<h2>What Our Client Says?</h2>   \r\n<div class=\"text\">Glad customers are the backbone of any storng business and our is no exeption.<br> Read our testimonials below</div>', 'شهادة', '<h2>ماذا يقول عملائنا؟</h2>\r\n<div class=\"text\">العملاء السعداء هم العمود الفقري لأي عمل تجاري قوي وليس لك استثناء.<br>اقرأ شهاداتنا أدناه</div>', '2020-09-15 15:01:04', '2020-09-15 15:08:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `cust_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `title`, `title_ar`, `status`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Brake Fire Profile', 'ملف حريق الفرامل', '1', NULL, '2020-08-22 02:29:59', '2020-08-22 02:29:59', NULL),
(2, 'Sanitizer Gate Profile', 'الملف الشخصي بوابة المطهر', '1', NULL, '2020-08-22 02:32:31', '2020-08-22 02:32:31', NULL),
(3, 'Telecommunication profile', 'الملف الشخصي للاتصالات', '1', NULL, '2020-08-22 02:39:36', '2020-09-12 09:57:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `electronics_installations`
--

CREATE TABLE `electronics_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject_english` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_arabic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_english` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_arabic` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_english` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_arabic` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `subject_english`, `subject_arabic`, `sms_english`, `sms_arabic`, `message_english`, `message_arabic`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Enquiry', 'تحقيق', 'Thank you for your enquiry. It has been forwarded to the relevant department and will be dealt with as soon as possible. https://bit.ly/30xkfeC', 'شكرا لاستفسارك. تم إرساله إلى الإدارة المعنية وسيتم التعامل معه في أقرب وقت ممكن.', '<p>Thank you for your enquiry. It has been forwarded to the relevant department and will be dealt with as soon as possible.</p>', '<p>شكرا لاستفسارك. تم إرساله إلى الإدارة المعنية وسيتم التعامل معه في أقرب وقت ممكن.</p>', '2020-09-23 20:00:16', '2020-09-23 20:51:38', NULL),
(2, 'BOQ Quotation Invoice', 'فاتورة عرض أسعار BOQ', 'BOQ Quotation Invoice Generated, For more details please visit https://bit.ly/30xkfeC', 'تم إنشاء فاتورة عرض أسعار BOQ ، يرجى الاتصال بنا', '<p>We are Pleased to offer you our best price for the mentioned project. As per your requirement, we hope that you will find our price most suitable and quality up to the standards.</p>', '<p>يسعدنا أن نقدم لكم أفضل الأسعار للمشروع المذكور. وفقًا لمتطلباتك ، نأمل أن تجد سعرنا الأنسب والجودة وفقًا للمعايير.</p>', '2020-09-23 21:20:58', '2020-09-23 21:23:51', NULL),
(3, 'Contact Us', 'اتصل بنا', 'Thank you for getting in touch! We appreciate you contacting us Brake Fire. One of our colleagues will get back in touch with you soon! For more details please visit https://bit.ly/30xkfeC', 'شكرا لك على تواصلك! نحن نقدر لك اتصالك بنا Brake Fire. سيقوم أحد زملائنا بالاتصال بك قريبًا!', '<p><p><b>Thank you for getting in touch! </b></p><br><p>We appreciate you contacting us Brake Fire. One of our colleagues will get back in touch with you soon!</p><br><p>Have a great day!</p></p>', '<p><p><b>Thank you for getting in touch! </b></p><br><p>We appreciate you contacting us Brake Fire. One of our colleagues will get back in touch with you soon!</p><br><p>Have a great day!</p></p>', '2020-09-24 10:59:33', '2020-09-24 11:08:20', NULL),
(4, 'BOQ Form Submitted', 'تم إرسال نموذج BOQ', 'Thank you for ordering the featured selection. For more details please visit https://bit.ly/30xkfeC', 'شكرا لك على طلب الاختيار المميز', '<p>Thank you for ordering the featured selection this month.</p><br>\r\n<p>This is an automatically generated message to confirm receipt of your order via the Internet. You do not need to reply to this e-mail, but you may wish to save it for your records.</p><br>\r\n<p>Your order should arrive. Thank you.</p>', '<p>Thank you for ordering the featured selection this month.</p><br>\r\n<p>This is an automatically generated message to confirm receipt of your order via the Internet. You do not need to reply to this e-mail, but you may wish to save it for your records.</p><br>\r\n<p>Please note BOQ No. {boqno}.\r\n<p>Your order should arrive. Thank you.</p>', '2020-09-24 11:54:40', '2020-09-24 12:00:49', NULL),
(5, 'Enquiry Submitted', 'Reply of Enquiry....', 'We received your inquiry about our service. Please note the {enq_no} for further communication with us. We will contact with you asap. For more details please visit https://bit.ly/30xkfeC', 'Reply of Enquiry....', '<p>We received your inquiry about our service. Please note the {enq_no} for further communication with us. Our team will contact with you asap.</p>', '<p>We will contact with you asap.</p>', '2020-09-24 12:33:05', '2020-10-01 16:41:04', NULL),
(6, 'Complain MAIL', 'Reply of Enquiry....', 'We received your complain about our service. Please note the complain number {complain_number} for further communication with us. We will contact with you asap. For more details please visit https://bit.ly/30xkfeC', '.....', 'We received your complain about our service. Please note the complain number {complain_number} for further communication with us. Our team will contact with you asap.', '<p>We will contact with you asap.</p>', '2020-09-24 12:33:05', '2020-10-01 16:40:07', NULL),
(7, 'Inspection Report', 'Inspection Report.', 'Inspection Report Submitted Successfully.  Please note the report number {report_no}. for further communication with us. For more details please visit https://bit.ly/30xkfeC', '.', '<p>Inspection Report Submitted Successfully.  Please note the report number {report_no}. for further communication with us. Our team will contact with you asap. </p>', '<p>Inspection Report</p>', '2020-10-02 06:00:22', '2020-10-02 06:44:06', NULL),
(8, 'Pro-Forma Invoice', 'Pro-Forma Invoice', 'Please find pro-forma invoice for quotation no {quotationno}.  Please note the quotation number for further communication with us. Our team will contact with you asap. For more details please visit https://bit.ly/30xkfeC', 'Pro-Forma Invoice', '<p>Please find pro-forma invoice for quotation no {quotationno}.  Please note the quotation number for further communication with us. Our team will contact with you asap. </p>', 'Pro-Forma Invoice', '2020-10-02 06:00:22', '2020-10-02 22:17:28', NULL),
(9, 'Inquiry Assignment Notification', 'Inquiry Assignment Notification', 'Inquiry Assignment Notification. Plz note {inquiryno}. For more details please visit https://bit.ly/30xkfeC', 'Inquiry Assignment Notification. Plz note inquiry {inquiryno}.', 'Hi, plz check your account. you assign a new inquiry assignment. Plz note inquiry no  {inquiryno}.', 'Hi, plz check your account. you assign a new inquiry assignment. Plz note inquiry no  {inquiryno}.', NULL, NULL, NULL),
(10, 'Carrier Submitted Successfully', 'Carrier Submitted Successfully', 'Carrier Submitted Successfully. Plz note {carrierno}. For more details please visit https://bit.ly/30xkfeC', 'Carrier Submitted Successfully. Plz note {carrierno}. For more details please visit https://bit.ly/30xkfeC', 'Carrier Submitted Successfully. Plz note {carrierno} for further communication with us. For more details please visit https://bit.ly/30xkfeC', 'Carrier Submitted Successfully. Plz note {carrierno} for further communication with us. For more details please visit https://bit.ly/30xkfeC', NULL, NULL, NULL),
(11, 'Carrier Submitted Successfully', 'EMAIL TEMPLATE.', 'Carrier Submitted Successfully. Plz note {carrierno}. For more details please visit https://bit.ly/30xkfeC', '.', 'Carrier Submitted Successfully. Plz note {carrierno} for further communication with us. For more details please visit https://bit.ly/30xkfeC', '<p>EMAIL TEMPLATE</p>', '2020-10-12 21:34:43', '2020-10-12 21:35:24', NULL),
(12, 'Complain Assignment Notification', 'Complain Assignment Notification', NULL, NULL, '<p>Complain Assignment Notification</p>', '<p>Complain Assignment Notification</p>', '2020-10-12 21:35:54', '2020-10-12 21:35:54', NULL),
(13, 'Complain Assignment Notification', 'Complain Assignment Notification', NULL, NULL, '<p>Complain Assignment Notification</p>', '<p>Complain Assignment Notification</p>', '2020-10-12 21:35:54', '2020-10-12 21:35:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_emergency_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_qualification` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_speciality` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_doj` date DEFAULT NULL,
  `emp_dob` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees_title`
--

CREATE TABLE `employees_title` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees_title`
--

INSERT INTO `employees_title` (`id`, `title_ar`, `title_en`, `status`) VALUES
(1, 'الرائد.', NULL, 1),
(2, 'نقيب مهندس.', NULL, 1),
(3, 'نقيب دكتور.', NULL, 1),
(4, 'نقيب.', NULL, 1),
(5, 'ملازم أول.', NULL, 1),
(6, 'ملازم أول مهندس.', NULL, 1),
(7, 'ملازم أول.', NULL, 1),
(8, 'ملازم ثاني.', NULL, 1),
(9, 'وكيل أول.', NULL, 1),
(10, 'وكيل.', NULL, 1),
(11, 'رقيب أول.', NULL, 1),
(12, 'رقيب.', NULL, 1),
(13, 'عريف أول.', NULL, 1),
(14, 'عريف.', NULL, 1),
(15, 'شرطي أول.', NULL, 1),
(16, 'شرطي.', NULL, 1),
(17, 'السيد.', NULL, 1),
(18, 'السيدة.', NULL, 1),
(19, 'الانسة.', NULL, 1),
(20, 'الدكتور.', NULL, 1),
(21, 'المهندس.', NULL, 1),
(22, 'اللواء خبير.', NULL, 1),
(23, 'العميد.', NULL, 1),
(24, 'العميد خبير.', NULL, 1),
(25, 'العقيد خبير.', NULL, 1),
(26, 'العقيد.', NULL, 1),
(27, 'العقيد خبير طيار.', NULL, 1),
(28, 'العقيد خبير.', NULL, 1),
(29, 'المقدم طيار.', NULL, 1),
(30, 'المقدم.', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `estimator_id` int(11) NOT NULL DEFAULT 0,
  `estimator_view` tinyint(4) NOT NULL DEFAULT 0,
  `boq_id` int(2) NOT NULL DEFAULT 0,
  `enq_no` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_owner_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enq_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enq_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enq_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enq_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `service_type_details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emirates` int(11) NOT NULL DEFAULT 0,
  `emiratescity` int(11) NOT NULL DEFAULT 0,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enq_reply` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `estimator_id`, `estimator_view`, `boq_id`, `enq_no`, `company_owner_name`, `enq_name`, `enq_phone`, `enq_email`, `enq_address`, `service_type`, `service_type_details`, `emirates`, `emiratescity`, `location`, `attachments`, `enq_reply`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, 0, 1, 'BF-INQ-11', NULL, 'Customer One', '971559007972', 'customer@admin.com', NULL, 1, 'Supply', 2, 141, 'DUBAI, Ajman', NULL, '<br><p>Hi, hhh</p>', 0, 1, '2020-10-04 01:23:14', '2020-10-16 22:35:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `estimations`
--

CREATE TABLE `estimations` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `estimations`
--

INSERT INTO `estimations` (`id`, `category_id`, `title`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 'CON FACP', 1, '2020-08-13 11:39:11', '2020-08-13 11:43:29', NULL),
(2, 14, 'ADD FACPPPZZXXXC', 1, '2020-08-13 11:44:18', '2020-10-02 08:44:14', NULL),
(3, 21, 'CON SMOKE', 1, '2020-08-16 22:42:31', '2020-08-16 22:42:31', NULL),
(4, 14, 'CON HEAT', 1, '2020-08-18 20:28:22', '2020-08-18 20:28:22', NULL),
(5, 14, 'CON MCP', 1, '2020-08-18 20:30:45', '2020-08-18 20:30:45', NULL),
(6, 14, 'CON SOUNDER', 1, '2020-08-18 20:33:13', '2020-08-18 20:33:13', NULL),
(7, 21, 'REMOTE INDICATOR', 1, '2020-08-18 20:35:19', '2020-10-08 20:54:32', NULL),
(8, 14, 'BELL', 1, '2020-08-18 20:36:22', '2020-08-18 20:36:22', NULL),
(9, 21, 'ADD SMOKE', 1, '2020-08-18 20:43:48', '2020-08-18 20:43:48', NULL),
(10, 14, 'ADD HEAT', 1, '2020-08-18 21:13:44', '2020-08-18 21:13:44', NULL),
(11, 17, 'ADD MCP', 1, '2020-08-18 21:13:44', '2020-08-18 21:13:44', NULL),
(12, 18, 'ADD RELAY MODULE', 1, '2020-08-18 21:13:44', '2020-08-18 21:13:44', NULL),
(13, 14, 'ADD MONITOR MODULE', 1, '2020-08-18 21:13:44', '2020-08-18 21:13:44', NULL),
(14, 14, 'ADD SOUNDER', 1, '2020-08-18 21:13:44', '2020-08-18 21:13:44', NULL),
(15, 18, 'ADD SOUNDER W/P', 1, '2020-08-18 21:13:44', '2020-10-08 20:54:04', NULL),
(16, 17, 'BATTERY', 1, '2020-08-19 09:37:20', '2020-10-08 20:53:52', NULL),
(17, 14, 'Sprinkler', 1, '2020-08-23 05:17:09', '2020-10-08 20:52:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `estimationsizes`
--

CREATE TABLE `estimationsizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `estimation_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `estimationsizes`
--

INSERT INTO `estimationsizes` (`id`, `estimation_id`, `title`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1 ZONE', 1, '2020-08-18 20:47:24', '2020-08-18 20:47:24', NULL),
(2, 1, '2 ZONE', 1, '2020-08-18 20:47:41', '2020-08-18 20:47:41', NULL),
(3, 1, '4 ZONE', 1, '2020-08-18 20:47:55', '2020-08-18 20:47:55', NULL),
(4, 1, '8 ZONE', 1, '2020-08-18 20:48:06', '2020-08-18 20:48:06', NULL),
(5, 1, '16 ZONE', 1, '2020-08-18 20:48:17', '2020-08-18 20:48:17', NULL),
(6, 2, '1 LOOP', 1, '2020-08-18 20:48:47', '2020-08-18 20:48:47', NULL),
(7, 1, '2 LOOP', 1, '2020-08-18 20:48:57', '2020-08-18 20:48:57', NULL),
(8, 8, '6\"', 1, '2020-08-18 20:49:23', '2020-08-18 20:49:23', NULL),
(9, 8, '8\"', 1, '2020-08-18 20:49:34', '2020-08-18 20:50:03', NULL),
(10, 16, '1.2 AH', 1, '2020-08-19 09:38:06', '2020-08-19 09:38:06', NULL),
(11, 16, '3.2 AH', 1, '2020-08-19 09:40:54', '2020-08-19 09:40:54', NULL),
(12, 17, 'Pendent', 1, '2020-08-23 05:17:40', '2020-08-23 05:17:40', NULL),
(13, 17, 'Upright', 1, '2020-08-23 05:18:05', '2020-08-23 05:18:05', NULL),
(14, 17, 'Sidewall', 1, '2020-08-23 05:18:18', '2020-09-27 03:26:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary_ar` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_time` datetime DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `title_ar`, `status`, `summary`, `summary_ar`, `event_time`, `details`, `details_ar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'HAPPY EID UL ADHA', 'عيد سعيد عيد الأضحى', '1', '<p>\"May Allah bring you joy, happiness, peace and prosperity on this blessed occasion. Wishing you and your family on this happy occasion of&nbsp;<strong>Eid</strong>!&nbsp;<strong>Eid</strong>&nbsp;Mubarak!\" \"May Allah flood your life with happiness on this occasion, your heart with love, your soul with spiritual, your mind with wisdom, wishing you a very Happy&nbsp;<strong>Eid</strong>.\"</p>', '<p>\"الله يجلب لك الفرح والسعادة والسلام والازدهار بهذه المناسبة المباركة. أتمنى لك ولأسرتك هذه المناسبة السعيدة للعيد! عيد مبارك!\" \"عسى الله أن يغمر حياتك بالسعادة بهذه المناسبة ، قلبك بالحب ، روحك بالروح ، عقلك بالحكمة ، أتمنى لك عيداً سعيداً\".</p>', '2020-07-29 00:00:00', '<p>\"May Allah bring you joy, happiness, peace and prosperity on this blessed occasion. Wishing you and your family on this happy occasion of&nbsp;<strong>Eid</strong>!&nbsp;<strong>Eid</strong>&nbsp;Mubarak!\" \"May Allah flood your life with happiness on this occasion, your heart with love, your soul with spiritual, your mind with wisdom, wishing you a very Happy&nbsp;<strong>Eid</strong>.\"</p>', '<p>\"الله يجلب لك الفرح والسعادة والسلام والازدهار بهذه المناسبة المباركة. أتمنى لك ولأسرتك هذه المناسبة السعيدة للعيد! عيد مبارك!\" \"عسى الله أن يغمر حياتك بالسعادة بهذه المناسبة ، قلبك بالحب ، روحك بالروح ، عقلك بالحكمة ، أتمنى لك عيداً سعيداً\".</p>', '2020-09-13 02:15:12', '2020-09-13 02:15:12', NULL),
(2, 'Eid Mubarak 2020', 'عيد مبارك 2020', '1', '<p>The Brake fire family extends its warmest congratulations on the occasion of the coming of Eid Al Fitar. we wish to be all in good health.</p>', '<p>تتقدم عائلة مكابح النار بأحر التهاني بمناسبة حلول عيد الفطر المبارك. نتمنى أن نكون جميعًا بصحة جيدة.</p>', '2020-05-20 00:00:00', '<p>The Brake fire family extends its warmest congratulations on the occasion of the coming of Eid Al Fitar. we wish to be all in good health.</p>', '<p>تتقدم عائلة مكابح النار بأحر التهاني بمناسبة حلول عيد الفطر المبارك. نتمنى أن نكون جميعًا بصحة جيدة.</p>', '2020-09-13 05:07:53', '2020-09-13 05:07:53', NULL),
(3, 'UAE National Day 2 December', 'اليوم الوطني لدولة الإمارات العربية المتحدة 2 ديسمبر', '1', '<p>The National Day of UAE is celebrated on December 2 every year that marks the day when emirates united together at Union House to form the United Arab Emirates in 1971. Sheikh Zayed bin Sultan Al Nahyan was the first president of UAE. Celebrations are held across the country on UAE national day holiday. Fireworks, dance shows, and car rallies are some of the most common activities that people enjoy on this holiday. Air shows are conducted and military parades are held at Abu Dhabi National Exhibition Centre on UAE holiday.</p>', '<p>يتم الاحتفال باليوم الوطني لدولة الإمارات العربية المتحدة في الثاني من ديسمبر من كل عام ، والذي يصادف اليوم الذي اتحدت فيه الإمارات في بيت الاتحاد لتشكيل دولة الإمارات العربية المتحدة في عام 1971. كان الشيخ زايد بن سلطان آل نهيان أول رئيس لدولة الإمارات العربية المتحدة. تقام الاحتفالات في جميع أنحاء الدولة في عطلة اليوم الوطني لدولة الإمارات العربية المتحدة. تعد الألعاب النارية وعروض الرقص وتجمعات السيارات من أكثر الأنشطة شيوعًا التي يستمتع بها الناس في هذه العطلة. تقام العروض الجوية والاستعراضات العسكرية في مركز أبوظبي الوطني للمعارض في عطلة بدولة الإمارات العربية المتحدة.</p>', '2020-05-19 00:00:00', '<p>يتم الاحتفال باليوم الوطني لدولة الإمارات العربية المتحدة في الثاني من ديسمبر من كل عام ، والذي يصادف اليوم الذي اتحدت فيه الإمارات في بيت الاتحاد لتشكيل دولة الإمارات العربية المتحدة في عام 1971. كان الشيخ زايد بن سلطان آل نهيان أول رئيس لدولة الإمارات العربية المتحدة. تقام الاحتفالات في جميع أنحاء الدولة في عطلة اليوم الوطني لدولة الإمارات العربية المتحدة. تعد الألعاب النارية وعروض الرقص وتجمعات السيارات من أكثر الأنشطة شيوعًا التي يستمتع بها الناس في هذه العطلة. تقام العروض الجوية والاستعراضات العسكرية في مركز أبوظبي الوطني للمعارض في عطلة بدولة الإمارات العربية المتحدة.</p><p>في ديسمبر 1971 ، قرر حكام دبي وأبو ظبي والعين وعجمان والشارقة وأم القيوين الاتحاد ووضع فكرة الإمارات العربية المتحدة. في وقت لاحق من فبراير 1972 ، قررت رأس الخيمة الانضمام إليهم لتصبح وحدة واحدة. بدأت فكرة التوطين الموحد في الإمارات بعد أن أعلن البريطانيون رغبتهم في الانسحاب من مستعمرات شرق البحر المتوسط. وعقد اجتماع بين الشيخ زايد بن سلطان آل نهيان والشيخ راشد بن سعيد آل مكتوم وقرروا الاتحاد. كما دعوا الإمارات الخليجية الأخرى للالتقاء من أجل الاتحاد. بعد مغادرة البريطانيين في عام 1968 ، بدأت الاحتفالات بالعيد الوطني بعد ثلاث سنوات في عام 1971. عندما وافق الدستور الاتحادي لدولة الإمارات العربية المتحدة على المبدأ في الأول من ديسمبر عام 1971. وفي اليوم التالي ، قررت الإمارة السابعة الانضمام أيضًا. لذلك ، يصادف الثاني من ديسمبر من كل عام الاحتفال باليوم الوطني لدولة الإمارات العربية المتحدة.</p>', '<p>The&nbsp;<a href=\"https://www.fnp.ae/article/when-is-uae-national-day\">National Day of UAE</a>&nbsp;is celebrated on December 2 every year that marks the day when emirates united together at Union House to form the United Arab Emirates in 1971. Sheikh Zayed bin Sultan Al Nahyan was the first president of UAE. Celebrations are held across the country on UAE national day holiday. Fireworks, dance shows, and car rallies are some of the most common activities that people enjoy on this holiday. Air shows are conducted and military parades are held at Abu Dhabi National Exhibition Centre on UAE holiday.</p><p>In December 1971, the rulers of Dubai, Abu Dhabi, Al-Ain, Ajman, Sharjah, &amp; Umm al-Quwain decided to unite and laid down the idea of UAE. Later in February 1972, Ras Al Khaimah decided to join them and become a single unit. The idea of united settlement in UAE began after the British declared their desire to withdraw from colonies of the eastern Mediterranean. A meeting was held between Sheikh Zayed bin Sultan Al Nahyan and Sheikh Rashid bin Saeed Al Makhtoum and they decided to unite. They also invited the other Gulf Emirates to come together for the union. After the British left in 1968, the National Day celebrations started three years later in 1971. When the Emirates Federal Constitution approved the principle on Dec 1, 1971. And the very next day, the seventh emirate decided to join in as well. Therefore, each year, 2nd December marks the celebration of the National day of UAE.</p>', '2020-09-13 05:10:46', '2020-09-13 05:10:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fire_installations`
--

CREATE TABLE `fire_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fire_maintenances`
--

CREATE TABLE `fire_maintenances` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fire_supplies`
--

CREATE TABLE `fire_supplies` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gas_installations`
--

CREATE TABLE `gas_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `g_maintinance_installations`
--

CREATE TABLE `g_maintinance_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_products`
--

CREATE TABLE `inspection_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_category_id` int(11) NOT NULL,
  `inspection_product_id` int(11) NOT NULL,
  `inspection_product_size_id` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inspection_products`
--

INSERT INTO `inspection_products` (`id`, `inspection_category_id`, `inspection_product_id`, `inspection_product_size_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, 1, 1, NULL, NULL, NULL),
(2, 6, 1, 2, 1, NULL, NULL, NULL),
(3, 6, 1, 3, 1, NULL, NULL, NULL),
(4, 6, 1, 4, 1, NULL, NULL, NULL),
(5, 7, 17, 12, 1, NULL, NULL, NULL),
(6, 7, 17, 13, 1, NULL, NULL, NULL),
(7, 7, 17, 14, 1, NULL, NULL, NULL),
(8, 8, 3, 0, 1, NULL, NULL, NULL),
(9, 8, 4, 0, 1, NULL, NULL, NULL),
(10, 8, 5, 0, 1, NULL, NULL, NULL),
(11, 8, 6, 0, 1, NULL, NULL, NULL),
(12, 9, 14, 0, 1, NULL, NULL, NULL),
(13, 9, 15, 0, 1, NULL, NULL, NULL),
(14, 9, 16, 10, 1, NULL, NULL, NULL),
(15, 9, 16, 11, 1, NULL, NULL, NULL),
(16, 12, 13, 0, 1, NULL, NULL, NULL),
(17, 12, 14, 0, 1, NULL, NULL, NULL),
(18, 11, 8, 8, 1, NULL, NULL, NULL),
(19, 11, 8, 9, 1, NULL, NULL, NULL),
(20, 11, 11, 0, 1, NULL, NULL, NULL),
(21, 11, 15, 0, 1, NULL, NULL, NULL),
(22, 11, 16, 10, 1, NULL, NULL, NULL),
(23, 11, 16, 11, 1, NULL, NULL, NULL),
(24, 10, 1, 1, 1, NULL, NULL, NULL),
(25, 10, 1, 2, 1, NULL, NULL, NULL),
(26, 10, 1, 3, 1, NULL, NULL, NULL),
(27, 10, 1, 4, 1, NULL, NULL, NULL),
(28, 10, 1, 7, 1, NULL, NULL, NULL),
(29, 10, 2, 6, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inspection_reports`
--

CREATE TABLE `inspection_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `inspection_number` int(11) NOT NULL DEFAULT 1,
  `report_no` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `boqdetails_id` int(11) DEFAULT NULL,
  `inspection_type_id` int(11) NOT NULL DEFAULT 0,
  `client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dtd` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inspection_reports`
--

INSERT INTO `inspection_reports` (`id`, `inspection_number`, `report_no`, `user_id`, `boqdetails_id`, `inspection_type_id`, `client`, `ref_no`, `location`, `dtd`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(15, 1, 'BF-INS-REP-2', 3, NULL, 1, 'Mitus', '6', 'dubai BHUJ', '2020-10-11', 1, '2020-10-11 10:22:07', '2020-10-11 10:22:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inspection_report_products`
--

CREATE TABLE `inspection_report_products` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `report_id` int(11) NOT NULL DEFAULT 0,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `report_status` int(11) NOT NULL DEFAULT 0,
  `msg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inspection_report_products`
--

INSERT INTO `inspection_report_products` (`id`, `report_id`, `product_id`, `report_status`, `msg`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 12, 2, 'FSDGDF FGYUI32', 1, '2020-08-28 08:47:06', '2020-08-28 08:47:06', NULL),
(2, 2, 1, 2, 'pppkkk', 1, '2020-08-29 05:46:48', '2020-08-29 05:46:48', NULL),
(3, 3, 1, 1, 'ghgfh', 1, '2020-10-02 05:41:49', '2020-10-02 05:41:49', NULL),
(4, 3, 2, 1, 'fghfgh', 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(5, 3, 3, 1, 'cv', 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(6, 3, 4, 2, NULL, 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(7, 3, 5, 2, 'gfhfghf', 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(8, 3, 6, 1, 'fghgfh', 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(9, 3, 7, 2, NULL, 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(10, 3, 8, 2, NULL, 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(11, 3, 9, 2, 'ggggggghhhhhhhh', 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(12, 3, 10, 2, NULL, 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(13, 3, 11, 1, NULL, 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(14, 3, 12, 1, 'fgfgh', 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(15, 3, 13, 2, 'ggggggggggg', 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(16, 3, 14, 2, NULL, 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(17, 3, 15, 2, NULL, 1, '2020-10-02 05:41:50', '2020-10-02 05:41:50', NULL),
(18, 4, 1, 1, 'ghgfh', 1, '2020-10-02 05:45:08', '2020-10-02 05:45:08', NULL),
(19, 4, 2, 1, 'fghfgh', 1, '2020-10-02 05:45:08', '2020-10-02 05:45:08', NULL),
(20, 4, 3, 1, 'cv', 1, '2020-10-02 05:45:08', '2020-10-02 05:45:08', NULL),
(21, 4, 4, 2, NULL, 1, '2020-10-02 05:45:08', '2020-10-02 05:45:08', NULL),
(22, 4, 5, 2, 'gfhfghf', 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(23, 4, 6, 1, 'fghgfh', 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(24, 4, 7, 2, NULL, 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(25, 4, 8, 2, NULL, 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(26, 4, 9, 2, 'ggggggghhhhhhhh', 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(27, 4, 10, 2, NULL, 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(28, 4, 11, 1, NULL, 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(29, 4, 12, 1, 'fgfgh', 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(30, 4, 13, 2, 'ggggggggggg', 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(31, 4, 14, 2, NULL, 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(32, 4, 15, 2, NULL, 1, '2020-10-02 05:45:09', '2020-10-02 05:45:09', NULL),
(33, 5, 1, 1, 'ghgfh', 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(34, 5, 2, 1, 'fghfgh', 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(35, 5, 3, 1, 'cv', 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(36, 5, 4, 2, NULL, 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(37, 5, 5, 2, 'gfhfghf', 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(38, 5, 6, 1, 'fghgfh', 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(39, 5, 7, 2, NULL, 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(40, 5, 8, 2, NULL, 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(41, 5, 9, 2, 'ggggggghhhhhhhh', 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(42, 5, 10, 2, NULL, 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(43, 5, 11, 1, NULL, 1, '2020-10-02 05:56:27', '2020-10-02 05:56:27', NULL),
(44, 5, 12, 1, 'fgfgh', 1, '2020-10-02 05:56:28', '2020-10-02 05:56:28', NULL),
(45, 5, 13, 2, 'ggggggggggg', 1, '2020-10-02 05:56:28', '2020-10-02 05:56:28', NULL),
(46, 5, 14, 2, NULL, 1, '2020-10-02 05:56:28', '2020-10-02 05:56:28', NULL),
(47, 5, 15, 2, NULL, 1, '2020-10-02 05:56:28', '2020-10-02 05:56:28', NULL),
(48, 6, 1, 1, 'ghgfh', 1, '2020-10-02 05:56:59', '2020-10-02 05:56:59', NULL),
(49, 6, 2, 1, 'fghfgh', 1, '2020-10-02 05:56:59', '2020-10-02 05:56:59', NULL),
(50, 6, 3, 1, 'cv', 1, '2020-10-02 05:56:59', '2020-10-02 05:56:59', NULL),
(51, 6, 4, 2, NULL, 1, '2020-10-02 05:56:59', '2020-10-02 05:56:59', NULL),
(52, 6, 5, 2, 'gfhfghf', 1, '2020-10-02 05:56:59', '2020-10-02 05:56:59', NULL),
(53, 6, 6, 1, 'fghgfh', 1, '2020-10-02 05:56:59', '2020-10-02 05:56:59', NULL),
(54, 6, 7, 2, NULL, 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(55, 6, 8, 2, NULL, 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(56, 6, 9, 2, 'ggggggghhhhhhhh', 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(57, 6, 10, 2, NULL, 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(58, 6, 11, 1, NULL, 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(59, 6, 12, 1, 'fgfgh', 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(60, 6, 13, 2, 'ggggggggggg', 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(61, 6, 14, 2, NULL, 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(62, 6, 15, 2, NULL, 1, '2020-10-02 05:57:00', '2020-10-02 05:57:00', NULL),
(63, 7, 1, 1, 'ghgfh', 1, '2020-10-02 06:06:33', '2020-10-02 06:06:33', NULL),
(64, 7, 2, 1, 'fghfgh', 1, '2020-10-02 06:06:33', '2020-10-02 06:06:33', NULL),
(65, 7, 3, 1, 'cv', 1, '2020-10-02 06:06:33', '2020-10-02 06:06:33', NULL),
(66, 7, 4, 2, NULL, 1, '2020-10-02 06:06:33', '2020-10-02 06:06:33', NULL),
(67, 7, 5, 2, 'gfhfghf', 1, '2020-10-02 06:06:33', '2020-10-02 06:06:33', NULL),
(68, 7, 6, 1, 'fghgfh', 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(69, 7, 7, 2, NULL, 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(70, 7, 8, 2, NULL, 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(71, 7, 9, 2, 'ggggggghhhhhhhh', 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(72, 7, 10, 2, NULL, 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(73, 7, 11, 1, NULL, 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(74, 7, 12, 1, 'fgfgh', 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(75, 7, 13, 2, 'ggggggggggg', 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(76, 7, 14, 2, NULL, 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(77, 7, 15, 2, NULL, 1, '2020-10-02 06:06:34', '2020-10-02 06:06:34', NULL),
(78, 8, 1, 1, 'ghgfh', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(79, 8, 2, 1, 'fghfgh', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(80, 8, 3, 1, 'cv', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(81, 8, 4, 2, NULL, 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(82, 8, 5, 2, 'gfhfghf', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(83, 8, 6, 1, 'fghgfh', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(84, 8, 7, 2, NULL, 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(85, 8, 8, 2, NULL, 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(86, 8, 9, 2, 'ggggggghhhhhhhh', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(87, 8, 10, 2, NULL, 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(88, 8, 11, 1, NULL, 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(89, 8, 12, 1, 'fgfgh', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(90, 8, 13, 2, 'ggggggggggg', 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(91, 8, 14, 2, NULL, 1, '2020-10-02 06:07:52', '2020-10-02 06:07:52', NULL),
(92, 8, 15, 2, NULL, 1, '2020-10-02 06:07:53', '2020-10-02 06:07:53', NULL),
(93, 9, 1, 1, NULL, 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(94, 9, 2, 1, NULL, 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(95, 9, 3, 1, NULL, 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(96, 9, 4, 2, NULL, 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(97, 9, 5, 2, 'ret', 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(98, 9, 6, 2, 'ret', 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(99, 9, 7, 2, 'etert', 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(100, 9, 8, 2, 'etet', 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(101, 9, 9, 1, NULL, 1, '2020-10-02 06:50:56', '2020-10-02 06:50:56', NULL),
(102, 9, 10, 1, NULL, 1, '2020-10-02 06:50:57', '2020-10-02 06:50:57', NULL),
(103, 9, 11, 2, 'ertet', 1, '2020-10-02 06:50:57', '2020-10-02 06:50:57', NULL),
(104, 9, 12, 2, 'ertret', 1, '2020-10-02 06:50:57', '2020-10-02 06:50:57', NULL),
(105, 9, 13, 2, NULL, 1, '2020-10-02 06:50:57', '2020-10-02 06:50:57', NULL),
(106, 9, 14, 2, NULL, 1, '2020-10-02 06:50:57', '2020-10-02 06:50:57', NULL),
(107, 9, 15, 2, NULL, 1, '2020-10-02 06:50:57', '2020-10-02 06:50:57', NULL),
(108, 10, 1, 1, 'dsfsdf', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(109, 10, 2, 1, 'dsfdsf', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(110, 10, 3, 1, 'dsfsdf', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(111, 10, 4, 2, NULL, 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(112, 10, 5, 2, NULL, 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(113, 10, 6, 2, NULL, 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(114, 10, 7, 2, NULL, 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(115, 10, 8, 2, 'dfdsg', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(116, 10, 9, 2, 'dsfsdf', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(117, 10, 10, 2, 'fghgghjhk', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(118, 10, 11, 2, 'fdggj', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(119, 10, 12, 2, 'ghjgj', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(120, 10, 13, 2, 'ghjghjhkhjk', 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(121, 10, 14, 2, NULL, 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(122, 10, 15, 2, NULL, 1, '2020-10-02 07:00:25', '2020-10-02 07:00:25', NULL),
(123, 11, 1, 2, 'pppkkk', 1, '2020-10-02 07:03:36', '2020-10-02 07:03:36', NULL),
(124, 11, 2, 2, 'asds ddgdfg', 1, '2020-10-02 07:03:36', '2020-10-02 07:03:36', NULL),
(125, 11, 3, 2, 'dfds dgffgh', 1, '2020-10-02 07:03:36', '2020-10-02 07:03:36', NULL),
(126, 11, 4, 2, 'dfgfh ffgfg fggh', 1, '2020-10-02 07:03:36', '2020-10-02 07:03:36', NULL),
(127, 11, 5, 2, 'fgdg', 1, '2020-10-02 07:03:36', '2020-10-02 07:03:36', NULL),
(128, 11, 6, 1, 'ffh dfgfd f', 1, '2020-10-02 07:03:36', '2020-10-02 07:03:36', NULL),
(129, 11, 7, 1, 'fdgdfg', 1, '2020-10-02 07:03:36', '2020-10-02 07:03:36', NULL),
(130, 11, 8, 2, 'ew5435', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(131, 11, 9, 2, 'dfgdfgj', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(132, 11, 10, 1, 'dffdfg', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(133, 11, 11, 2, 'cxdfh sd', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(134, 11, 12, 2, 'fggf', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(135, 11, 13, 1, '575fh gf gfhgf', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(136, 11, 14, 2, 'dryy', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(137, 11, 15, 2, 'dfg erte', 1, '2020-10-02 07:03:37', '2020-10-02 07:03:37', NULL),
(138, 14, 1, 1, 'pppkkk', 1, '2020-10-02 07:10:42', '2020-10-02 07:10:42', NULL),
(139, 14, 2, 2, NULL, 1, '2020-10-02 07:10:42', '2020-10-02 07:10:42', NULL),
(140, 14, 3, 1, 'FFGHF', 1, '2020-10-02 07:10:42', '2020-10-02 07:10:42', NULL),
(141, 14, 4, 2, NULL, 1, '2020-10-02 07:10:42', '2020-10-02 07:10:42', NULL),
(142, 14, 5, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(143, 14, 6, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(144, 14, 7, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(145, 14, 8, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(146, 14, 9, 2, 'dgdfdfg', 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(147, 14, 10, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(148, 14, 11, 2, 'DFD SDFDSF', 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(149, 14, 12, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(150, 14, 13, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(151, 14, 14, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(152, 14, 15, 2, NULL, 1, '2020-10-02 07:10:43', '2020-10-02 07:10:43', NULL),
(153, 15, 1, 1, 'pppkkk', 1, '2020-10-11 10:22:07', '2020-10-11 10:22:07', NULL),
(154, 15, 2, 1, 'PKKKK', 1, '2020-10-11 10:22:07', '2020-10-11 10:22:07', NULL),
(155, 15, 3, 1, 'FFGHF', 1, '2020-10-11 10:22:07', '2020-10-11 10:22:07', NULL),
(156, 15, 4, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(157, 15, 5, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(158, 15, 6, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(159, 15, 7, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(160, 15, 8, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(161, 15, 9, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(162, 15, 10, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(163, 15, 11, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(164, 15, 12, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(165, 15, 13, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(166, 15, 14, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL),
(167, 15, 15, 2, NULL, 1, '2020-10-11 10:22:08', '2020-10-11 10:22:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inspection_types`
--

CREATE TABLE `inspection_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inspection_types`
--

INSERT INTO `inspection_types` (`id`, `parent_id`, `name`, `name_ar`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'Fire Alarm System', 'نظام انذار الاحرائق', '1', '2020-08-22 07:46:40', '2020-08-22 07:46:40', NULL),
(2, 0, 'Fire Sprinkler System', 'نظام رش النار', '1', '2020-08-22 07:47:19', '2020-08-22 07:47:19', NULL),
(6, 1, 'Fire Alarm System', 'نظام انذار الاحرائق', '1', '2020-08-22 20:46:24', '2020-08-22 20:46:24', NULL),
(7, 1, 'Emergency Light System', 'نظام إضاءة الطوارئ', '1', '2020-08-22 20:47:42', '2020-08-22 20:47:42', NULL),
(8, 1, 'Fire Hose Reel / Dry Riser / Wet Riser System', 'بكرة خرطوم الحريق / الناهض الجاف / نظام الرافع الرطب', '1', '2020-08-22 20:49:30', '2020-08-22 20:49:30', NULL),
(9, 1, 'Fire Extinguisher', 'طفاية حريق', '1', '2020-08-22 20:49:49', '2020-08-22 20:49:49', NULL),
(10, 2, 'Fire Sprinkler System', 'نظام رش النار', '1', '2020-08-22 20:50:57', '2020-08-22 20:50:57', NULL),
(11, 2, 'Fire Pump', 'مضخة حريق', '1', '2020-08-22 20:51:15', '2020-08-22 20:51:15', NULL),
(12, 2, 'Water / Foam Hydrant System', 'نظام صنبور المياه / الرغوة', '1', '2020-08-22 20:51:32', '2020-08-22 20:51:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobcode`
--

CREATE TABLE `jobcode` (
  `id` int(10) UNSIGNED NOT NULL,
  `boqdetails_id` int(11) NOT NULL,
  `jobcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_dtd` datetime DEFAULT NULL,
  `end_dtd` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobcode`
--

INSERT INTO `jobcode` (`id`, `boqdetails_id`, `jobcode`, `remarks`, `start_dtd`, `end_dtd`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 4, 'BFA15143', 'New Job Code for QNO-4587', '2020-09-06 10:00:00', NULL, 1, '2020-08-27 23:18:31', '2020-09-06 01:18:23', NULL),
(6, 5, 'SDF3453', 'Job Code Created', '2020-08-30 09:00:00', '2020-08-31 14:00:00', 1, '2020-08-29 03:22:49', '2020-09-06 01:19:27', NULL),
(7, 6, 'fdcgfhf', 'fcvhfgh', '2020-10-09 23:28:22', '2020-10-17 23:28:26', 1, '2020-10-06 12:28:31', '2020-10-06 12:28:31', NULL),
(8, 7, 'BF-JS-11', 'DFGFDDFGFGDF', '2020-10-08 22:56:01', '2020-10-16 22:56:06', 1, '2020-10-08 11:56:12', '2020-10-08 11:56:12', NULL),
(9, 8, 'BF-JS-11', 'DFGFDDFGFGDF', '2020-10-08 23:06:18', '2020-10-10 23:06:20', 1, '2020-10-08 12:06:27', '2020-10-08 12:06:27', NULL),
(10, 12, 'BF-JS-11', 'DFGFDDFGFGDF', '2020-10-17 12:28:43', '2020-10-31 12:28:45', 1, '2020-10-17 01:28:52', '2020-10-17 01:28:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobcode_details`
--

CREATE TABLE `jobcode_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `jobcode_id` int(11) NOT NULL DEFAULT 0,
  `sitename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_ongoing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_completed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agentname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `projecttype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimationtime` int(11) DEFAULT 0,
  `task` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobcode_details`
--

INSERT INTO `jobcode_details` (`id`, `jobcode_id`, `sitename`, `work_ongoing`, `work_completed`, `msg`, `agentname`, `projecttype`, `estimationtime`, `task`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 5, 'JCD2323', 'DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK', 'JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF', 'VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE', NULL, NULL, 2, NULL, 1, '2020-08-27 23:39:25', '2020-10-16 23:28:44', NULL),
(3, 5, 'chfhfgh', 'DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK', 'JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF', 'VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE', 'Mang. Syed', 'dfgdfg', 6, NULL, 1, '2020-10-16 23:29:07', '2020-10-17 01:37:27', NULL),
(4, 5, 'testt', 'DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK DFJGHKD KHSDDJFG DSKGLDKHG KDFDSFDLK', 'JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF JDFGHKDF DFKJGLDGFGDSF DSFNMNDJF', 'VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE VBMN,J DFGFF FDTTRWE', 'Mang. Syed', 'dfgdfg dfgdfg', 2, NULL, 1, '2020-10-17 01:34:18', '2020-10-17 01:34:18', NULL),
(5, 8, 'chfhfgh', 'DFJGHKD KHSDDJFG DSKGLDKHGp', 'fghgfh', 'VBMN,J DFGFF FDTTRWE VBMN,J DFGFFg', 'Mang. Syed', 'dfgdfg dfgdfg', 2, NULL, 1, '2020-10-17 01:41:17', '2020-10-17 01:51:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `labourcost`
--

CREATE TABLE `labourcost` (
  `id` int(10) UNSIGNED NOT NULL,
  `labour_cost` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `labourcost`
--

INSERT INTO `labourcost` (`id`, `labour_cost`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 100, 1, NULL, NULL, NULL),
(2, 200, 1, NULL, NULL, NULL),
(3, 300, 1, NULL, NULL, NULL),
(4, 400, 1, NULL, NULL, NULL),
(5, 500, 1, NULL, NULL, NULL),
(6, 600, 1, NULL, NULL, NULL),
(7, 700, 1, NULL, NULL, NULL),
(8, 800, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `labour_hours`
--

CREATE TABLE `labour_hours` (
  `id` int(10) UNSIGNED NOT NULL,
  `labour_hour` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_type` int(11) DEFAULT 0,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `estimation` int(11) NOT NULL DEFAULT 0,
  `estimationsize` int(11) NOT NULL DEFAULT 0,
  `brand` int(11) NOT NULL DEFAULT 0,
  `price` decimal(10,0) NOT NULL DEFAULT 0,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `service_type`, `category_id`, `estimation`, `estimationsize`, `brand`, `price`, `code`, `title`, `remark`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 14, 1, 1, 4, '2', 'X452', 'Loren Ipsum Loren Ipsum', 'Loren ipsum txt Loren ipsum txt', 1, '2020-08-18 21:37:41', '2020-08-18 21:40:04', NULL),
(2, 2, 21, 8, 8, 4, '3', 'S345', 'Loren Ipsum Loren Ipsum Loren Ipsum', 'Loren Ipsum Loren Ipsum', 1, '2020-08-18 21:39:26', '2020-08-18 21:42:05', NULL),
(3, 2, 17, 1, 2, 4, '4', 'SD23', 'Loren Ipsum Loren Ipsum Loren Ipsum', 'Loren Ipsum Loren Ipsum Loren Ipsum', 1, '2020-08-18 21:40:48', '2020-08-18 21:40:48', NULL),
(4, 1, 21, 1, 3, 6, '2', 'X234', 'Loren ipsum Loren ipsum Loren ipsum', 'Its a best product XC', 1, '2020-08-19 04:17:39', '2020-08-19 04:17:39', NULL),
(5, 1, 14, 1, 4, 6, '2', 'X231', 'Loren ipsum Loren ipsum Loren ipsum', 'Its a best product XC', 1, '2020-08-19 04:18:47', '2020-08-19 04:18:47', NULL),
(6, 1, 14, 2, 6, 7, '3', 'XS34', 'Loren Ipsum Loren Ipsum Loren Ipsum', 'Its a best product XC', 1, '2020-08-19 04:21:01', '2020-08-19 04:21:01', NULL),
(7, 2, 14, 2, 7, 7, '1', 'QW324', 'Loren ipsum Loren ipsum', 'Its a best product Y', 1, '2020-08-19 04:22:28', '2020-08-19 04:22:28', NULL),
(8, 1, 21, 8, 9, 0, '2', 'A455', 'Lorem ipsum', 'Its a best product', 1, '2020-08-19 04:24:08', '2020-08-19 04:24:08', NULL),
(9, 1, 14, 4, 0, 0, '3', 'AS785', 'Loren ipsum', 'Its a best product', 1, '2020-08-19 04:28:53', '2020-08-19 04:28:53', NULL),
(10, 3, 14, 16, 10, 7, '2', 'AS345', 'lOREN IPSUM', 'Its a best product XC', 1, '2020-08-19 09:39:24', '2020-08-19 09:39:24', NULL),
(11, 1, 21, 16, 11, 8, '1', 'AW23', 'Loren', 'Its a best product Y', 1, '2020-08-19 09:41:31', '2020-08-19 09:41:31', NULL),
(12, 1, 14, 6, 0, 5, '789', 'ZS345', 'Testing Device', 'Its a best product XC', 1, '2020-08-22 23:20:26', '2020-08-22 23:20:26', NULL),
(13, 3, 14, 10, 3, 7, '145', 'ASW345', 'Material desc', 'material remark', 1, '2020-08-25 09:30:52', '2020-08-25 09:30:52', NULL),
(14, 4, 14, 2, 6, 5, '3', 'BXC', 'Sony', 'Its a best product', 1, '2020-10-13 21:07:25', '2020-10-13 21:30:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `material_requests`
--

CREATE TABLE `material_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `jobcodeid` int(11) NOT NULL,
  `requestno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodid` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `descripption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(10, 'App\\Service', 1, 'image', '5f40b1704e558_01', '5f40b1704e558_01.jpg', 'image/jpeg', 'public', 57606, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 2, '2020-08-22 00:21:35', '2020-08-22 00:21:46'),
(11, 'App\\Service', 2, 'image', '5f40b3593ca88_02', '5f40b3593ca88_02.jpg', 'image/jpeg', 'public', 99799, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 3, '2020-08-22 00:25:41', '2020-08-22 00:25:42'),
(12, 'App\\Service', 3, 'image', '5f40b3782f2a5_03', '5f40b3782f2a5_03.jpg', 'image/jpeg', 'public', 69789, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 4, '2020-08-22 00:26:10', '2020-08-22 00:26:12'),
(13, 'App\\Service', 4, 'image', '5f40b3a5d125e_04', '5f40b3a5d125e_04.jpg', 'image/jpeg', 'public', 95991, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 5, '2020-08-22 00:26:56', '2020-08-22 00:26:57'),
(15, 'App\\Service', 5, 'image', '5f40b42c014f5_05', '5f40b42c014f5_05.jpg', 'image/jpeg', 'public', 49004, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 6, '2020-08-22 00:29:15', '2020-08-22 00:29:16'),
(16, 'App\\Service', 6, 'image', '5f40b4929fc7b_06', '5f40b4929fc7b_06.jpg', 'image/jpeg', 'public', 81978, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 7, '2020-08-22 00:30:52', '2020-08-22 00:30:54'),
(17, 'App\\Service', 7, 'image', '5f40b4c84f2bc_07', '5f40b4c84f2bc_07.jpg', 'image/jpeg', 'public', 43853, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 8, '2020-08-22 00:31:49', '2020-08-22 00:31:50'),
(18, 'App\\Service', 8, 'image', '5f40b54254323_08', '5f40b54254323_08.jpg', 'image/jpeg', 'public', 73567, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 9, '2020-08-22 00:33:49', '2020-08-22 00:33:51'),
(19, 'App\\Certificate', 1, 'image', '5f40b74993490_c01', '5f40b74993490_c01.jpg', 'image/jpeg', 'public', 300848, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 10, '2020-08-22 00:42:29', '2020-08-22 00:42:31'),
(20, 'App\\Certificate', 2, 'image', '5f40b7df4bf60_c02', '5f40b7df4bf60_c02.jpg', 'image/jpeg', 'public', 303533, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 11, '2020-08-22 00:44:58', '2020-08-22 00:45:00'),
(21, 'App\\Certificate', 4, 'image', '5f40b7ee6c8ae_c03', '5f40b7ee6c8ae_c03.jpg', 'image/jpeg', 'public', 307795, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 12, '2020-08-22 00:45:13', '2020-08-22 00:45:18'),
(22, 'App\\Certificate', 3, 'image', '5f40b7ee6c8ae_c03', '5f40b7ee6c8ae_c03.jpg', 'image/jpeg', 'public', 307795, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 12, '2020-08-22 00:45:13', '2020-08-22 00:45:18'),
(23, 'App\\Certificate', 5, 'image', '5f40b7fced044_c04', '5f40b7fced044_c04.jpg', 'image/jpeg', 'public', 248890, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 13, '2020-08-22 00:45:27', '2020-08-22 00:45:32'),
(24, 'App\\Certificate', 1, 'image', '5f40b890b6619_c01', '5f40b890b6619_c01.jpg', 'image/jpeg', 'public', 300848, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 14, '2020-08-22 00:47:56', '2020-08-22 00:47:58'),
(25, 'App\\Certificate', 2, 'image', '5f40b8a86870d_c02', '5f40b8a86870d_c02.jpg', 'image/jpeg', 'public', 303533, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 15, '2020-08-22 00:48:20', '2020-08-22 00:48:22'),
(26, 'App\\Certificate', 3, 'image', '5f40b8c35e1cb_c03', '5f40b8c35e1cb_c03.jpg', 'image/jpeg', 'public', 307795, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 16, '2020-08-22 00:48:46', '2020-08-22 00:48:48'),
(27, 'App\\Certificate', 4, 'image', '5f40b8d7c9d41_c04', '5f40b8d7c9d41_c04.jpg', 'image/jpeg', 'public', 248890, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 17, '2020-08-22 00:49:07', '2020-08-22 00:49:09'),
(28, 'App\\Certificate', 5, 'image', '5f40b8ed1e3c9_c05', '5f40b8ed1e3c9_c05.jpg', 'image/jpeg', 'public', 48324, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 18, '2020-08-22 00:49:27', '2020-08-22 00:49:28'),
(30, 'App\\ContentManagementSystem', 2, 'image', '5f40be3943f5b_b01', '5f40be3943f5b_b01.png', 'image/png', 'public', 1013753, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 19, '2020-08-22 01:12:06', '2020-08-22 01:12:07'),
(31, 'App\\ContentManagementSystem', 1, 'image', '5f40bfc7712dd_chairman', '5f40bfc7712dd_chairman.jpg', 'image/jpeg', 'public', 73856, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 20, '2020-08-22 01:18:44', '2020-08-22 01:18:45'),
(32, 'App\\Download', 1, 'image', 'Brake-Fire-Profile', 'Brake-Fire-Profile.pdf', 'application/pdf', 'public', 2569391, '[]', '[]', '[]', 21, '2020-08-22 02:29:59', '2020-08-22 02:29:59'),
(33, 'App\\Download', 2, 'image', 'Sanitizer-gate-profile', 'Sanitizer-gate-profile.pdf', 'application/pdf', 'public', 2569391, '[]', '[]', '[]', 22, '2020-08-22 02:34:31', '2020-08-22 02:34:31'),
(34, 'App\\Download', 3, 'image', 'TELECOMMUNICATION-PROFILE', 'TELECOMMUNICATION-PROFILE.pdf', 'application/pdf', 'public', 2569391, '[]', '[]', '[]', 23, '2020-08-22 02:39:36', '2020-08-22 02:39:36'),
(35, 'App\\ContentManagementSystem', 4, 'image', '5f40d5f064c37_gas-banner', '5f40d5f064c37_gas-banner.jpg', 'image/jpeg', 'public', 171863, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 24, '2020-08-22 02:53:38', '2020-08-22 02:53:45'),
(36, 'App\\ContentManagementSystem', 5, 'image', '5f40d75a8f9bc_portables-Page', '5f40d75a8f9bc_portables-Page.jpg', 'image/jpeg', 'public', 241114, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 25, '2020-08-22 02:59:17', '2020-08-22 02:59:18'),
(37, 'App\\ContentManagementSystem', 6, 'image', '5f40d88b35faa_banner4', '5f40d88b35faa_banner4.jpg', 'image/jpeg', 'public', 648024, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 26, '2020-08-22 03:04:28', '2020-08-22 03:04:29'),
(40, 'App\\ContentManagementSystem', 7, 'image', '5f5df403d08c2_c01', '5f5df403d08c2_c01.jpg', 'image/jpeg', 'public', 300848, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 28, '2020-09-13 04:57:19', '2020-09-13 04:57:31'),
(41, 'App\\Event', 1, 'image', '5f5df43459e21_EID ULADHA', '5f5df43459e21_EID-ULADHA.jpg', 'image/jpeg', 'public', 576815, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 29, '2020-09-13 04:58:08', '2020-09-13 04:58:09'),
(42, 'App\\Event', 2, 'image', '5f5df6411e3eb_Eid-Mubarak-2020-images', '5f5df6411e3eb_Eid-Mubarak-2020-images.jpg', 'image/jpeg', 'public', 321147, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 30, '2020-09-13 05:07:53', '2020-09-13 05:08:00'),
(43, 'App\\Event', 3, 'image', '5f5df6d47ec28_RM_20191202_NATIONALDAYAUH_05', '5f5df6d47ec28_RM_20191202_NATIONALDAYAUH_05.jpg', 'image/jpeg', 'public', 275001, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 31, '2020-09-13 05:10:46', '2020-09-13 05:10:48'),
(44, 'App\\Banner', 1, 'image', '5f7bae8080754_img02', '5f7bae8080754_img02.jpg', 'image/jpeg', 'public', 19043, '[]', '{\"generated_conversions\":{\"thumb\":true,\"preview\":true}}', '[]', 32, '2020-10-05 18:08:43', '2020-10-05 18:08:54');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2020_07_10_000001_create_media_table', 1),
(8, '2020_07_10_000002_create_employees_table', 1),
(9, '2020_07_10_000003_create_sanatization_installations_table', 1),
(10, '2020_07_10_000004_create_g_maintinance_installations_table', 1),
(11, '2020_07_10_000005_create_electronics_installations_table', 1),
(12, '2020_07_10_000006_create_network_installations_table', 1),
(13, '2020_07_10_000007_create_gas_installations_table', 1),
(14, '2020_07_10_000008_create_telecom_installations_table', 1),
(15, '2020_07_10_000009_create_ac_installations_table', 1),
(16, '2020_07_10_000010_create_cctv_installations_table', 1),
(17, '2020_07_10_000011_create_customers_table', 1),
(18, '2020_07_10_000012_create_visits_table', 1),
(19, '2020_07_10_000013_create_complains_table', 1),
(20, '2020_07_10_000014_create_permissions_table', 1),
(21, '2020_07_10_000015_create_bookings_table', 1),
(22, '2020_07_10_000016_create_enquiries_table', 1),
(23, '2020_07_10_000017_create_fire_supplies_table', 1),
(24, '2020_07_10_000018_create_fire_maintenances_table', 1),
(25, '2020_07_10_000019_create_fire_installations_table', 1),
(26, '2020_07_10_000020_create_users_table', 1),
(27, '2020_07_10_000021_create_roles_table', 1),
(28, '2020_07_10_000022_create_cleaning_installations_table', 1),
(29, '2020_07_10_000023_create_role_user_pivot_table', 1),
(30, '2020_07_10_000024_create_brand_role_pivot_table', 1),
(31, '2020_07_10_000024_create_permission_role_pivot_table', 1),
(32, '2020_08_13_002013_create_service_type_table', 1),
(33, '2020_07_10_000024_create_estimation_table', 2),
(34, '2020_07_10_000024_create_estimationsize_table', 3),
(35, '2020_07_10_000024_create_material_table', 4),
(36, '2020_07_10_000024_create_servicetype_table', 5),
(37, '2020_08_18_000006_create_content_management_systems_table', 6),
(38, '2020_08_18_000007_create_categories_table', 7),
(39, '2020_08_18_000007_create_labourcost_table', 8),
(40, '2020_08_18_000007_create_amc_table', 9),
(41, '2020_08_18_000007_create_profit_table', 10),
(42, '2020_08_18_000007_create_boqtype_table', 11),
(43, '2020_08_18_000007_create_boqdetails_table', 12),
(44, '2020_08_18_000007_create_boqdetailstype_table', 13),
(45, '2020_08_18_000007_create_boqdetailsprod_table', 14),
(46, '2020_08_21_000008_create_services_table', 15),
(47, '2020_08_21_000009_create_certificates_table', 15),
(48, '2020_08_21_000010_create_downloads_table', 15),
(49, '2020_08_21_000011_create_website_configs_table', 16),
(50, '2020_08_22_000006_create_content_management_systems_table', 17),
(51, '2020_08_22_000008_create_services_table', 17),
(52, '2020_08_22_000012_create_events_table', 18),
(53, '2020_08_22_000013_create_teams_table', 18),
(54, '2020_08_22_000015_create_inspection_types_table', 19),
(55, '2020_08_22_000015_create_inspection_products_table', 20),
(56, '2020_08_22_000015_create_inspection_reports_table', 21),
(57, '2020_08_22_000015_create_inspection_report_products_table', 22),
(58, '2020_08_22_000015_create_jobcode_table', 23),
(59, '2020_08_22_000015_create_jobcodedetails_table', 23),
(60, '2020_08_28_000020_create_payment_modules_table', 24),
(61, '2020_08_29_000021_create_contactuses_table', 25),
(62, '2020_08_29_000022_create_pro_forma_invoices_table', 26),
(63, '2020_08_29_000022_create_pro_forma_invoice_items_table', 27),
(64, '2020_09_01_000003_create_material_requests_table', 28),
(65, '2020_09_01_000004_create_drawing_details_table', 28),
(66, '2020_09_01_000005_create_approve_drawings_table', 28),
(67, '2020_09_01_000026_create_email_templates_table', 28),
(68, '2020_09_05_000023_create_uae_location_lists_table', 29),
(69, '2020_09_14_000024_create_testimonials_table', 30),
(70, '2020_09_15_000005_create_our_services_table', 31),
(71, '2020_09_15_000006_create_our_projects_table', 31),
(72, '2020_09_15_000007_create_our_partners_table', 31),
(73, '2020_09_22_000008_create_careers_table', 32),
(74, '2020_09_27_000008_create_sms_table', 33),
(75, '2020_10_01_000005_create_user_complains_table', 34),
(76, '2020_10_05_000005_create_banners_table', 35),
(77, '2020_10_17_000005_create_labour_hours_table', 36);

-- --------------------------------------------------------

--
-- Table structure for table `network_installations`
--

CREATE TABLE `network_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `our_partners`
--

CREATE TABLE `our_partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `partners_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partners_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `our_projects`
--

CREATE TABLE `our_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `smart_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titlr_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_ar` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `our_services`
--

CREATE TABLE `our_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sh_content_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sh_content_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_content_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_content_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `our_services`
--

INSERT INTO `our_services` (`id`, `title_en`, `title_ar`, `sh_content_en`, `sh_content_ar`, `long_content_en`, `long_content_ar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Home Automation', 'أنظمة مكافحة الحريق', 'For the greater part of us, our house isn\'t only a place, it\'s the place.', 'أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق', 'For the greater part of us, our house isn\'t only a place, it\'s the place. For the greater part of us, our house isn\'t only a place, it\'s the place. For the greater part of us, our house isn\'t only a place, it\'s the place.', 'أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق أنظمة مكافحة الحريق', '2020-09-17 14:00:16', '2020-09-17 14:01:01', NULL),
(2, 'CCTV Design House', 'بيت تصميم الدوائر التلفزيونية المغلقة', 'For the greater part of us, our house isn\'t only a place, it\'s the place.', 'بالنسبة للجزء الأكبر منا ، منزلنا ليس مجرد مكان ، إنه المكان.', 'For the greater part of us, our house isn\'t only a place, it\'s the place.', 'بالنسبة للجزء الأكبر منا ، منزلنا ليس مجرد مكان ، إنه المكان.', '2020-09-18 20:59:06', '2020-09-18 20:59:06', NULL),
(3, 'Cabinet Alarm', 'إنذار مجلس الوزراء', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', '2020-09-18 21:00:01', '2020-09-18 21:00:01', NULL),
(4, 'Access System', 'نظام الوصول', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', '2020-09-18 21:00:55', '2020-09-18 21:00:55', NULL),
(5, 'Fire Alarm & Life Safety', 'إنذار الحريق وسلامة الحياة', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', '2020-09-18 21:02:32', '2020-09-18 21:02:32', NULL),
(6, 'Commercial Video Security', 'أمن الفيديو التجاري', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', '2020-09-18 21:03:21', '2020-09-18 21:03:21', NULL),
(7, 'Video intercom System', 'نظام الاتصال الداخلي عبر الفيديو', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', '2020-09-18 21:04:14', '2020-09-18 21:04:14', NULL),
(8, 'Central Station Monitoring', 'مراقبة المحطة المركزية', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', 'Monitor your home or property using our reliable mobile applications.', 'راقب منزلك أو ممتلكاتك باستخدام تطبيقاتنا المحمولة الموثوقة.', '2020-09-18 21:05:08', '2020-09-18 21:05:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_modules`
--

CREATE TABLE `payment_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `jobcodeid` int(11) NOT NULL,
  `payment_amount` decimal(15,2) DEFAULT NULL,
  `civil_defence_fee` decimal(15,2) DEFAULT NULL,
  `received_amount` decimal(15,2) DEFAULT NULL,
  `balanced_amount` decimal(15,2) DEFAULT NULL,
  `vat` decimal(10,0) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_modules`
--

INSERT INTO `payment_modules` (`id`, `jobcodeid`, `payment_amount`, `civil_defence_fee`, `received_amount`, `balanced_amount`, `vat`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 7, '34543.00', '4354.00', '454.00', '45.00', '33', '2020-10-06 21:01:51', '2020-10-06 21:01:51', NULL),
(7, 6, '159.00', '41.00', '100.00', '100.00', '12', '2020-10-06 21:12:11', '2020-10-06 21:12:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(43, 'enquiry_create', NULL, NULL, NULL),
(44, 'enquiry_edit', NULL, NULL, NULL),
(45, 'enquiry_show', NULL, NULL, NULL),
(46, 'enquiry_delete', NULL, NULL, NULL),
(47, 'enquiry_access', NULL, NULL, NULL),
(58, 'complain_create', NULL, NULL, NULL),
(59, 'complain_edit', NULL, NULL, NULL),
(60, 'complain_show', NULL, NULL, NULL),
(61, 'complain_delete', NULL, NULL, NULL),
(62, 'complain_access', NULL, NULL, NULL),
(68, 'customer_create', NULL, NULL, NULL),
(69, 'customer_edit', NULL, NULL, NULL),
(70, 'customer_show', NULL, NULL, NULL),
(71, 'customer_delete', NULL, NULL, NULL),
(72, 'customer_access', NULL, NULL, NULL),
(118, 'profile_password_edit', NULL, NULL, NULL),
(119, 'brand_access', NULL, NULL, NULL),
(120, 'brand_delete', NULL, NULL, NULL),
(121, 'brand_show', NULL, NULL, NULL),
(122, 'brand_edit', NULL, NULL, NULL),
(123, 'brand_create', NULL, NULL, NULL),
(124, 'estimation_access', NULL, NULL, NULL),
(125, 'estimation_delete', NULL, NULL, NULL),
(126, 'estimation_show', NULL, NULL, NULL),
(127, 'estimation_edit', NULL, NULL, NULL),
(128, 'estimation_create', NULL, NULL, NULL),
(129, 'estimationsize_access', NULL, NULL, NULL),
(130, 'estimationsize_delete', NULL, NULL, NULL),
(131, 'estimationsize_show', NULL, NULL, NULL),
(132, 'estimationsize_edit', NULL, NULL, NULL),
(133, 'estimationsize_create', NULL, NULL, NULL),
(134, 'servicetype_access', NULL, NULL, NULL),
(135, 'servicetype_delete', NULL, NULL, NULL),
(136, 'servicetype_show', NULL, NULL, NULL),
(137, 'servicetype_edit', NULL, NULL, NULL),
(138, 'servicetype_create', NULL, NULL, NULL),
(139, 'material_access', NULL, NULL, NULL),
(140, 'material_delete', NULL, NULL, NULL),
(141, 'material_show', NULL, NULL, NULL),
(142, 'material_edit', NULL, NULL, NULL),
(143, 'material_create', NULL, NULL, NULL),
(144, 'content_management_system_access', NULL, NULL, NULL),
(145, 'content_management_system_delete', NULL, NULL, NULL),
(146, 'content_management_system_show', NULL, NULL, NULL),
(147, 'content_management_system_edit', NULL, NULL, NULL),
(148, 'content_management_system_create', NULL, NULL, NULL),
(149, 'category_access', NULL, NULL, NULL),
(150, 'category_delete', NULL, NULL, NULL),
(151, 'category_show', NULL, NULL, NULL),
(152, 'category_edit', NULL, NULL, NULL),
(153, 'category_create', NULL, NULL, NULL),
(154, 'website_config_access', NULL, NULL, NULL),
(155, 'website_config_delete', NULL, NULL, NULL),
(156, 'website_config_show', NULL, NULL, NULL),
(157, 'website_config_edit', NULL, NULL, NULL),
(158, 'website_config_create', NULL, NULL, NULL),
(159, 'service_access', NULL, NULL, NULL),
(160, 'service_delete', NULL, NULL, NULL),
(161, 'service_show', NULL, NULL, NULL),
(162, 'service_edit', NULL, NULL, NULL),
(163, 'service_create', NULL, NULL, NULL),
(169, 'download_access', NULL, NULL, NULL),
(170, 'download_delete', NULL, NULL, NULL),
(171, 'download_show', NULL, NULL, NULL),
(172, 'download_edit', NULL, NULL, NULL),
(173, 'download_create', NULL, NULL, NULL),
(174, 'event_access', NULL, NULL, NULL),
(175, 'event_delete', NULL, NULL, NULL),
(176, 'event_show', NULL, NULL, NULL),
(177, 'event_edit', NULL, NULL, NULL),
(178, 'event_create', NULL, NULL, NULL),
(184, 'inspection_type_access', NULL, NULL, NULL),
(185, 'inspection_type_delete', NULL, NULL, NULL),
(186, 'inspection_type_show', NULL, NULL, NULL),
(187, 'inspection_type_edit', NULL, NULL, NULL),
(188, 'inspection_type_create', NULL, NULL, NULL),
(189, 'inspection_report_access', NULL, NULL, NULL),
(190, 'inspection_report_delete', NULL, NULL, NULL),
(191, 'inspection_report_show', NULL, NULL, NULL),
(192, 'inspection_report_edit', NULL, NULL, NULL),
(193, 'inspection_report_create', NULL, NULL, NULL),
(194, 'job_code_access', NULL, NULL, NULL),
(195, 'job_code_delete', NULL, NULL, NULL),
(196, 'job_code_show', NULL, NULL, NULL),
(197, 'job_code_edit', NULL, NULL, NULL),
(198, 'job_code_create', NULL, NULL, NULL),
(199, 'jobcode_detail_access', NULL, NULL, NULL),
(200, 'jobcode_detail_delete', NULL, NULL, NULL),
(201, 'jobcode_detail_show', NULL, NULL, NULL),
(202, 'jobcode_detail_edit', NULL, NULL, NULL),
(203, 'jobcode_detail_create', NULL, NULL, NULL),
(204, 'payment_module_access', NULL, NULL, NULL),
(205, 'payment_module_delete', NULL, NULL, NULL),
(206, 'payment_module_show', NULL, NULL, NULL),
(207, 'payment_module_edit', NULL, NULL, NULL),
(208, 'payment_module_create', NULL, NULL, NULL),
(209, 'contact_us_access', NULL, NULL, NULL),
(210, 'contact_us_delete', NULL, NULL, NULL),
(211, 'contact_us_show', NULL, NULL, NULL),
(212, 'contact_us_edit', NULL, NULL, NULL),
(213, 'contact_us_create', NULL, NULL, NULL),
(214, 'contact_us_access', NULL, NULL, NULL),
(215, 'contact_us_delete', NULL, NULL, NULL),
(216, 'contact_us_show', NULL, NULL, NULL),
(217, 'contact_us_edit', NULL, NULL, NULL),
(218, 'contact_us_create', NULL, NULL, NULL),
(219, 'pro_forma_invoice_access', NULL, NULL, NULL),
(220, 'pro_forma_invoice_delete', NULL, NULL, NULL),
(221, 'pro_forma_invoice_show', NULL, NULL, NULL),
(222, 'pro_forma_invoice_edit', NULL, NULL, NULL),
(223, 'pro_forma_invoice_create', NULL, NULL, NULL),
(224, 'approve_drawing_access', NULL, NULL, NULL),
(225, 'approve_drawing_delete', NULL, NULL, NULL),
(226, 'approve_drawing_show', NULL, NULL, NULL),
(227, 'approve_drawing_edit', NULL, NULL, NULL),
(228, 'approve_drawing_create', NULL, NULL, NULL),
(229, 'drawing_detail_access', NULL, NULL, NULL),
(230, 'drawing_detail_delete', NULL, NULL, NULL),
(231, 'drawing_detail_show', NULL, NULL, NULL),
(232, 'drawing_detail_edit', NULL, NULL, NULL),
(233, 'drawing_detail_create', NULL, NULL, NULL),
(234, 'material_request_access', NULL, NULL, NULL),
(235, 'material_request_delete', NULL, NULL, NULL),
(236, 'material_request_show', NULL, NULL, NULL),
(237, 'material_request_edit', NULL, NULL, NULL),
(238, 'material_request_create', NULL, NULL, NULL),
(239, 'email_template_access', NULL, NULL, NULL),
(240, 'email_template_delete', NULL, NULL, NULL),
(241, 'email_template_show', NULL, NULL, NULL),
(242, 'email_template_edit', NULL, NULL, NULL),
(243, 'email_template_create', NULL, NULL, NULL),
(244, 'uae_location_list_access', NULL, NULL, NULL),
(245, 'uae_location_list_delete', NULL, NULL, NULL),
(246, 'uae_location_list_show', NULL, NULL, NULL),
(247, 'uae_location_list_edit', NULL, NULL, NULL),
(248, 'uae_location_list_create', NULL, NULL, NULL),
(249, 'our_service_access', NULL, NULL, NULL),
(250, 'our_service_delete', NULL, NULL, NULL),
(251, 'our_service_show', NULL, NULL, NULL),
(252, 'our_service_edit', NULL, NULL, NULL),
(253, 'our_service_create', NULL, NULL, NULL),
(254, 'our_project_access', NULL, NULL, NULL),
(255, 'our_project_delete', NULL, NULL, NULL),
(256, 'our_project_show', NULL, NULL, NULL),
(257, 'our_project_edit', NULL, NULL, NULL),
(258, 'our_project_create', NULL, NULL, NULL),
(259, 'our_partner_access', NULL, NULL, NULL),
(260, 'our_partner_delete', NULL, NULL, NULL),
(261, 'our_partner_show', NULL, NULL, NULL),
(262, 'our_partner_edit', NULL, NULL, NULL),
(263, 'our_partner_create', NULL, NULL, NULL),
(264, 'testimonial_access', NULL, NULL, NULL),
(265, 'testimonial_delete', NULL, NULL, NULL),
(266, 'testimonial_show', NULL, NULL, NULL),
(267, 'testimonial_edit', NULL, NULL, NULL),
(268, 'testimonial_create', NULL, NULL, NULL),
(269, 'boq_access', '2020-10-03 06:00:50', '2020-10-03 06:00:50', NULL),
(270, 'boq_edit', '2020-10-03 06:01:56', '2020-10-03 06:01:56', NULL),
(271, 'user_complains_access', NULL, NULL, NULL),
(272, 'user_complains_delete', NULL, NULL, NULL),
(273, 'user_complains_show', NULL, NULL, NULL),
(274, 'user_complains_edit', NULL, NULL, NULL),
(275, 'user_complains_create', NULL, NULL, NULL),
(276, 'banner_access', NULL, NULL, NULL),
(277, 'banner_delete', NULL, NULL, NULL),
(278, 'banner_show', NULL, NULL, NULL),
(279, 'banner_edit', NULL, NULL, NULL),
(280, 'banner_create', NULL, NULL, NULL),
(281, 'profit_create', NULL, NULL, NULL),
(282, 'profit_edit', NULL, NULL, NULL),
(283, 'profit_show', NULL, NULL, NULL),
(284, 'profit_delete', NULL, NULL, NULL),
(285, 'profit_access', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions_bk`
--

CREATE TABLE `permissions_bk` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions_bk`
--

INSERT INTO `permissions_bk` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(26, 'fire_maintenance_access', NULL, NULL, NULL),
(27, 'fire_supply_create', NULL, NULL, NULL),
(28, 'fire_supply_edit', NULL, NULL, NULL),
(29, 'fire_supply_show', NULL, NULL, NULL),
(30, 'fire_supply_delete', NULL, NULL, NULL),
(31, 'fire_supply_access', NULL, NULL, NULL),
(32, 'fire_service_access', NULL, NULL, NULL),
(33, 'cc_tv_access', NULL, NULL, NULL),
(34, 'sanatization_access', NULL, NULL, NULL),
(35, 'ac_service_access', NULL, NULL, NULL),
(36, 'telecom_service_access', NULL, NULL, NULL),
(37, 'cleaning_service_access', NULL, NULL, NULL),
(38, 'gas_service_access', NULL, NULL, NULL),
(39, 'network_service_access', NULL, NULL, NULL),
(40, 'g_maintinance_service_access', NULL, NULL, NULL),
(41, 'electronics_service_access', NULL, NULL, NULL),
(42, 'staff_department_access', NULL, NULL, NULL),
(43, 'enquiry_create', NULL, NULL, NULL),
(44, 'enquiry_edit', NULL, NULL, NULL),
(45, 'enquiry_show', NULL, NULL, NULL),
(46, 'enquiry_delete', NULL, NULL, NULL),
(47, 'enquiry_access', NULL, NULL, NULL),
(48, 'booking_create', NULL, NULL, NULL),
(49, 'booking_edit', NULL, NULL, NULL),
(50, 'booking_show', NULL, NULL, NULL),
(51, 'booking_delete', NULL, NULL, NULL),
(52, 'booking_access', NULL, NULL, NULL),
(53, 'visit_create', NULL, NULL, NULL),
(54, 'visit_edit', NULL, NULL, NULL),
(55, 'visit_show', NULL, NULL, NULL),
(56, 'visit_delete', NULL, NULL, NULL),
(57, 'visit_access', NULL, NULL, NULL),
(58, 'complain_create', NULL, NULL, NULL),
(59, 'complain_edit', NULL, NULL, NULL),
(60, 'complain_show', NULL, NULL, NULL),
(61, 'complain_delete', NULL, NULL, NULL),
(62, 'complain_access', NULL, NULL, NULL),
(63, 'employee_create', NULL, NULL, NULL),
(64, 'employee_edit', NULL, NULL, NULL),
(65, 'employee_show', NULL, NULL, NULL),
(66, 'employee_delete', NULL, NULL, NULL),
(67, 'employee_access', NULL, NULL, NULL),
(68, 'customer_create', NULL, NULL, NULL),
(69, 'customer_edit', NULL, NULL, NULL),
(70, 'customer_show', NULL, NULL, NULL),
(71, 'customer_delete', NULL, NULL, NULL),
(72, 'customer_access', NULL, NULL, NULL),
(73, 'cctv_installation_create', NULL, NULL, NULL),
(74, 'cctv_installation_edit', NULL, NULL, NULL),
(75, 'cctv_installation_show', NULL, NULL, NULL),
(76, 'cctv_installation_delete', NULL, NULL, NULL),
(77, 'cctv_installation_access', NULL, NULL, NULL),
(78, 'ac_installation_create', NULL, NULL, NULL),
(79, 'ac_installation_edit', NULL, NULL, NULL),
(80, 'ac_installation_show', NULL, NULL, NULL),
(81, 'ac_installation_delete', NULL, NULL, NULL),
(82, 'ac_installation_access', NULL, NULL, NULL),
(83, 'telecom_installation_create', NULL, NULL, NULL),
(84, 'telecom_installation_edit', NULL, NULL, NULL),
(85, 'telecom_installation_show', NULL, NULL, NULL),
(86, 'telecom_installation_delete', NULL, NULL, NULL),
(87, 'telecom_installation_access', NULL, NULL, NULL),
(88, 'gas_installation_create', NULL, NULL, NULL),
(89, 'gas_installation_edit', NULL, NULL, NULL),
(90, 'gas_installation_show', NULL, NULL, NULL),
(91, 'gas_installation_delete', NULL, NULL, NULL),
(92, 'gas_installation_access', NULL, NULL, NULL),
(93, 'network_installation_create', NULL, NULL, NULL),
(94, 'network_installation_edit', NULL, NULL, NULL),
(95, 'network_installation_show', NULL, NULL, NULL),
(96, 'network_installation_delete', NULL, NULL, NULL),
(97, 'network_installation_access', NULL, NULL, NULL),
(98, 'electronics_installation_create', NULL, NULL, NULL),
(99, 'electronics_installation_edit', NULL, NULL, NULL),
(100, 'electronics_installation_show', NULL, NULL, NULL),
(101, 'electronics_installation_delete', NULL, NULL, NULL),
(102, 'electronics_installation_access', NULL, NULL, NULL),
(103, 'g_maintinance_installation_create', NULL, NULL, NULL),
(104, 'g_maintinance_installation_edit', NULL, NULL, NULL),
(105, 'g_maintinance_installation_show', NULL, NULL, NULL),
(106, 'g_maintinance_installation_delete', NULL, NULL, NULL),
(107, 'g_maintinance_installation_access', NULL, NULL, NULL),
(108, 'sanatization_installation_create', NULL, NULL, NULL),
(109, 'sanatization_installation_edit', NULL, NULL, NULL),
(110, 'sanatization_installation_show', NULL, NULL, NULL),
(111, 'sanatization_installation_delete', NULL, NULL, NULL),
(112, 'sanatization_installation_access', NULL, NULL, NULL),
(113, 'cleaning_installation_create', NULL, NULL, NULL),
(114, 'cleaning_installation_edit', NULL, NULL, NULL),
(115, 'cleaning_installation_show', NULL, NULL, NULL),
(116, 'cleaning_installation_delete', NULL, NULL, NULL),
(117, 'cleaning_installation_access', NULL, NULL, NULL),
(118, 'profile_password_edit', NULL, NULL, NULL),
(119, 'brand_access', NULL, NULL, NULL),
(120, 'brand_delete', NULL, NULL, NULL),
(121, 'brand_show', NULL, NULL, NULL),
(122, 'brand_edit', NULL, NULL, NULL),
(123, 'brand_create', NULL, NULL, NULL),
(124, 'estimation_access', NULL, NULL, NULL),
(125, 'estimation_delete', NULL, NULL, NULL),
(126, 'estimation_show', NULL, NULL, NULL),
(127, 'estimation_edit', NULL, NULL, NULL),
(128, 'estimation_create', NULL, NULL, NULL),
(129, 'estimationsize_access', NULL, NULL, NULL),
(130, 'estimationsize_delete', NULL, NULL, NULL),
(131, 'estimationsize_show', NULL, NULL, NULL),
(132, 'estimationsize_edit', NULL, NULL, NULL),
(133, 'estimationsize_create', NULL, NULL, NULL),
(134, 'servicetype_access', NULL, NULL, NULL),
(135, 'servicetype_delete', NULL, NULL, NULL),
(136, 'servicetype_show', NULL, NULL, NULL),
(137, 'servicetype_edit', NULL, NULL, NULL),
(138, 'servicetype_create', NULL, NULL, NULL),
(139, 'material_access', NULL, NULL, NULL),
(140, 'material_delete', NULL, NULL, NULL),
(141, 'material_show', NULL, NULL, NULL),
(142, 'material_edit', NULL, NULL, NULL),
(143, 'material_create', NULL, NULL, NULL),
(144, 'content_management_system_access', NULL, NULL, NULL),
(145, 'content_management_system_delete', NULL, NULL, NULL),
(146, 'content_management_system_show', NULL, NULL, NULL),
(147, 'content_management_system_edit', NULL, NULL, NULL),
(148, 'content_management_system_create', NULL, NULL, NULL),
(149, 'category_access', NULL, NULL, NULL),
(150, 'category_delete', NULL, NULL, NULL),
(151, 'category_show', NULL, NULL, NULL),
(152, 'category_edit', NULL, NULL, NULL),
(153, 'category_create', NULL, NULL, NULL),
(154, 'website_config_access', NULL, NULL, NULL),
(155, 'website_config_delete', NULL, NULL, NULL),
(156, 'website_config_show', NULL, NULL, NULL),
(157, 'website_config_edit', NULL, NULL, NULL),
(158, 'website_config_create', NULL, NULL, NULL),
(159, 'service_access', NULL, NULL, NULL),
(160, 'service_delete', NULL, NULL, NULL),
(161, 'service_show', NULL, NULL, NULL),
(162, 'service_edit', NULL, NULL, NULL),
(163, 'service_create', NULL, NULL, NULL),
(164, 'certificate_access', NULL, NULL, NULL),
(165, 'certificate_delete', NULL, NULL, NULL),
(166, 'certificate_show', NULL, NULL, NULL),
(167, 'certificate_edit', NULL, NULL, NULL),
(168, 'certificate_create', NULL, NULL, NULL),
(169, 'download_access', NULL, NULL, NULL),
(170, 'download_delete', NULL, NULL, NULL),
(171, 'download_show', NULL, NULL, NULL),
(172, 'download_edit', NULL, NULL, NULL),
(173, 'download_create', NULL, NULL, NULL),
(174, 'event_access', NULL, NULL, NULL),
(175, 'event_delete', NULL, NULL, NULL),
(176, 'event_show', NULL, NULL, NULL),
(177, 'event_edit', NULL, NULL, NULL),
(178, 'event_create', NULL, NULL, NULL),
(179, 'team_access', NULL, NULL, NULL),
(180, 'team_delete', NULL, NULL, NULL),
(181, 'team_show', NULL, NULL, NULL),
(182, 'team_edit', NULL, NULL, NULL),
(183, 'team_create', NULL, NULL, NULL),
(184, 'inspection_type_access', NULL, NULL, NULL),
(185, 'inspection_type_delete', NULL, NULL, NULL),
(186, 'inspection_type_show', NULL, NULL, NULL),
(187, 'inspection_type_edit', NULL, NULL, NULL),
(188, 'inspection_type_create', NULL, NULL, NULL),
(189, 'inspection_report_access', NULL, NULL, NULL),
(190, 'inspection_report_delete', NULL, NULL, NULL),
(191, 'inspection_report_show', NULL, NULL, NULL),
(192, 'inspection_report_edit', NULL, NULL, NULL),
(193, 'inspection_report_create', NULL, NULL, NULL),
(194, 'job_code_access', NULL, NULL, NULL),
(195, 'job_code_delete', NULL, NULL, NULL),
(196, 'job_code_show', NULL, NULL, NULL),
(197, 'job_code_edit', NULL, NULL, NULL),
(198, 'job_code_create', NULL, NULL, NULL),
(199, 'jobcode_detail_access', NULL, NULL, NULL),
(200, 'jobcode_detail_delete', NULL, NULL, NULL),
(201, 'jobcode_detail_show', NULL, NULL, NULL),
(202, 'jobcode_detail_edit', NULL, NULL, NULL),
(203, 'jobcode_detail_create', NULL, NULL, NULL),
(204, 'payment_module_access', NULL, NULL, NULL),
(205, 'payment_module_delete', NULL, NULL, NULL),
(206, 'payment_module_show', NULL, NULL, NULL),
(207, 'payment_module_edit', NULL, NULL, NULL),
(208, 'payment_module_create', NULL, NULL, NULL),
(209, 'contact_us_access', NULL, NULL, NULL),
(210, 'contact_us_delete', NULL, NULL, NULL),
(211, 'contact_us_show', NULL, NULL, NULL),
(212, 'contact_us_edit', NULL, NULL, NULL),
(213, 'contact_us_create', NULL, NULL, NULL),
(214, 'contact_us_access', NULL, NULL, NULL),
(215, 'contact_us_delete', NULL, NULL, NULL),
(216, 'contact_us_show', NULL, NULL, NULL),
(217, 'contact_us_edit', NULL, NULL, NULL),
(218, 'contact_us_create', NULL, NULL, NULL),
(219, 'pro_forma_invoice_access', NULL, NULL, NULL),
(220, 'pro_forma_invoice_delete', NULL, NULL, NULL),
(221, 'pro_forma_invoice_show', NULL, NULL, NULL),
(222, 'pro_forma_invoice_edit', NULL, NULL, NULL),
(223, 'pro_forma_invoice_create', NULL, NULL, NULL),
(224, 'approve_drawing_access', NULL, NULL, NULL),
(225, 'approve_drawing_delete', NULL, NULL, NULL),
(226, 'approve_drawing_show', NULL, NULL, NULL),
(227, 'approve_drawing_edit', NULL, NULL, NULL),
(228, 'approve_drawing_create', NULL, NULL, NULL),
(229, 'drawing_detail_access', NULL, NULL, NULL),
(230, 'drawing_detail_delete', NULL, NULL, NULL),
(231, 'drawing_detail_show', NULL, NULL, NULL),
(232, 'drawing_detail_edit', NULL, NULL, NULL),
(233, 'drawing_detail_create', NULL, NULL, NULL),
(234, 'material_request_access', NULL, NULL, NULL),
(235, 'material_request_delete', NULL, NULL, NULL),
(236, 'material_request_show', NULL, NULL, NULL),
(237, 'material_request_edit', NULL, NULL, NULL),
(238, 'material_request_create', NULL, NULL, NULL),
(239, 'email_template_access', NULL, NULL, NULL),
(240, 'email_template_delete', NULL, NULL, NULL),
(241, 'email_template_show', NULL, NULL, NULL),
(242, 'email_template_edit', NULL, NULL, NULL),
(243, 'email_template_create', NULL, NULL, NULL),
(244, 'uae_location_list_access', NULL, NULL, NULL),
(245, 'uae_location_list_delete', NULL, NULL, NULL),
(246, 'uae_location_list_show', NULL, NULL, NULL),
(247, 'uae_location_list_edit', NULL, NULL, NULL),
(248, 'uae_location_list_create', NULL, NULL, NULL),
(249, 'our_service_access', NULL, NULL, NULL),
(250, 'our_service_delete', NULL, NULL, NULL),
(251, 'our_service_show', NULL, NULL, NULL),
(252, 'our_service_edit', NULL, NULL, NULL),
(253, 'our_service_create', NULL, NULL, NULL),
(254, 'our_project_access', NULL, NULL, NULL),
(255, 'our_project_delete', NULL, NULL, NULL),
(256, 'our_project_show', NULL, NULL, NULL),
(257, 'our_project_edit', NULL, NULL, NULL),
(258, 'our_project_create', NULL, NULL, NULL),
(259, 'our_partner_access', NULL, NULL, NULL),
(260, 'our_partner_delete', NULL, NULL, NULL),
(261, 'our_partner_show', NULL, NULL, NULL),
(262, 'our_partner_edit', NULL, NULL, NULL),
(263, 'our_partner_create', NULL, NULL, NULL),
(264, 'testimonial_access', NULL, NULL, NULL),
(265, 'testimonial_delete', NULL, NULL, NULL),
(266, 'testimonial_show', NULL, NULL, NULL),
(267, 'testimonial_edit', NULL, NULL, NULL),
(268, 'testimonial_create', NULL, NULL, NULL),
(269, 'boq_access', '2020-10-03 06:00:50', '2020-10-03 06:00:50', NULL),
(270, 'boq_edit', '2020-10-03 06:01:56', '2020-10-03 06:01:56', NULL),
(271, 'user_complains_access', NULL, NULL, NULL),
(272, 'user_complains_delete', NULL, NULL, NULL),
(273, 'user_complains_show', NULL, NULL, NULL),
(274, 'user_complains_edit', NULL, NULL, NULL),
(275, 'user_complains_create', NULL, NULL, NULL),
(276, 'banner_access', NULL, NULL, NULL),
(277, 'banner_delete', NULL, NULL, NULL),
(278, 'banner_show', NULL, NULL, NULL),
(279, 'banner_edit', NULL, NULL, NULL),
(280, 'banner_create', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 12),
(1, 13),
(1, 15),
(1, 16),
(1, 43),
(1, 44),
(1, 46),
(1, 47),
(1, 68),
(1, 69),
(1, 71),
(1, 72),
(1, 119),
(1, 120),
(1, 122),
(1, 123),
(1, 124),
(1, 125),
(1, 127),
(1, 128),
(1, 129),
(1, 130),
(1, 132),
(1, 133),
(1, 139),
(1, 140),
(1, 142),
(1, 143),
(1, 134),
(1, 135),
(1, 137),
(1, 138),
(1, 144),
(1, 145),
(1, 147),
(1, 148),
(1, 149),
(1, 150),
(1, 152),
(1, 153),
(1, 154),
(1, 155),
(1, 157),
(1, 158),
(1, 159),
(1, 160),
(1, 162),
(1, 163),
(1, 169),
(1, 170),
(1, 172),
(1, 173),
(1, 174),
(1, 175),
(1, 177),
(1, 178),
(1, 184),
(1, 185),
(1, 187),
(1, 188),
(1, 189),
(1, 190),
(1, 192),
(1, 193),
(3, 43),
(3, 44),
(3, 45),
(3, 46),
(3, 47),
(4, 12),
(4, 13),
(4, 14),
(4, 15),
(4, 16),
(4, 43),
(4, 44),
(4, 45),
(4, 46),
(4, 47),
(4, 58),
(4, 59),
(4, 60),
(4, 61),
(4, 62),
(4, 68),
(4, 69),
(4, 70),
(4, 71),
(4, 72),
(4, 118),
(4, 119),
(4, 120),
(4, 121),
(4, 122),
(4, 123),
(4, 124),
(4, 125),
(4, 126),
(4, 127),
(4, 128),
(4, 129),
(4, 130),
(4, 131),
(4, 132),
(4, 133),
(4, 134),
(4, 135),
(4, 136),
(4, 137),
(4, 138),
(4, 139),
(4, 140),
(4, 141),
(4, 142),
(4, 143),
(4, 144),
(4, 145),
(4, 146),
(4, 147),
(4, 148),
(4, 149),
(4, 150),
(4, 151),
(4, 152),
(4, 153),
(4, 154),
(4, 155),
(4, 156),
(4, 157),
(4, 158),
(4, 159),
(4, 160),
(4, 161),
(4, 162),
(4, 163),
(4, 169),
(4, 170),
(4, 171),
(4, 172),
(4, 173),
(4, 174),
(4, 175),
(4, 176),
(4, 177),
(4, 178),
(4, 184),
(4, 185),
(4, 186),
(4, 187),
(4, 188),
(4, 189),
(4, 190),
(4, 191),
(4, 192),
(4, 193),
(5, 12),
(4, 1),
(5, 1),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 7),
(6, 8),
(6, 9),
(6, 10),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 16),
(6, 43),
(6, 44),
(6, 45),
(6, 46),
(6, 47),
(6, 58),
(6, 59),
(6, 60),
(6, 61),
(6, 62),
(6, 68),
(6, 69),
(6, 70),
(6, 71),
(6, 72),
(6, 118),
(6, 119),
(6, 120),
(6, 121),
(6, 122),
(6, 123),
(6, 124),
(6, 125),
(6, 126),
(6, 127),
(6, 128),
(6, 129),
(6, 130),
(6, 131),
(6, 132),
(6, 133),
(6, 134),
(6, 135),
(6, 136),
(6, 137),
(6, 138),
(6, 139),
(6, 140),
(6, 141),
(6, 142),
(6, 143),
(6, 144),
(6, 145),
(6, 146),
(6, 147),
(6, 148),
(6, 149),
(6, 150),
(6, 151),
(6, 152),
(6, 153),
(6, 154),
(6, 155),
(6, 156),
(6, 157),
(6, 158),
(6, 159),
(6, 160),
(6, 161),
(6, 162),
(6, 163),
(6, 169),
(6, 170),
(6, 171),
(6, 172),
(6, 173),
(6, 174),
(6, 175),
(6, 176),
(6, 177),
(6, 178),
(6, 184),
(6, 185),
(6, 186),
(6, 187),
(6, 188),
(6, 189),
(6, 190),
(6, 191),
(6, 192),
(6, 193),
(1, 194),
(1, 195),
(1, 197),
(1, 198),
(1, 199),
(1, 200),
(1, 202),
(1, 203),
(1, 204),
(1, 205),
(1, 207),
(1, 208),
(5, 2),
(5, 3),
(5, 4),
(5, 8),
(5, 9),
(5, 10),
(5, 11),
(6, 6),
(6, 11),
(6, 194),
(6, 195),
(6, 196),
(6, 197),
(6, 198),
(6, 199),
(6, 200),
(6, 201),
(6, 202),
(6, 203),
(6, 204),
(6, 205),
(6, 206),
(6, 207),
(6, 208),
(6, 209),
(6, 210),
(6, 211),
(6, 212),
(6, 213),
(6, 214),
(6, 215),
(6, 216),
(6, 217),
(6, 218),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 194),
(4, 195),
(4, 196),
(4, 197),
(4, 198),
(4, 199),
(4, 200),
(4, 201),
(4, 202),
(4, 203),
(4, 204),
(4, 205),
(4, 206),
(4, 207),
(4, 208),
(4, 209),
(4, 210),
(4, 211),
(4, 212),
(4, 213),
(4, 214),
(4, 215),
(4, 216),
(4, 217),
(4, 218),
(1, 224),
(1, 225),
(1, 227),
(1, 228),
(1, 229),
(1, 230),
(1, 232),
(1, 233),
(1, 234),
(1, 235),
(1, 237),
(1, 238),
(1, 239),
(1, 240),
(1, 242),
(1, 243),
(3, 224),
(3, 225),
(3, 226),
(3, 227),
(3, 228),
(3, 229),
(3, 230),
(3, 231),
(3, 232),
(3, 233),
(3, 234),
(3, 235),
(3, 236),
(3, 237),
(3, 238),
(3, 239),
(3, 240),
(3, 241),
(3, 242),
(3, 243),
(1, 244),
(1, 245),
(1, 247),
(1, 248),
(1, 249),
(1, 250),
(1, 252),
(1, 253),
(1, 254),
(1, 255),
(1, 257),
(1, 258),
(1, 259),
(1, 260),
(1, 262),
(1, 263),
(1, 264),
(1, 265),
(1, 267),
(1, 268),
(7, 44),
(7, 45),
(7, 47),
(7, 269),
(1, 271),
(1, 272),
(1, 274),
(1, 275),
(5, 272),
(5, 273),
(5, 274),
(5, 5),
(5, 6),
(5, 7),
(5, 13),
(5, 14),
(5, 15),
(5, 16),
(5, 43),
(5, 44),
(5, 45),
(5, 46),
(5, 47),
(5, 58),
(5, 59),
(5, 60),
(5, 61),
(5, 62),
(5, 68),
(5, 69),
(5, 70),
(5, 71),
(5, 72),
(5, 118),
(5, 119),
(5, 120),
(5, 121),
(5, 122),
(5, 123),
(5, 124),
(5, 125),
(5, 126),
(5, 127),
(5, 128),
(5, 129),
(5, 130),
(5, 131),
(5, 132),
(5, 133),
(5, 134),
(5, 135),
(5, 136),
(5, 137),
(5, 138),
(5, 139),
(5, 140),
(5, 141),
(5, 142),
(5, 143),
(5, 144),
(5, 145),
(5, 146),
(5, 147),
(5, 148),
(5, 149),
(5, 150),
(5, 151),
(5, 152),
(5, 153),
(5, 154),
(5, 155),
(5, 156),
(5, 157),
(5, 158),
(5, 159),
(5, 160),
(5, 161),
(5, 162),
(5, 163),
(5, 169),
(5, 170),
(5, 171),
(5, 172),
(5, 173),
(5, 174),
(5, 175),
(5, 176),
(5, 177),
(5, 178),
(5, 184),
(5, 185),
(5, 186),
(5, 187),
(5, 188),
(5, 189),
(5, 190),
(5, 191),
(5, 192),
(5, 193),
(5, 194),
(5, 195),
(5, 196),
(5, 197),
(5, 198),
(5, 199),
(5, 200),
(5, 201),
(5, 202),
(5, 203),
(5, 204),
(5, 205),
(5, 206),
(5, 207),
(5, 208),
(5, 209),
(5, 210),
(5, 211),
(5, 212),
(5, 213),
(5, 214),
(5, 215),
(5, 216),
(5, 217),
(5, 218),
(5, 219),
(5, 220),
(5, 221),
(5, 222),
(5, 223),
(5, 224),
(5, 225),
(5, 226),
(5, 227),
(5, 228),
(5, 229),
(5, 230),
(5, 231),
(5, 232),
(5, 233),
(5, 234),
(5, 235),
(5, 236),
(5, 237),
(5, 238),
(5, 239),
(5, 240),
(5, 241),
(5, 242),
(5, 243),
(5, 244),
(5, 245),
(5, 246),
(5, 247),
(5, 248),
(5, 249),
(5, 250),
(5, 251),
(5, 252),
(5, 253),
(5, 254),
(5, 255),
(5, 256),
(5, 257),
(5, 258),
(5, 259),
(5, 260),
(5, 261),
(5, 262),
(5, 263),
(5, 264),
(5, 265),
(5, 266),
(5, 267),
(5, 268),
(5, 269),
(5, 270),
(5, 271),
(5, 275),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(5, 6),
(5, 7),
(5, 8),
(5, 9),
(5, 10),
(5, 11),
(5, 12),
(5, 13),
(5, 14),
(5, 15),
(5, 16),
(5, 43),
(5, 44),
(5, 45),
(5, 46),
(5, 47),
(5, 58),
(5, 59),
(5, 60),
(5, 61),
(5, 62),
(5, 68),
(5, 69),
(5, 70),
(5, 71),
(5, 72),
(5, 118),
(5, 119),
(5, 120),
(5, 121),
(5, 122),
(5, 123),
(5, 124),
(5, 125),
(5, 126),
(5, 127),
(5, 128),
(5, 129),
(5, 130),
(5, 131),
(5, 132),
(5, 133),
(5, 134),
(5, 135),
(5, 136),
(5, 137),
(5, 138),
(5, 139),
(5, 140),
(5, 141),
(5, 142),
(5, 143),
(5, 144),
(5, 145),
(5, 146),
(5, 147),
(5, 148),
(5, 149),
(5, 150),
(5, 151),
(5, 152),
(5, 153),
(5, 154),
(5, 155),
(5, 156),
(5, 157),
(5, 158),
(5, 159),
(5, 160),
(5, 161),
(5, 162),
(5, 163),
(5, 169),
(5, 170),
(5, 171),
(5, 172),
(5, 173),
(5, 174),
(5, 175),
(5, 176),
(5, 177),
(5, 178),
(5, 184),
(5, 185),
(5, 186),
(5, 187),
(5, 188),
(5, 189),
(5, 190),
(5, 191),
(5, 192),
(5, 193),
(5, 194),
(5, 195),
(5, 196),
(5, 197),
(5, 198),
(5, 199),
(5, 200),
(5, 201),
(5, 202),
(5, 203),
(5, 204),
(5, 205),
(5, 206),
(5, 207),
(5, 208),
(5, 209),
(5, 210),
(5, 211),
(5, 212),
(5, 213),
(5, 214),
(5, 215),
(5, 216),
(5, 217),
(5, 218),
(5, 219),
(5, 220),
(5, 221),
(5, 222),
(5, 223),
(5, 224),
(5, 225),
(5, 226),
(5, 227),
(5, 228),
(5, 229),
(5, 230),
(5, 231),
(5, 232),
(5, 233),
(5, 234),
(5, 235),
(5, 236),
(5, 237),
(5, 238),
(5, 239),
(5, 240),
(5, 241),
(5, 242),
(5, 243),
(5, 244),
(5, 245),
(5, 246),
(5, 247),
(5, 248),
(5, 249),
(5, 250),
(5, 251),
(5, 252),
(5, 253),
(5, 254),
(5, 255),
(5, 256),
(5, 257),
(5, 258),
(5, 259),
(5, 260),
(5, 261),
(5, 262),
(5, 263),
(5, 264),
(5, 265),
(5, 266),
(5, 267),
(5, 268),
(5, 269),
(5, 270),
(5, 271),
(5, 272),
(5, 273),
(5, 274),
(5, 275),
(2, 45),
(2, 47),
(2, 269),
(2, 271),
(2, 273),
(1, 276),
(1, 277),
(1, 279),
(1, 280),
(5, 276),
(5, 277),
(5, 278),
(5, 279),
(5, 280),
(7, 270),
(7, 271),
(7, 272),
(7, 273),
(7, 274),
(7, 275),
(1, 214),
(1, 215),
(1, 217),
(1, 218),
(1, 219),
(1, 220),
(1, 223),
(1, 269),
(1, 270),
(1, 281),
(1, 282),
(1, 284),
(1, 285),
(5, 12),
(5, 13),
(5, 15),
(5, 16),
(5, 43),
(5, 44),
(5, 46),
(5, 47),
(5, 68),
(5, 69),
(5, 71),
(5, 72),
(5, 119),
(5, 120),
(5, 122),
(5, 123),
(5, 124),
(5, 125),
(5, 127),
(5, 128),
(5, 129),
(5, 130),
(5, 132),
(5, 133),
(5, 134),
(5, 135),
(5, 137),
(5, 138),
(5, 139),
(5, 140),
(5, 142),
(5, 143),
(5, 144),
(5, 145),
(5, 147),
(5, 148),
(5, 149),
(5, 150),
(5, 152),
(5, 153),
(5, 154),
(5, 155),
(5, 157),
(5, 158),
(5, 159),
(5, 160),
(5, 162),
(5, 163),
(5, 169),
(5, 170),
(5, 172),
(5, 173),
(5, 174),
(5, 175),
(5, 177),
(5, 178),
(5, 184),
(5, 185),
(5, 187),
(5, 188),
(5, 189),
(5, 190),
(5, 192),
(5, 193),
(5, 194),
(5, 195),
(5, 197),
(5, 198),
(5, 199),
(5, 200),
(5, 202),
(5, 203),
(5, 204),
(5, 205),
(5, 207),
(5, 208),
(5, 214),
(5, 215),
(5, 217),
(5, 218),
(5, 219),
(5, 220),
(5, 222),
(5, 223),
(5, 224),
(5, 225),
(5, 227),
(5, 228),
(5, 229),
(5, 230),
(5, 232),
(5, 233),
(5, 234),
(5, 235),
(5, 237),
(5, 238),
(5, 239),
(5, 240),
(5, 242),
(5, 243),
(5, 244),
(5, 245),
(5, 247),
(5, 248),
(5, 249),
(5, 250),
(5, 252),
(5, 253),
(5, 254),
(5, 255),
(5, 257),
(5, 258),
(5, 259),
(5, 260),
(5, 262),
(5, 263),
(5, 264),
(5, 265),
(5, 267),
(5, 268),
(5, 269),
(5, 270),
(5, 271),
(5, 272),
(5, 274),
(5, 275),
(5, 276),
(5, 277),
(5, 279),
(5, 280),
(5, 281),
(5, 282),
(5, 284),
(5, 285),
(5, 1),
(5, 2),
(5, 3),
(5, 5),
(5, 6),
(5, 7),
(5, 8),
(5, 10),
(5, 11),
(5, 12),
(5, 13),
(5, 15),
(5, 16),
(5, 43),
(5, 44),
(5, 46),
(5, 47),
(5, 58),
(5, 59),
(5, 61),
(5, 62),
(5, 68),
(5, 69),
(5, 71),
(5, 72),
(5, 118),
(5, 119),
(5, 120),
(5, 122),
(5, 123),
(5, 124),
(5, 125),
(5, 127),
(5, 128),
(5, 129),
(5, 130),
(5, 132),
(5, 133),
(5, 134),
(5, 135),
(5, 137),
(5, 138),
(5, 139),
(5, 140),
(5, 142),
(5, 143),
(5, 144),
(5, 145),
(5, 147),
(5, 148),
(5, 149),
(5, 150),
(5, 152),
(5, 153),
(5, 154),
(5, 155),
(5, 157),
(5, 158),
(5, 159),
(5, 160),
(5, 162),
(5, 163),
(5, 169),
(5, 170),
(5, 172),
(5, 173),
(5, 174),
(5, 175),
(5, 177),
(5, 178),
(5, 184),
(5, 185),
(5, 187),
(5, 188),
(5, 189),
(5, 190),
(5, 192),
(5, 193),
(5, 194),
(5, 195),
(5, 197),
(5, 198),
(5, 199),
(5, 200),
(5, 202),
(5, 203),
(5, 204),
(5, 205),
(5, 207),
(5, 208),
(5, 209),
(5, 210),
(5, 212),
(5, 213),
(5, 214),
(5, 215),
(5, 217),
(5, 218),
(5, 219),
(5, 220),
(5, 222),
(5, 223),
(5, 224),
(5, 225),
(5, 227),
(5, 228),
(5, 229),
(5, 230),
(5, 232),
(5, 233),
(5, 234),
(5, 235),
(5, 237),
(5, 238),
(5, 239),
(5, 240),
(5, 242),
(5, 243),
(5, 244),
(5, 245),
(5, 247),
(5, 248),
(5, 249),
(5, 250),
(5, 252),
(5, 253),
(5, 254),
(5, 255),
(5, 257),
(5, 258),
(5, 259),
(5, 260),
(5, 262),
(5, 263),
(5, 264),
(5, 265),
(5, 267),
(5, 268),
(5, 269),
(5, 270),
(5, 271),
(5, 272),
(5, 274),
(5, 275),
(5, 276),
(5, 277),
(5, 279),
(5, 280),
(5, 281),
(5, 282),
(5, 284),
(5, 285),
(1, 1),
(1, 2),
(1, 3),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 15),
(1, 16),
(1, 43),
(1, 44),
(1, 46),
(1, 47),
(1, 58),
(1, 59),
(1, 61),
(1, 62),
(1, 68),
(1, 69),
(1, 71),
(1, 72),
(1, 118),
(1, 119),
(1, 120),
(1, 122),
(1, 123),
(1, 124),
(1, 125),
(1, 127),
(1, 128),
(1, 129),
(1, 130),
(1, 132),
(1, 133),
(1, 134),
(1, 135),
(1, 137),
(1, 138),
(1, 139),
(1, 140),
(1, 142),
(1, 143),
(1, 144),
(1, 145),
(1, 147),
(1, 148),
(1, 149),
(1, 150),
(1, 152),
(1, 153),
(1, 154),
(1, 155),
(1, 157),
(1, 158),
(1, 159),
(1, 160),
(1, 162),
(1, 163),
(1, 169),
(1, 170),
(1, 172),
(1, 173),
(1, 174),
(1, 175),
(1, 177),
(1, 178),
(1, 184),
(1, 185),
(1, 187),
(1, 188),
(1, 189),
(1, 190),
(1, 192),
(1, 193),
(1, 194),
(1, 195),
(1, 197),
(1, 198),
(1, 199),
(1, 200),
(1, 202),
(1, 203),
(1, 204),
(1, 205),
(1, 207),
(1, 208),
(1, 209),
(1, 210),
(1, 212),
(1, 213),
(1, 214),
(1, 215),
(1, 217),
(1, 218),
(1, 219),
(1, 220),
(1, 222),
(1, 223),
(1, 224),
(1, 225),
(1, 227),
(1, 228),
(1, 229),
(1, 230),
(1, 232),
(1, 233),
(1, 234),
(1, 235),
(1, 237),
(1, 238),
(1, 239),
(1, 240),
(1, 242),
(1, 243),
(1, 244),
(1, 245),
(1, 247),
(1, 248),
(1, 249),
(1, 250),
(1, 252),
(1, 253),
(1, 254),
(1, 255),
(1, 257),
(1, 258),
(1, 259),
(1, 260),
(1, 262),
(1, 263),
(1, 264),
(1, 265),
(1, 267),
(1, 268),
(1, 269),
(1, 270),
(1, 271),
(1, 272),
(1, 274),
(1, 275),
(1, 276),
(1, 277),
(1, 279),
(1, 280),
(1, 281),
(1, 282),
(1, 284),
(1, 285);

-- --------------------------------------------------------

--
-- Table structure for table `profit`
--

CREATE TABLE `profit` (
  `id` int(10) UNSIGNED NOT NULL,
  `profit` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profit`
--

INSERT INTO `profit` (`id`, `profit`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 1, NULL, NULL, NULL),
(2, 10, 1, NULL, NULL, NULL),
(3, 15, 1, NULL, NULL, NULL),
(4, 20, 1, NULL, NULL, NULL),
(5, 25, 1, NULL, NULL, NULL),
(6, 30, 1, NULL, NULL, NULL),
(7, 35, 1, NULL, NULL, NULL),
(8, 40, 1, NULL, NULL, NULL),
(9, 45, 1, NULL, NULL, NULL),
(10, 50, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pro_forma_invoices`
--

CREATE TABLE `pro_forma_invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `jobcodeid` int(11) NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scope_of_work` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `before_vat` decimal(15,2) NOT NULL,
  `vat_amt` decimal(15,2) NOT NULL,
  `payment_received_from_advanced` decimal(15,2) NOT NULL,
  `net_payable` decimal(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pro_forma_invoices`
--

INSERT INTO `pro_forma_invoices` (`id`, `jobcodeid`, `invoice_no`, `transaction_no`, `ref_no`, `scope_of_work`, `description`, `before_vat`, `vat_amt`, `payment_received_from_advanced`, `net_payable`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'BFA/20214', '1000315648400003', 'TEUE937', 'FIRE PROTECTION SYSTEM', NULL, '500.00', '25.00', '400.00', '125.00', '2020-09-06 01:38:22', '2020-09-06 01:38:22', NULL),
(2, 7, '2000/73', '1000315648400003', 'TEUE937', 'FIRE PROTECTION SYSTEM', NULL, '345.00', '435.00', '435.00', '343.00', '2020-10-06 21:18:41', '2020-10-06 21:18:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pro_forma_invoice_items`
--

CREATE TABLE `pro_forma_invoice_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `pro_forma_invoice_id` int(11) NOT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_vat` tinyint(4) NOT NULL DEFAULT 1,
  `add_vat_amt` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amt` decimal(15,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pro_forma_invoice_items`
--

INSERT INTO `pro_forma_invoice_items` (`id`, `pro_forma_invoice_id`, `details`, `add_vat`, `add_vat_amt`, `amt`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Net 25% OF WORK COMPLETED', 1, '25.00', '500.00', '2020-09-06 01:38:22', '2020-09-27 09:37:29', '2020-09-27 09:37:29'),
(2, 1, 'Net 25% OF WORK COMPLETED', 1, '25.00', '500.00', '2020-09-27 09:37:30', '2020-09-27 09:39:40', '2020-09-27 09:39:40'),
(3, 1, 'Net 25% OF WORK COMPLETED', 1, '25.00', '500.00', '2020-09-27 09:39:40', '2020-09-27 09:39:40', NULL),
(4, 2, 'test 01', 0, '0.00', '0.00', '2020-10-06 21:18:41', '2020-10-06 21:32:19', '2020-10-06 21:32:19'),
(5, 2, 'test 01', 0, '0.00', '0.00', '2020-10-06 21:32:19', '2020-10-06 21:32:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'User', NULL, NULL, NULL),
(3, 'Agent', '2020-08-25 22:50:10', '2020-08-25 22:50:10', NULL),
(4, 'Customer Care', '2020-08-25 22:53:49', '2020-08-25 22:53:49', NULL),
(5, 'Manager', '2020-08-25 23:00:37', '2020-08-25 23:00:37', NULL),
(6, 'Accounts', '2020-08-26 05:49:15', '2020-08-26 05:49:15', NULL),
(7, 'Estimator', '2020-10-03 05:57:10', '2020-10-03 05:57:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(3, 5),
(4, 3),
(5, 4),
(6, 6),
(10, 7),
(11, 7),
(14, 2),
(15, 2),
(16, 2),
(23, 7),
(24, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sanatization_installations`
--

CREATE TABLE `sanatization_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `status`, `content`, `title_ar`, `content_ar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Fire alarm and fire fighting equipment and systems trading', '1', '<p>&nbsp;</p><p>BRAKE Fire safety and security company submit a wide group of fire fighting systems options, that it works hardly to find the advanced ones which are suitable with local market needs and requirements according to international standards and governmental accreditation . It also provides buildings with the most safety degrees to protect the individuals and public interest within the lowest cost and the most effective systems .</p><p>&nbsp;</p><p>&nbsp;</p><ul><li>Alarm systems.</li><li>Fighting systems.</li><li>Emergency lighting systems</li><li>Fire and water PUMPS.</li><li>Fire wires and cables.</li><li>Filling and operating machines.</li><li>Industrial tools.</li><li>Spare parts and requirements for fire extinguishers.<br>&nbsp;</li></ul><p>&nbsp;</p><p>&nbsp; &nbsp;&nbsp;</p><p>&nbsp;</p>', 'تجارة أنظمة ومعدات المكافحة والإنذار من الحريق', '<p>&lt;p style=\"text-align: justify;\"&gt;تقــدم شــركة بريك فاير للأمن والسلامة مجموعــة واســعة مــن خيــارات أنظمــة مكافحــة الحريــق، &nbsp;حيث تعمل لإيجــاد وتوفير المتقدمة الـتي تناسب الإحتياجات والسوق المحلية وذلــك وفقــا للمعاييــر الدوليــة والإعتمادات الحكومية &nbsp;كما تؤمن بريك فاير أقصى درجات السلامة للمباني والحفاظ على المصالح الخاصــة والعامة مــن خــلال الأنظمة الأقل تكلفة والأكثر فعالية .&lt;/p&gt;</p><p>&lt;ul&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;أنظمة الإنذار&lt;/li&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;أنظمة المكافحة&lt;/li&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;أنظمة إنارة الطوارئ&lt;/li&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;مضخات الحريق والمياة&lt;/li&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;الأسلاك والكابلات الخاصة بالحريق&lt;/li&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;مكائن التشغيل والتعبئة&lt;/li&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;العدد الصناعية&lt;/li&gt;</p><p>&nbsp; &nbsp;&lt;li&gt;قطع غيار ولوازم طفايات الحريق&lt;/li&gt;</p><p>&lt;/ul&gt;</p>', '2020-08-22 00:21:34', '2020-08-22 01:00:33', NULL),
(2, 'System Installation', '1', NULL, 'تركيب الأنظمة', NULL, '2020-08-22 00:25:40', '2020-08-22 00:25:40', NULL),
(3, 'Fire Alarm systems', '1', NULL, 'أنظمة الإنذار من الحريق', NULL, '2020-08-22 00:26:10', '2020-08-22 00:26:10', NULL),
(4, 'Fire Fighting Systems', '1', NULL, 'أنظمة مكافحة الحريق', NULL, '2020-08-22 00:26:56', '2020-08-22 00:26:56', NULL),
(5, 'Safety and Security Systems Maintenance', '1', NULL, 'صيانة أنظمة الأمن والسلامة', NULL, '2020-08-22 00:26:56', '2020-08-22 00:29:14', NULL),
(6, 'Installing and maintenance of Smart houses systems and surveillance cameras', '1', NULL, 'تركيب وصيانة أنظمة المنازل الذكية وكاميرات المراقبة', NULL, '2020-08-22 00:30:52', '2020-08-22 00:30:52', NULL),
(7, 'Installing and maintenance of telecommunication system', '1', NULL, 'تركيب وصيانة أنظمة الإتصالات', NULL, '2020-08-22 00:31:49', '2020-08-22 00:31:49', NULL),
(8, 'Installation & maintenance of air condition systems', '1', NULL, 'مقاولات تركيب وصيانة أنظمة التبريد والتكييف', NULL, '2020-08-22 00:33:49', '2020-08-22 00:33:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `servicetype`
--

CREATE TABLE `servicetype` (
  `id` int(10) UNSIGNED NOT NULL,
  `servicename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `servicetype`
--

INSERT INTO `servicetype` (`id`, `servicename`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Fire', 1, NULL, NULL, NULL),
(2, 'CCTV', 1, NULL, NULL, NULL),
(3, 'AC', 1, NULL, NULL, NULL),
(4, 'Etisalat', 1, NULL, NULL, NULL),
(5, 'GM', 1, NULL, NULL, NULL),
(6, 'Others', 1, NULL, NULL, NULL),
(7, 'dssf', 1, '2020-09-29 20:54:36', '2020-09-29 20:54:36', NULL),
(8, 'fghfghdfg   sdfdsf', 1, '2020-09-29 20:55:38', '2020-09-29 20:56:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE `service_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `servicename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE `sms` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `status`, `sms_content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '0', '{!!configData(16)!!}&mobileno=+971559007972&msgtext=Thank you for your enquiry. It has been forwarded to the relevant department and will be dealt with as soon as possible.', '2020-09-27 08:04:03', '2020-09-27 08:04:03', NULL),
(2, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-09-27 09:30:04', '2020-09-27 09:30:04', NULL),
(3, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-09-27 09:30:24', '2020-09-27 09:30:24', NULL),
(4, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-09-30 19:23:52', '2020-09-30 19:23:52', NULL),
(5, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank-you-for-ordering-the-featured-selection.', '2020-09-30 19:58:56', '2020-09-30 19:58:56', NULL),
(6, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-09-30 20:25:05', '2020-09-30 20:25:05', NULL),
(7, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-user,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-09-30 21:44:52', '2020-09-30 21:44:52', NULL),
(8, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-10-01 15:09:00', '2020-10-01 15:09:00', NULL),
(9, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-10-01 15:16:30', '2020-10-01 15:16:30', NULL),
(10, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-10-01 16:22:04', '2020-10-01 16:22:04', NULL),
(11, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-10-01 16:30:41', '2020-10-01 16:30:41', NULL),
(12, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear-Tester,-Your-Online-Inquiry-has-been-received-our-Customer-Service-Will-assist-you-shortly.', '2020-10-01 16:33:01', '2020-10-01 16:33:01', NULL),
(13, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-48-for-further-communication-with', '2020-10-01 16:47:30', '2020-10-01 16:47:30', NULL),
(14, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-49-for-further-communication-with', '2020-10-01 16:48:35', '2020-10-01 16:48:35', NULL),
(15, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-complain-about-our-service.-Please-note-the-complain-number-BF-CMP-4-for-further-co', '2020-10-01 16:51:04', '2020-10-01 16:51:04', NULL),
(16, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-complain-about-our-service.-Please-note-the-complain-number-BF-CMP-5-for-further-co', '2020-10-01 16:54:34', '2020-10-01 16:54:34', NULL),
(17, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-50-for-further-communication-with', '2020-10-01 16:55:44', '2020-10-01 16:55:44', NULL),
(18, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-2-for-further-communication-with-', '2020-10-01 22:23:31', '2020-10-01 22:23:31', NULL),
(19, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank-you-for-ordering-the-featured-selection.', '2020-10-01 22:28:53', '2020-10-01 22:28:53', NULL),
(20, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank-you-for-getting-in-touch!-We-appreciate-you-contacting-us-Brake-Fire.-One-of-our-colleagues-wi', '2020-10-01 23:08:52', '2020-10-01 23:08:52', NULL),
(21, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Inspection-Report-Submitted-Successfully', '2020-10-02 06:06:36', '2020-10-02 06:06:36', NULL),
(22, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Inspection-Report-Submitted-Successfully', '2020-10-02 06:07:53', '2020-10-02 06:07:53', NULL),
(23, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Inspection-Report-Submitted-Successfully.--Please-note-the-report-number-{report_no}.-for-further-co', '2020-10-02 06:50:57', '2020-10-02 06:50:57', NULL),
(24, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Inspection-Report-Submitted-Successfully.--Please-note-the-report-number-{report_no}.-for-further-co', '2020-10-02 07:00:26', '2020-10-02 07:00:26', NULL),
(25, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Inspection-Report-Submitted-Successfully.--Please-note-the-report-number-BF-INS-REP-2.-for-further-c', '2020-10-02 07:03:38', '2020-10-02 07:03:38', NULL),
(26, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Inspection-Report-Submitted-Successfully.--Please-note-the-report-number-BF-INS-REP-2.-for-further-c', '2020-10-02 07:10:44', '2020-10-02 07:10:44', NULL),
(27, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Please-find-pro-forma-invoice-for-quotation-no-QA_NO4587.--Please-note-the-quotation-number-for-furt', '2020-10-02 22:29:38', '2020-10-02 22:29:38', NULL),
(28, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Please-find-pro-forma-invoice-for-quotation-no-QA_NO4587.--Please-note-the-quotation-number-for-furt', '2020-10-02 22:30:25', '2020-10-02 22:30:25', NULL),
(29, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Please-find-pro-forma-invoice-for-quotation-no-QA_NO4587.--Please-note-the-quotation-number-for-furt', '2020-10-02 22:32:27', '2020-10-02 22:32:27', NULL),
(30, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Please-find-pro-forma-invoice-for-quotation-no-QA_NO4587.--Please-note-the-quotation-number-for-furt', '2020-10-02 22:35:11', '2020-10-02 22:35:11', NULL),
(31, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Please-find-pro-forma-invoice-for-quotation-no-QA_NO4587.--Please-note-the-quotation-number-for-furt', '2020-10-02 22:42:25', '2020-10-02 22:42:25', NULL),
(32, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=BOQ-Quotation-Invoice-Generated,-Please-contact-with-us', '2020-10-03 10:15:04', '2020-10-03 10:15:04', NULL),
(33, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=BOQ-Quotation-Invoice-Generated,-Please-contact-with-us', '2020-10-03 10:16:05', '2020-10-03 10:16:05', NULL),
(34, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-{inquiryno}.', '2020-10-03 10:17:00', '2020-10-03 10:17:00', NULL),
(35, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-DBBMV-5421.', '2020-10-03 10:24:45', '2020-10-03 10:24:45', NULL),
(36, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-DBBMV-5421.', '2020-10-03 11:26:47', '2020-10-03 11:26:47', NULL),
(37, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-DBBMV-5421.', '2020-10-03 11:27:11', '2020-10-03 11:27:11', NULL),
(38, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-DBBMV-5421.', '2020-10-03 11:29:55', '2020-10-03 11:29:55', NULL),
(39, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignm-http://brakefireae.com/Services', '2020-10-03 11:37:22', '2020-10-03 11:37:22', NULL),
(40, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-XBDH-4521.-For-more-details-please-visit-https://bit.ly/30', '2020-10-03 19:21:09', '2020-10-03 19:21:09', NULL),
(41, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-XBDH-4521.-For-more-details-please-visit-https://bit.ly/30', '2020-10-03 19:22:00', '2020-10-03 19:22:00', NULL),
(42, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-2-for-further-communication-with-', '2020-10-03 19:34:05', '2020-10-03 19:34:05', NULL),
(43, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-2-for-further-communication-with-', '2020-10-03 19:34:46', '2020-10-03 19:34:46', NULL),
(44, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-8-for-further-communication-with-', '2020-10-03 19:49:00', '2020-10-03 19:49:00', NULL),
(45, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-complain-about-our-service.-Please-note-the-complain-number-BF-CMP-2-for-further-co', '2020-10-03 19:49:36', '2020-10-03 19:49:36', NULL),
(46, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-complain-about-our-service.-Please-note-the-complain-number-BF-CMP-3-for-further-co', '2020-10-03 19:49:58', '2020-10-03 19:49:58', NULL),
(47, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-03 19:50:47', '2020-10-03 19:50:47', NULL),
(48, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-03 19:51:41', '2020-10-03 19:51:41', NULL),
(49, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-03 19:57:08', '2020-10-03 19:57:08', NULL),
(50, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-03 19:57:41', '2020-10-03 19:57:41', NULL),
(51, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-9-for-further-communication-with-', '2020-10-03 20:31:01', '2020-10-03 20:31:01', NULL),
(52, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-03 23:16:51', '2020-10-03 23:16:51', NULL),
(53, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-03 23:57:51', '2020-10-03 23:57:51', NULL),
(54, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inquiry-Assignment-Notification.-Plz-note-BF-INQ-9.-For-more-details-please-visit-https://bit.ly/30x', '2020-10-04 00:21:19', '2020-10-04 00:21:19', NULL),
(55, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971710015522&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-10-for-further-communication-with', '2020-10-04 00:37:31', '2020-10-04 00:37:31', NULL),
(56, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-04 00:41:44', '2020-10-04 00:41:44', NULL),
(57, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-04 00:54:30', '2020-10-04 00:54:30', NULL),
(58, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=We-received-your-inquiry-about-our-service.-Please-note-the-BF-INQ-11-for-further-communication-with', '2020-10-04 01:23:15', '2020-10-04 01:23:15', NULL),
(59, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Thank-you-for-ordering-the-featured-selection.-For-more-details-please-visit-https://bit.ly/30xkfeC', '2020-10-04 01:38:36', '2020-10-04 01:38:36', NULL),
(60, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=We-received-your-complain-about-our-service.-Please-note-the-complain-number-BF-CMP-4-for-further-co', '2020-10-04 03:51:20', '2020-10-04 03:51:20', NULL),
(61, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:39:07', '2020-10-05 16:39:07', NULL),
(62, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:46:43', '2020-10-05 16:46:43', NULL),
(63, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:47:14', '2020-10-05 16:47:14', NULL),
(64, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:49:29', '2020-10-05 16:49:29', NULL),
(65, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:51:13', '2020-10-05 16:51:13', NULL),
(66, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:51:23', '2020-10-05 16:51:23', NULL),
(67, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:53:27', '2020-10-05 16:53:27', NULL),
(68, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 16:53:37', '2020-10-05 16:53:37', NULL),
(69, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 17:01:48', '2020-10-05 17:01:48', NULL),
(70, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 17:07:40', '2020-10-05 17:07:40', NULL),
(71, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 17:14:12', '2020-10-05 17:14:12', NULL),
(72, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 17:15:13', '2020-10-05 17:15:13', NULL),
(73, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 17:15:23', '2020-10-05 17:15:23', NULL),
(74, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+BF-BOQ-23%3Cbr%3ERef+No+%3A+SYED%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 17:23:32', '2020-10-05 17:23:32', NULL),
(75, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+BF-BOQ-24%3Cbr%3ERef+No+%3A+SYED%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-05 17:25:19', '2020-10-05 17:25:19', NULL),
(76, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+BF-BOQ-25%3Cbr%3ERef+No+%3A+SYed%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-06 21:38:29', '2020-10-06 21:38:29', NULL),
(77, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-06 21:38:47', '2020-10-06 21:38:47', NULL),
(78, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-06 21:42:03', '2020-10-06 21:42:03', NULL),
(79, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+BF-BOQ-26%3Cbr%3ERef+No+%3A+SYed%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-08 10:32:50', '2020-10-08 10:32:50', NULL),
(80, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-08 10:33:10', '2020-10-08 10:33:10', NULL),
(81, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+BF-BOQ-27%3Cbr%3ERef+No+%3A+SYED%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-08 13:22:26', '2020-10-08 13:22:26', NULL),
(82, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-08 13:22:48', '2020-10-08 13:22:48', NULL),
(83, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+BF-BOQ-28%3Cbr%3ERef+No+%3A+SYED%3Cbr%3EThank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-08 21:19:31', '2020-10-08 21:19:31', NULL),
(84, '', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Dear+%3Cbr%3ERef+No+%3A+%3Cbr%3EBOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-08 21:19:55', '2020-10-08 21:19:55', NULL),
(85, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=Inspection+Report+Submitted+Successfully.++Please+note+the+report+number+BF-INS-REP-2.+for+further+communication+with+us.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-11 10:22:10', '2020-10-11 10:22:10', NULL),
(86, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007111&msgtext=We+received+your+complain+about+our+service.+Please+note+the+complain+number+BF-CMP-5+for+further+communication+with+us.+We+will+contact+with+you+asap.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 20:56:18', '2020-10-12 20:56:18', NULL),
(87, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007111&msgtext=We+received+your+complain+about+our+service.+Please+note+the+complain+number+BF-CMP-6+for+further+communication+with+us.+We+will+contact+with+you+asap.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:21:56', '2020-10-12 21:21:56', NULL),
(88, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971555222446&msgtext=We+received+your+complain+about+our+service.+Please+note+the+complain+number+BF-CMP-7+for+further+communication+with+us.+We+will+contact+with+you+asap.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:26:30', '2020-10-12 21:26:30', NULL),
(89, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=Thank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:38:19', '2020-10-12 21:38:19', NULL),
(90, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971&msgtext=Thank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:38:19', '2020-10-12 21:38:19', NULL),
(91, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971591591591&msgtext=BOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:38:36', '2020-10-12 21:38:36', NULL),
(92, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971555222446&msgtext=Thank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:42:03', '2020-10-12 21:42:03', NULL),
(93, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971&msgtext=Thank+you+for+ordering+the+featured+selection.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:42:04', '2020-10-12 21:42:04', NULL),
(94, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971555222446&msgtext=BOQ+Quotation+Invoice+Generated%2C+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-12 21:42:17', '2020-10-12 21:42:17', NULL),
(95, 'Send Successful\r\n\r\n', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL&mobileno=+971559007972&msgtext=We+received+your+inquiry+about+our+service.+Please+note+the+%7Benq_no%7D+for+further+communication+with+us.+We+will+contact+with+you+asap.+For+more+details+please+visit+https%3A%2F%2Fbit.ly%2F30xkfeC', '2020-10-16 22:35:02', '2020-10-16 22:35:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_tbl`
--

CREATE TABLE `status_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `bf_type` tinyint(4) DEFAULT NULL,
  `bf_id` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `msg` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary_ar` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `telecom_installations`
--

CREATE TABLE `telecom_installations` (
  `id` int(10) UNSIGNED NOT NULL,
  `plot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `city`, `message`, `name_ar`, `city_ar`, `message_ar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Syed Moin', 'Ajman, Dubai', 'I have been using Security Systems of company for the security of our home for over 4 years now and have been very happy with their service. they have called us within seconds.', 'سيد معين', 'عجمان ، دبي', 'لقد كنت أستخدم أنظمة الأمن الخاصة بالشركة لأمن منزلنا لأكثر من 4 سنوات حتى الآن وكنت سعيدًا جدًا بخدمتهم. لقد اتصلوا بنا في غضون ثوان.', NULL, NULL, NULL),
(2, 'Syed Moin', 'Ajman, Dubai', 'I have been using Security Systems of company for the security of our home for over 4 years now and have been very happy with their service. they have called us within seconds.', 'سيد معين', 'عجمان ، دبي', 'لقد كنت أستخدم أنظمة الأمن الخاصة بالشركة لأمن منزلنا لأكثر من 4 سنوات حتى الآن وكنت سعيدًا جدًا بخدمتهم. لقد اتصلوا بنا في غضون ثوان.', NULL, NULL, NULL),
(3, 'Syed Moin', 'Ajman, Dubai', 'I have been using Security Systems of company for the security of our home for over 4 years now and have been very happy with their service. they have called us within seconds.', 'سيد معين', 'عجمان ، دبي', 'لقد كنت أستخدم أنظمة الأمن الخاصة بالشركة لأمن منزلنا لأكثر من 4 سنوات حتى الآن وكنت سعيدًا جدًا بخدمتهم. لقد اتصلوا بنا في غضون ثوان.', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `uae_location_lists`
--

CREATE TABLE `uae_location_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `emiratesid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_by` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uae_location_lists`
--

INSERT INTO `uae_location_lists` (`id`, `emiratesid`, `name`, `order_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'Abu Dhabi', 3, NULL, NULL, NULL),
(2, 0, 'Ajman', 1, NULL, NULL, NULL),
(3, 0, 'Dubai', 2, NULL, NULL, NULL),
(4, 0, 'Fujairah', 5, NULL, NULL, NULL),
(5, 0, 'Ras Al Khaimah', 6, NULL, NULL, NULL),
(6, 0, 'Sharjah', 4, NULL, NULL, NULL),
(7, 0, 'Umm Al Quwain', 7, NULL, NULL, NULL),
(8, 1, 'Abu Dhabi Co-operative', 1, NULL, NULL, NULL),
(9, 1, 'Abu Dhabi Co-operative', 1, '2020-09-05 20:11:34', '2020-09-05 20:11:34', NULL),
(10, 1, 'Abu Dhabi Co-operative Society', 1, '2020-09-05 20:11:34', '2020-09-05 20:11:34', NULL),
(11, 1, 'Abu Dhabi International Airport', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(12, 1, 'Abu Dhabi Mall', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(13, 1, 'Airport Road', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(14, 1, 'Al Bateen', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(15, 1, 'Al Bateen', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(16, 1, 'Al Dhafrah', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(17, 1, 'Al Falah Street', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(18, 1, 'Al Ittihad Street', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(19, 1, 'Al Jazeira Hospital', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(20, 1, 'Al Karama Street', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(21, 1, 'Al Khaleej Al Arabi Street', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(22, 1, 'Al Khalidiya', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(23, 1, 'Al Khubeirah', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(24, 1, 'Al Madina Al Riyadiya', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(25, 1, 'Al Manhal', 1, '2020-09-05 20:11:35', '2020-09-05 20:11:35', NULL),
(26, 1, 'Al Manhal Street', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(27, 1, 'Al Maqta\'a', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(28, 1, 'Al Mariah Cinema', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(29, 1, 'Al Markaziyah', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(30, 1, 'Al Matar', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(31, 1, 'Al Meena', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(32, 1, 'Al Mushrif', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(33, 1, 'Al Nahyan Street', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(34, 1, 'Al Nasr Street', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(35, 1, 'Al Noor Hospital', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(36, 1, 'Al Qurayyah', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(37, 1, 'Al Raha', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(38, 1, 'Al Raha Beach', 1, '2020-09-05 20:11:36', '2020-09-05 20:11:36', NULL),
(39, 1, 'Al Reef', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(40, 1, 'Al Reem Island', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(41, 1, 'Al Rowdah', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(42, 1, 'Al Saada Street', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(43, 1, 'Al Safarat', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(44, 1, 'Al Samha', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(45, 1, 'Al Shalilah', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(46, 1, 'Al Wahdah Area', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(47, 1, 'Al Wahdah Mall', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(48, 1, 'Bainunah', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(49, 1, 'Bainunah Street', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(50, 1, 'Baniyas', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(51, 1, 'Baniyas Street', 1, '2020-09-05 20:11:37', '2020-09-05 20:11:37', NULL),
(52, 1, 'Bateen Street', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(53, 1, 'Breakwaters', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(54, 1, 'Central Hospital', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(55, 1, 'Central Market', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(56, 1, 'Central Post Office', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(57, 1, 'Coast Road', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(58, 1, 'Corniche', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(59, 1, 'Corniche Hospital', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(60, 1, 'Corniche Road', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(61, 1, 'Dalma Street', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(62, 1, 'Defence Road', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(63, 1, 'East Road', 1, '2020-09-05 20:11:38', '2020-09-05 20:11:38', NULL),
(64, 1, 'Eastern Ring Road', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(65, 1, 'Eid Musallah', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(66, 1, 'Electra', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(67, 1, 'Electra Street', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(68, 1, 'Fish Market Area', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(69, 1, 'Hamdan Street', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(70, 1, 'Haza\'a Bin Zayed Street', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(71, 1, 'Hydra Village', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(72, 1, 'Karama', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(73, 1, 'Khaleej Al Arabi Street', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(74, 1, 'Khalidiya Street', 1, '2020-09-05 20:11:39', '2020-09-05 20:11:39', NULL),
(75, 1, 'Khalidiyah', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(76, 1, 'Khalifa Bin Zayed Street', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(77, 1, 'Khalifa Street', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(78, 1, 'King Khalid Bin Abdel Aziz Street', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(79, 1, 'Liwa Street', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(80, 1, 'Madinat Khalifa', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(81, 1, 'Madinat Zayed', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(82, 1, 'Mafraq', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(83, 1, 'Manasir', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(84, 1, 'Marina Mall', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(85, 1, 'Meena Road', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(86, 1, 'Meena Souq', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(87, 1, 'Mohammed Bin Khalifa Street', 1, '2020-09-05 20:11:40', '2020-09-05 20:11:40', NULL),
(88, 1, 'Mohammed Bin Zayed City', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(89, 1, 'Muroor', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(90, 1, 'Muroor Road', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(91, 1, 'Mussafah', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(92, 1, 'Mussafah Industrial Area', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(93, 1, 'Mussafah Sanaiya', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(94, 1, 'Mussafah Shabia', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(95, 1, 'Najda', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(96, 1, 'Najda Street', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(97, 1, 'National Cinema', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(98, 1, 'New Medical Centre', 1, '2020-09-05 20:11:41', '2020-09-05 20:11:41', NULL),
(99, 1, 'New Shahama', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(100, 1, 'Officers City Area', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(101, 1, 'Officers Club Area', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(102, 1, 'Old Airport Road', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(103, 1, 'Old Fish Market Area', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(104, 1, 'Old Passport Road', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(105, 1, 'Port Zayed Area', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(106, 1, 'Qasr Al Bahr', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(107, 1, 'Qasr Al Shatie', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(108, 1, 'Rahaba', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(109, 1, 'Rowdah', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(110, 1, 'Ruwais', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(111, 1, 'Sadiyat Island', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(112, 1, 'Saeed Bin Tahnoon The 1st Street', 1, '2020-09-05 20:11:42', '2020-09-05 20:11:42', NULL),
(113, 1, 'Salam Street', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(114, 1, 'Shahama', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(115, 1, 'Shamkha', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(116, 1, 'Sheikh Hamdan Bin Mohammad Street', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(117, 1, 'Sheikh Rashid Bin Saeed Al Maktoum Street', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(118, 1, 'Sultan Bin Zayed Street', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(119, 1, 'Tarif Industrial Area', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(120, 1, 'Tourist Club Area', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(121, 1, 'Umm Al Nar', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(122, 1, 'Unknown', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(123, 1, 'Zayed The 1st Street', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(124, 1, 'Zayed The 2nd Street', 1, '2020-09-05 20:11:43', '2020-09-05 20:11:43', NULL),
(125, 2, 'Ajman Beach Hotel', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(126, 2, 'Ajman Centre', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(127, 2, 'Ajman City Centre', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(128, 2, 'Ajman Free Zone', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(129, 2, 'Ajman Kempinski', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(130, 2, 'Ajman University', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(131, 2, 'Al Bustan', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(132, 2, 'Al Gurf', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(133, 2, 'Al Nakheel', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(134, 2, 'Al Rumailah', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(135, 2, 'Al Sawan', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(136, 2, 'Al Zora', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(137, 2, 'Coral Beach Hotel', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(138, 2, 'Cultural Centre', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(139, 2, 'Etisalat', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(140, 2, 'Fruit &amp; Vegetable Market', 1, '2020-09-05 20:16:37', '2020-09-05 20:16:37', NULL),
(141, 2, 'Al Ain Oasis', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(142, 2, 'Al Ain Zoo', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(143, 2, 'Al Basraa', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(144, 2, 'Al Bateen', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(145, 2, 'Al Faqah', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(146, 2, 'Al Hair', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(147, 2, 'Al Jahili', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(148, 2, 'Al Jamia Street', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(149, 2, 'Al Jimi', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(150, 2, 'Al Jimi Street', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(151, 2, 'Al Kabisi', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(152, 2, 'Al Khaleej Al Arabi Street', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(153, 2, 'Al Maqam', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(154, 2, 'Al Masoudi', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(155, 2, 'Al Mutarad', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(156, 2, 'Al Qattara', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(157, 2, 'Al Towwaiya', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(158, 2, 'Al Zakher', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(159, 2, 'Buraimi', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(160, 2, 'Hamdan Bin Mohammed Street', 1, '2020-09-05 20:16:38', '2020-09-05 20:16:38', NULL),
(161, 2, 'Jebel Hafeet', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(162, 2, 'Khalifa Street', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(163, 2, 'Murrabha', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(164, 2, 'Muwaiji', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(165, 2, 'Nahyan Awwal Street', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(166, 2, 'Sanaiya', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(167, 2, 'Shakboot Ibn Sultan Street', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(168, 2, 'Tahnoon Ibn Zayed Street', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(169, 2, 'Tawam', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(170, 2, 'Town Square', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(171, 2, 'Truck Road', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(172, 2, 'Unknown', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(173, 2, 'Zakher', 1, '2020-09-05 20:16:39', '2020-09-05 20:16:39', NULL),
(174, 3, 'Academic City', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(175, 3, 'Airport Free Zone', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(176, 3, 'Al Aweer', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(177, 3, 'Al Bada\'a', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(178, 3, 'Al Barari', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(179, 3, 'Al Barsha', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(180, 3, 'Al Furjan', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(181, 3, 'Al Garhoud', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(182, 3, 'Al Hamriya', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(183, 3, 'Al Jaddaf', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(184, 3, 'Al Jafiliya', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(185, 3, 'Al Karama', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(186, 3, 'Al Khabisi', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(187, 3, 'Al Khawaneej', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(188, 3, 'Al Kifaf', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(189, 3, 'Al Mamzar', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(190, 3, 'Al Marqadh', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(191, 3, 'Al Mizhar', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(192, 3, 'Al Muhaisnah', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(193, 3, 'Al Nahda', 1, '2020-09-05 20:28:56', '2020-09-05 20:28:56', NULL),
(194, 3, 'Al Nasr', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(195, 3, 'Al Qasba\'a', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(196, 3, 'Al Quoz', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(197, 3, 'Al Qusais', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(198, 3, 'Al Rashidiya', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(199, 3, 'Al Safa', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(200, 3, 'Al Satwa', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(201, 3, 'Al Shindagah', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(202, 3, 'Al Sufouh', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(203, 3, 'Al Twar', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(204, 3, 'Al Warqa\'a', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(205, 3, 'Al Wasl', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(206, 3, 'Arabian Ranches', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(207, 3, 'Bu Kadra', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(208, 3, 'Bur Dubai', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(209, 3, 'Burj Khalifa', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(210, 3, 'Business Bay', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(211, 3, 'Culture Village', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(212, 3, 'Deira', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(213, 3, 'DIFC', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(214, 3, 'Discovery Gardens', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(215, 3, 'Downtown Burj Dubai', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(216, 3, 'Downtown Jebel Ali', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(217, 3, 'Dubai Autodrome &amp; Business Park', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(218, 3, 'Dubai Festival City', 1, '2020-09-05 20:28:57', '2020-09-05 20:28:57', NULL),
(219, 3, 'Dubai Healthcare City', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(220, 3, 'Dubai Industrial City', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(221, 3, 'Dubai Internet City', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(222, 3, 'Dubai Investment Park', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(223, 3, 'Dubai Lagoon', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(224, 3, 'Dubai Land', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(225, 3, 'Dubai Marina', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(226, 3, 'Dubai Media City', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(227, 3, 'Dubai Pearl', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(228, 3, 'Dubai Promenade', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(229, 3, 'Dubai Silicon Oasis', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(230, 3, 'Dubai Sports City', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(231, 3, 'Dubai Technology Park', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(232, 3, 'Dubai Waterfront', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(233, 3, 'Dubai World Central', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(234, 3, 'DuBiotech', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(235, 3, 'Emirates Hills', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(236, 3, 'Emirates Road', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(237, 3, 'Forbidden City', 1, '2020-09-05 20:28:58', '2020-09-05 20:28:58', NULL),
(238, 3, 'Global Village', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(239, 3, 'Golf Gardens', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(240, 3, 'Green Community', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(241, 3, 'Greens', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(242, 3, 'IMPZ', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(243, 3, 'International City', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(244, 3, 'Jebel Ali', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(245, 3, 'Jebel Ali Free Zone Authority', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(246, 3, 'Jumeirah Area', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(247, 3, 'Jumeirah Beach Residence', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(248, 3, 'Jumeirah Golf Estates', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(249, 3, 'Jumeirah Heights', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(250, 3, 'Jumeirah Islands', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(251, 3, 'Jumeirah Lake Towers', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(252, 3, 'Jumeirah Park', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(253, 3, 'Jumeirah Village Circle', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(254, 3, 'Jumeirah Village Triangle', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(255, 3, 'Knowledge Village', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(256, 3, 'Maritime City', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(257, 3, 'Meadows', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(258, 3, 'Meydan City', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(259, 3, 'Mina Al Arab', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(260, 3, 'Mirdif', 1, '2020-09-05 20:28:59', '2020-09-05 20:28:59', NULL),
(261, 3, 'Motor City', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(262, 3, 'Mushrif Park', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(263, 3, 'Nad Al Hammar', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(264, 3, 'Nad Al Sheba', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(265, 3, 'Old Town', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(266, 3, 'Palm Jebel Ali', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(267, 3, 'Palm Jumeirah', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(268, 3, 'Ras Al Khor', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(269, 3, 'Shaikh Zayed Road', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(270, 3, 'Sonapur', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(271, 3, 'Springs', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(272, 3, 'Tecom', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(273, 3, 'The Gardens', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(274, 3, 'The Lagoons', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(275, 3, 'The Lakes', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(276, 3, 'The Old Town Island', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(277, 3, 'The Views', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(278, 3, 'The World Islands', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(279, 3, 'Umm Al Sheif', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(280, 3, 'Umm Ramool', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(281, 3, 'Umm Suqueim', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(282, 3, 'Unknown', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(283, 3, 'Zabeel', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(284, 4, 'Airport Road', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(285, 4, 'Al Faseel Road', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(286, 4, 'Al Hail', 1, '2020-09-05 20:29:00', '2020-09-05 20:29:00', NULL),
(287, 4, 'Al Nakheel Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(288, 4, 'Al Sharqi', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(289, 4, 'Al Sharqi Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(290, 4, 'Bank Street', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(291, 4, 'Bidiya', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(292, 4, 'Central Market', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(293, 4, 'Corniche Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(294, 4, 'Dhaid', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(295, 4, 'Dibba', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(296, 4, 'Faseel', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(297, 4, 'Friday Market', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(298, 4, 'Fujairah Free Zone', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(299, 4, 'Fujairah Hospital Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(300, 4, 'Gurfa Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(301, 4, 'Hamad Bin Abdullah Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(302, 4, 'Kalba', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(303, 4, 'Kalba Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(304, 4, 'Khor Kalba', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(305, 4, 'Khorfakkan', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(306, 4, 'Khorfakkan Road', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(307, 4, 'Madhab', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(308, 4, 'Massafi', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(309, 4, 'Murbah', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(310, 4, 'Safad', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(311, 4, 'Sakamkam', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(312, 4, 'Siji', 1, '2020-09-05 20:29:01', '2020-09-05 20:29:01', NULL),
(313, 5, 'Airport Road', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(314, 5, 'Al Araibi', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(315, 5, 'Al Hamra Village', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(316, 5, 'Al Juwais', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(317, 5, 'Al Khor Road', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(318, 5, 'Al Nakheel', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(319, 5, 'Al Nakheel Hotel', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(320, 5, 'Al Sall', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(321, 5, 'Bin Dahir Road', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(322, 5, 'Bin Majid Road', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(323, 5, 'Commercial Centre', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(324, 5, 'Dafan', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(325, 5, 'Dahan', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(326, 5, 'Eid Musallah', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(327, 5, 'Eid Musallah Road', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(328, 5, 'Electricity Road', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(329, 5, 'Etisalat', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(330, 5, 'Gulf Cinema', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(331, 5, 'Jazeera', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(332, 5, 'Kharran', 1, '2020-09-05 20:29:02', '2020-09-05 20:29:02', NULL),
(333, 5, 'Khat', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(334, 5, 'Khuzam', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(335, 5, 'Khuzam Road', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(336, 5, 'Mammoorah', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(337, 5, 'Mammoorah Road', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(338, 5, 'Police Station', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(339, 5, 'Rams', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(340, 5, 'Ras Al Khaimah Cinema', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(341, 5, 'Ras Al Khaimah Hotel', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(342, 5, 'Ruler\'s Palace', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(343, 5, 'Saif Gobash Hospital', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(344, 5, 'Saqr Hospital', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(345, 5, 'Sham', 1, '2020-09-05 20:29:03', '2020-09-05 20:29:03', NULL),
(346, 5, 'Sultan Al Kabeer Road', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(347, 6, 'Abu Shaghara', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(348, 6, 'Abu Shaghara Park', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(349, 6, 'Al Abar', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(350, 6, 'Al Arouba Road', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(351, 6, 'Al Azra', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(352, 6, 'Al Bu Daniq', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(353, 6, 'Al Buheira Corniche Road', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(354, 6, 'Al Darari', 1, '2020-09-05 20:29:04', '2020-09-05 20:29:04', NULL),
(355, 6, 'Al Dhaid', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(356, 6, 'Al Dhaid Road', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(357, 6, 'Al Falah Plaza', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(358, 6, 'Al Falaj', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(359, 6, 'Al Fardan Centre', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(360, 6, 'Al Fayha', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(361, 6, 'Al Fisht', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(362, 6, 'Al Gharb District', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(363, 6, 'Al Ghubaiba', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(364, 6, 'Al Ghuwair', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(365, 6, 'Al Goaz', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(366, 6, 'Al Hazana', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(367, 6, 'Al Heera Road', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(368, 6, 'Al Ittihad Road', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(369, 6, 'Al Ittihad Square', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(370, 6, 'Al Jazeira', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(371, 6, 'Al Jazzat', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(372, 6, 'Al Jubail', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(373, 6, 'Al Khan', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(374, 6, 'Al Khan Road', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(375, 6, 'Al Khezammia', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(376, 6, 'Al Majara', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(377, 6, 'Al Majaz', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(378, 6, 'Al Majaz Park', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(379, 6, 'Al Manakh', 1, '2020-09-05 20:29:05', '2020-09-05 20:29:05', NULL),
(380, 6, 'Al Mansura', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(381, 6, 'Al Merraija', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(382, 6, 'Al Mina Road', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(383, 6, 'Al Mirgab Road', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(384, 6, 'Al Mujarrah', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(385, 6, 'Al Muntaza', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(386, 6, 'Al Muntazah Road', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(387, 6, 'Al Nabba\'a', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(388, 6, 'Al Nahda', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(389, 6, 'Al Nahda Park', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(390, 6, 'Al Nahda Road', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(391, 6, 'Al Nekhailat', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(392, 6, 'Al Nud', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(393, 6, 'Al Qasimia Hospital', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(394, 6, 'Al Qassimia Road', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(395, 6, 'Al Qassimiya', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(396, 6, 'Al Qassimiya', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(397, 6, 'Al Ramla', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(398, 6, 'Al Ramtha', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(399, 6, 'Al Rifa\'ah', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(400, 6, 'Al Riqa\'a', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(401, 6, 'Al Rolla', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(402, 6, 'Al Rolla Square', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(403, 6, 'Al Safia Park', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(404, 6, 'Al Shahba', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(405, 6, 'Al Sharq', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(406, 6, 'Al Sharq Road', 1, '2020-09-05 20:29:06', '2020-09-05 20:29:06', NULL),
(407, 6, 'Al Shuwaiheen', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(408, 6, 'Al Soor', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(409, 6, 'Al Soor Road', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(410, 6, 'Al Subaikha', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(411, 6, 'Al Sweihat', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(412, 6, 'Al Taawun', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(413, 6, 'Al Taawun Mall', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(414, 6, 'Al Taawun Road', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(415, 6, 'Al Tala\'a', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(416, 6, 'Al Wahda Road', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(417, 6, 'Al Zahra Road', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(418, 6, 'Al Zahra Square', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(419, 6, 'Ansar Mall', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(420, 6, 'Bank Street', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(421, 6, 'Buheira Corniche', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(422, 6, 'Buteena', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(423, 6, 'Butina', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(424, 6, 'Caterpillar Roundabout', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(425, 6, 'Clock Tower', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(426, 6, 'Concorde Cinema', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(427, 6, 'Dasman', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(428, 6, 'Fire Station Roundabout', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(429, 6, 'Geco Roundabout', 1, '2020-09-05 20:29:07', '2020-09-05 20:29:07', NULL),
(430, 6, 'Halwan', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(431, 6, 'Hamriya Free Zone', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(432, 6, 'Industrial Area', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(433, 6, 'Jamal Abdul Nasser Street', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(434, 6, 'Khalidiya', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(435, 6, 'King Abdul Aziz Street', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(436, 6, 'King Faisal Road', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(437, 6, 'Kuwait Road', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(438, 6, 'Layea', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(439, 6, 'Maisaloon', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(440, 6, 'Maliha Road', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(441, 6, 'Mega Mall', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(442, 6, 'Mina Road', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(443, 6, 'Mirgab', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(444, 6, 'Mubarak Centre', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(445, 6, 'Musallah', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(446, 6, 'Nasseriya', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(447, 6, 'National Paints Roundabout', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(448, 6, 'Qadsia', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(449, 6, 'Qanat Al Qasba', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(450, 6, 'Quran Roundabout', 1, '2020-09-05 20:29:08', '2020-09-05 20:29:08', NULL),
(451, 6, 'Rolla Mall', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(452, 6, 'Safeer Mall', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(453, 6, 'Sahara Centre', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(454, 6, 'Samnan', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(455, 6, 'Sharjah Airport Free Zone', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(456, 6, 'Sharjah Cricket Stadium', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(457, 6, 'Sharqan', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(458, 6, 'Sheikh Humaid Bin Saqr Al Qasimi Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(459, 6, 'Sheikh Khalid Bin Khalid Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(460, 6, 'Sheikh Khalid Bin Mohammed Al Qasimi Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(461, 6, 'Sheikh Mohammed Bin Saqr Al Qasimi Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(462, 6, 'Sheikh Rashid Bin Saqr Al Qasimi Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(463, 6, 'Sheikh Saqr Bin Khalid Al Qasimi Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(464, 6, 'Sheikh Saud Bin Sultan Al Qasimi Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(465, 6, 'Sheikh Sultan Al Awal Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(466, 6, 'Sheikh Sultan Bin Saqr Al Qasimi Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(467, 6, 'Sheikh Zayed Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(468, 6, 'Um Al Tarrafa', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(469, 6, 'University Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(470, 6, 'Waset Road', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(471, 6, 'Yarmouk', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(472, 7, 'Al Arabi Club', 1, '2020-09-05 20:29:09', '2020-09-05 20:29:09', NULL),
(473, 7, 'Al Arabi Club', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL),
(474, 7, 'Chamber Of Commerce', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL),
(475, 7, 'Fire Station', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL),
(476, 7, 'Municipality', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL),
(477, 7, 'Pearl Hotel', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL),
(478, 7, 'Police Station', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL),
(479, 7, 'Umm Al Quwain Beach Hotel', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL),
(480, 7, 'Umm Al Quwain New Hospital', 1, '2020-09-05 20:29:10', '2020-09-05 20:29:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1:Customer, 2: Staff ( Admin )',
  `mobileno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `name`, `name_ar`, `email`, `user_type`, `mobileno`, `company_name`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'Admin Kasif Moinn', 'المشرف كاسيف معين', 'admin', 2, '971559007972', NULL, NULL, '$2y$10$y4WK0TOMte8JLyT/gnTR7eWjlqaj17SZH.nLF1BNaFwt9LlNf0FnG', NULL, NULL, '2020-08-16 01:25:55', NULL),
(2, NULL, 'User Syed Moinn', 'المستخدم سيد موين', 'test@admin.com', 1, '971559007972', NULL, NULL, '$2y$10$ZbrEw7oauPW0RsWPBGnMYu2Cyk1hSLnYC0hxGltktE5USypn1ofD.', NULL, '2020-08-17 06:01:04', '2020-08-17 06:01:04', NULL),
(3, NULL, 'Mang. Syed', 'مانغ. سيد', 'manager@admin.com', 2, '971559007972', NULL, NULL, '$2y$10$LL..WXRy3rLH4X.u9kzOgeowwvtuglZ3ThMII8z1hKunCYG6ygfCm', NULL, '2020-08-25 07:33:15', '2020-08-25 23:11:20', NULL),
(4, NULL, 'Agent Moinn', 'الوكيل معين', 'agent@admin.com', 2, '971559007972', NULL, NULL, '$2y$10$OPsjBCFyw6SjDD2KOUZXJuYtOk90d/KQOMQ5o7oaYCv1q9azcsXhG', NULL, '2020-08-25 23:19:23', '2020-08-25 23:19:23', NULL),
(5, NULL, 'CC. Kasif', 'رعاية العميل', 'customercare@admin.com', 2, '971559007972', NULL, NULL, '$2y$10$pjZIfXXOQvlJ7PzZ02Kn.OFccO4J8haZRnlUcaXXfxe5Ec52jSjw2', NULL, '2020-08-25 23:21:07', '2020-08-25 23:21:07', NULL),
(6, NULL, 'Acc Syed Kasif', 'اكس سيد كاسيف', 'accounts@admin.com', 2, '971559007972', NULL, NULL, '$2y$10$25GGhZU6nNTfQZAenioC5u4Lbi5Bp6Yzg6C2CxPcOShQi9J2qIoPu', NULL, '2020-08-26 06:11:03', '2020-08-26 06:11:03', NULL),
(7, NULL, 'Syed Kasif', 'سيد كاسيف', 'syedk@gmail.com', 1, '971559007972', NULL, NULL, '$2y$10$hcmU.NCQlFHgxenDjRBJVudt3KxcAIdsZas/8fO7GYGXi9GDMfjIC', NULL, '2020-09-05 10:28:38', '2020-09-05 10:28:38', NULL),
(9, NULL, 'Syed Kasif', 'سيد كاسيف', 'syedka@gmail.com', 1, '971559007972', NULL, NULL, '$2y$10$yhmxz3FYNLG2PMtq.cTejuCh6ymtiyEBiUP8Cs6ScMBIe677sGsl6', NULL, '2020-09-05 10:29:13', '2020-09-05 10:29:13', NULL),
(10, NULL, 'Estimator SK', NULL, 'sk@gmail.com', 2, '971559007972', NULL, NULL, '$2y$10$4sHJf1V/5yyOfl.m1sT9DeE7xm5i5b103oV4c1CcEWTGyi5XuwiFO', NULL, '2020-10-03 06:05:01', '2020-10-03 06:05:01', NULL),
(11, NULL, 'Estimator SK1', NULL, 'sk1@admin.com', 2, NULL, NULL, NULL, '$2y$10$yp/iXadkcrR9hXzsueDuyuGh2K.CS4j1p/CUSF48A0ur19O.f.enq', NULL, '2020-10-03 20:55:35', '2020-10-03 20:55:35', NULL),
(12, NULL, 'Tester', NULL, 'tester@admin.com', 2, NULL, NULL, NULL, '$2y$10$gkqxYpqeYuK3FW0x2M720ODCWqMlblmvYeQlrPxb0w6MDDWYtq5IC', NULL, '2020-10-04 13:09:31', '2020-10-04 13:09:31', NULL),
(13, NULL, 'Tester', NULL, 'teste111r@admin.com', 2, NULL, NULL, NULL, '$2y$10$G2spYjkooM67rPGJq6yZwuoIq/jmlOSyTDFQ8iLbh4z9KOliY9hYS', NULL, '2020-10-04 13:23:05', '2020-10-04 13:23:05', NULL),
(14, NULL, 'Tester', NULL, 'tester6666@admin.com', 2, NULL, NULL, NULL, '$2y$10$/tGBe1qGQxVD9qh7owRGMOy8QADnIDjxmiJ/RygSyOvKlLDK7DWB6', NULL, '2020-10-04 18:28:26', '2020-10-04 18:28:26', NULL),
(15, NULL, 'Tester', NULL, 'testeeeer@admin.com', 1, NULL, NULL, NULL, '$2y$10$3PYIHZVbNOKeqeHMgXOOpeJcuL10mXIU/C1SCZQPwHQZULATS9Zaq', NULL, '2020-10-04 18:50:32', '2020-10-04 18:50:41', NULL),
(16, '7036', 'dsgdfgfd', NULL, 'sdfsffsd@sfd.gfd', 2, '111111111', NULL, NULL, '$2y$10$/LM88BnSjPxQR1SGupAptu0yaEaGRb1qbNNG8BVxw5jgxDt.QQwwS', NULL, '2020-10-08 23:30:50', '2020-10-09 03:36:32', NULL),
(17, 'CUST-4769', 'dfgdfgdg', NULL, 'dfgfdghjiu@sdfds.gfd', 1, NULL, NULL, NULL, '$2y$10$I.RqP4O0R7LuONFEF8w6ZeXr8l1nZ.FzwIEynoRS1w3wTt9rbviD6', NULL, '2020-10-09 01:04:40', '2020-10-09 01:04:40', NULL),
(18, 'CUST-1001', 'ddgdgfdg', NULL, 'sdfjhgqwe@swr.dfs', 1, NULL, NULL, NULL, '$2y$10$ozGaS8UqokUE5/69ycnIgeYSTHJ9VfK7DE0dqwUjOMYSTKUHHPray', NULL, '2020-10-09 01:06:19', '2020-10-09 01:06:19', NULL),
(19, 'CUST-6949', 'UKHUB', NULL, 'testehhhggbbr@admin.com', 1, NULL, '', NULL, '$2y$10$Cx6yXDW50EmQe3/0wkVaa.zvnDk4TmOrS4v3Vg/oTdfxY249wocqu', NULL, '2020-10-09 01:15:32', '2020-10-09 01:15:32', NULL),
(20, 'CUST-9608', 'UKHUB222p', NULL, 'testedddccr@admin.com', 1, NULL, '', NULL, '$2y$10$shnhBzgB2AVWb0oRPMUsuuKiPJS8r24IiGA4b8fWBE77Vc45eBTBS', NULL, '2020-10-09 01:17:13', '2020-10-09 01:17:13', NULL),
(21, 'CUST-7045', 'Tester', NULL, 'tesvvvvter@admin.com', 1, NULL, 'UKHUB', NULL, '$2y$10$WBl23pft0K2djnF3qC.B.OnzYpf.kfTTWkdyNW5AWZRgZsaU1X3B6', NULL, '2020-10-09 01:19:59', '2020-10-09 01:19:59', NULL),
(22, 'CUST-4591', 'Tester', NULL, 'tester333@admin.com', 1, '971555222446', 'UKHUB', NULL, '$2y$10$A8o5n1rWZijJxlH2lTtBo.HXCNCgbU5CZJ5XXP/C691avnt/hHnIG', NULL, '2020-10-11 10:52:06', '2020-10-11 10:52:06', NULL),
(23, 'CUST-6207', 'Tester', NULL, 'tester444@admin.com', 1, '1591591591', 'UKHUB', NULL, '$2y$10$0LfuhfyWtUG0Gl41VhvaIuAMz9CG6OYp6DJiJMYMzY.bFeAV/gQ1.', NULL, '2020-10-11 11:36:00', '2020-10-11 11:36:00', NULL),
(24, 'CUST-7338', 'TESTER', NULL, 'test@gmail.com', 1, '971555222446', 'TEST', NULL, '$2y$10$izmhOWNAVDqpY72mLb0vfeCTIGj8K69VQ7/Ps7O8FNUPqkoWrtN3.', NULL, '2020-10-12 19:53:05', '2020-10-12 19:53:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_complains`
--

CREATE TABLE `user_complains` (
  `id` int(10) UNSIGNED NOT NULL,
  `estimator_id` int(11) NOT NULL DEFAULT 0,
  `company_owner_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) NOT NULL DEFAULT 0,
  `service_type_details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emirates` int(11) NOT NULL DEFAULT 0,
  `emiratescity` int(11) NOT NULL DEFAULT 0,
  `project_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complain_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complain_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_complains`
--

INSERT INTO `user_complains` (`id`, `estimator_id`, `company_owner_name`, `service_type`, `service_type_details`, `emirates`, `emiratescity`, `project_details`, `complain_number`, `fname`, `emailid`, `contact_no`, `address`, `location`, `details`, `complain_details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 11, NULL, 0, NULL, 0, 0, NULL, 'BF-CMP-4', 'SYed', 'syed@admin.com', '971559007972', '23 RD Road, Ajman', 'UAE MArket', NULL, NULL, '2020-10-04 03:51:20', '2020-10-04 21:50:24', NULL),
(9, 0, NULL, 0, NULL, 0, 0, NULL, 'BF-CMP-5', 'Tester', 'tester@admin.com', '971559007111', '23 RD Road,', 'DUBAO', 'dfgf dfgfdg', NULL, '2020-10-12 20:56:16', '2020-10-12 20:56:16', NULL),
(10, 0, NULL, 0, NULL, 0, 0, NULL, 'BF-CMP-6', 'Tester', 'test@gmail.com', '971559007111', '23 RD Road,', 'DUBAI', 'fghfgh  fhfgh', NULL, '2020-10-12 21:21:55', '2020-10-12 21:21:55', NULL),
(11, 10, NULL, 0, NULL, 0, 0, NULL, 'BF-CMP-7', 'TESTER', 'test@gmail.com', '971555222446', 'dfgfdg dfdfgfdg', 'sfd ddffdg', 'fdgdfgd', NULL, '2020-10-12 21:26:30', '2020-10-12 21:30:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` int(10) UNSIGNED NOT NULL,
  `bookid` int(11) NOT NULL,
  `custid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `visit_date` datetime DEFAULT NULL,
  `visit_date_2` datetime DEFAULT NULL,
  `visit_details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_configs`
--

CREATE TABLE `website_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `config_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `website_configs`
--

INSERT INTO `website_configs` (`id`, `config_name`, `config_value`, `config_file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'VAT_PERCENT', '5', '', '2020-08-23 19:45:37', '2020-08-23 19:45:37', NULL),
(2, 'COMPANY_EMAIL_ID', 'info@brakefiregcc.com', '', '2020-08-23 19:45:37', '2020-08-23 19:49:02', NULL),
(3, 'COMPANY_NAME', 'Brake Fire Safety & Security L.L.C.', '', '2020-08-23 19:51:41', '2020-08-23 19:51:41', NULL),
(4, 'COMPANY_Address', 'Ind Area, Ajman', '', '2020-08-23 19:52:17', '2020-08-23 19:52:17', NULL),
(5, 'CONTACT_NO', '+971551668286', '', '2020-08-23 19:53:01', '2020-08-23 19:53:01', NULL),
(6, 'INVOICE_FOLDER_PATH', 'D:\\\\server734\\\\htdocs\\\\projects\\\\quickadmin\\\\brakefire\\\\brakefire\\\\public\\\\invoice\\\\', NULL, NULL, NULL, NULL),
(7, 'DEFAULT_PROFIT', '40', NULL, '2020-08-28 09:24:14', '2020-08-28 09:24:14', NULL),
(8, 'OFFICE_ADDRESS_ENG', 'Ajman - Industrial Area / 1 Amman Street', NULL, '2020-09-12 11:02:37', '2020-09-12 11:02:37', NULL),
(9, 'OFFICE_ADDRESS_AR', 'عجمان - المنطقة الصناعية / 1 شارع عمان', NULL, '2020-09-12 11:03:23', '2020-09-12 11:03:23', NULL),
(10, 'FACEBOOKURL', 'https://www.facebook.com/', NULL, '2020-09-20 00:25:03', '2020-09-20 00:25:03', NULL),
(11, 'TWITTERURL', 'https://twitter.com/', NULL, '2020-09-20 00:25:55', '2020-09-20 00:25:55', NULL),
(12, 'GOOGLEURL', 'https://www.google.com/', NULL, '2020-09-20 00:26:29', '2020-09-20 00:26:29', NULL),
(13, 'LINKEDINURL', 'https://www.linkedin.com/', NULL, '2020-09-20 00:27:02', '2020-09-20 00:27:02', NULL),
(14, 'RSSURL', '/News-And-Events', NULL, '2020-09-20 00:27:02', '2020-09-26 10:28:12', NULL),
(15, 'INSTAGRAMURL', 'https://www.instagram.com/?hl=en', NULL, '2020-09-20 00:27:41', '2020-09-20 00:27:41', NULL),
(16, 'SMSURL', 'https://mshastra.com/sendurlcomma.aspx?user=20096531&pwd=xkpk6k&senderid=SMSAlert&priority=High&CountryCode=ALL', NULL, '2020-09-23 12:29:06', '2020-09-23 12:29:06', NULL),
(17, 'GOOGLE_CAPTCHA_PUBLICKEY', '6LdLTc8ZAAAAABQ8sIIKzL0rgMSkk9bcXlMD2J3m', NULL, '2020-09-23 19:51:07', '2020-09-23 19:51:07', NULL),
(18, 'GOOGLE_CAPTCHA_PRIVATEKEY', '6LdLTc8ZAAAAAMB8psTYWt_f40b8EvNQtNZGbRx6', NULL, '2020-09-23 19:51:43', '2020-09-23 19:51:43', NULL),
(19, 'COMPANY_EMAIL_ID_CC', 'info@brakefiregcc.com', NULL, '2020-09-23 21:37:08', '2020-09-23 21:37:08', NULL),
(20, 'BANNERMSG_ENG', 'Special Price on Installation of Monitoring Camera', NULL, '2020-09-26 09:36:12', '2020-09-26 09:36:12', NULL),
(21, 'BANNERMSG_ARB', 'سعر خاص لتركيب كاميرات المراقبة', NULL, '2020-09-26 09:36:39', '2020-09-26 09:36:39', NULL),
(22, 'BANNERTITLE_ENG', 'Amazing Limited Time Offer', NULL, '2020-09-26 09:38:17', '2020-09-26 09:38:17', NULL),
(23, 'BANNERTITLE_ARB', 'عرض مدهش لفترة محدودة', NULL, '2020-09-26 09:39:06', '2020-09-26 09:39:06', NULL),
(24, 'BANNERVISIBILITY', 'active', NULL, '2020-09-26 11:01:12', '2020-09-26 11:06:45', NULL),
(25, 'MATERIAL_REQUEST_PREFIX', 'BF-RN-MR-', NULL, '2020-09-26 20:14:44', '2020-09-26 20:14:44', NULL),
(26, 'COMPANY_NAME_ARB', 'الفرامل من الحرائق والسلامة والأمن ذ.', NULL, '2020-09-27 21:19:14', '2020-09-27 21:19:14', NULL),
(27, 'POBOX', 'P.O. Box : 45599', NULL, '2020-09-28 11:13:14', '2020-09-28 11:13:14', NULL),
(28, 'HQADDRESS', 'HQ. AJMAN - UAE', NULL, '2020-09-28 11:13:14', '2020-09-28 11:17:10', NULL),
(29, 'FAX_NO', '+971551668286', NULL, '2020-09-28 11:19:40', '2020-09-28 11:19:40', NULL),
(30, 'WEBSITE_NAME', 'www.brakefireae.com', NULL, '2020-09-28 11:38:14', '2020-09-28 11:38:14', NULL),
(31, 'BOQPREFIX', 'BF-BOQ-', NULL, '2020-09-30 19:40:41', '2020-10-01 13:59:53', NULL),
(32, 'ENQUIRY', 'BF-INQ-', NULL, '2020-10-01 14:00:59', '2020-10-01 14:00:59', NULL),
(33, 'COMPLAIN', 'BF-CMP-', NULL, '2020-10-01 14:01:49', '2020-10-01 14:01:49', NULL),
(34, 'CARRIER', 'BF-CR-', NULL, '2020-10-01 14:02:45', '2020-10-01 14:02:45', NULL),
(35, 'CONTACTUS', 'BF-CONT-', NULL, '2020-10-01 17:00:46', '2020-10-01 17:00:46', NULL),
(36, 'HEADERBANNER', '<figure class=\"image\"><img src=\"http://brakefireae.com/safedia/images/resource/thumb-3.png\" alt=\"\"></figure><span class=\"label\"><img src=\"http://brakefireae.com/safedia/images/icons/free-label.png\" alt=\"\" style=\"display:none;\"></span>', NULL, '2020-10-01 21:41:37', '2020-10-01 21:45:15', NULL),
(37, 'BOQPREFIX_STARTFROM', '30', NULL, '2020-10-01 22:19:27', '2020-10-12 21:42:02', NULL),
(38, 'ENQUIRY_STARTFROM', '11', NULL, '2020-10-01 22:19:52', '2020-10-04 01:23:14', NULL),
(39, 'COMPLAIN_STARTFROM', '7', NULL, '2020-10-01 22:20:11', '2020-10-12 21:26:29', NULL),
(40, 'CARRIER_STARTFROM', '1', NULL, '2020-10-01 22:20:53', '2020-10-01 22:20:53', NULL),
(41, 'CONTACTUS_STARTFROM', '1', NULL, '2020-10-01 22:21:14', '2020-10-01 22:21:14', NULL),
(42, 'INSPECTION_REPORT', 'BF-INS-REP-', NULL, '2020-10-02 06:25:13', '2020-10-02 06:25:13', NULL),
(43, 'INSPECTION_REPORT_STARTFROM', '1', NULL, '2020-10-02 06:25:46', '2020-10-02 06:25:46', NULL),
(44, 'JOBCODE_PREFIX', 'BF-JS-', NULL, '2020-10-08 11:47:52', '2020-10-08 11:47:52', NULL),
(45, 'JOBCODE_STARTFRM', '11', NULL, '2020-10-08 11:48:12', '2020-10-17 01:28:52', NULL),
(46, 'ProFormaInvoice', 'PRO-INV-', NULL, '2020-10-08 12:11:47', '2020-10-08 12:11:47', NULL),
(47, 'ProFormaInvoice_Startfrm', '15', NULL, '2020-10-08 12:12:20', '2020-10-08 12:12:20', NULL),
(48, 'PROJECTCODE', 'BF-PJ-', NULL, '2020-10-08 12:20:43', '2020-10-08 12:20:43', NULL),
(49, 'PROJECTCODE_STARTFRM', '123', NULL, '2020-10-08 12:21:17', '2020-10-08 12:33:21', NULL),
(50, 'LABOURCOSTPERHOUR', '50', NULL, '2020-10-11 09:50:46', '2020-10-11 09:50:46', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ac_installations`
--
ALTER TABLE `ac_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amc`
--
ALTER TABLE `amc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `amc_amc_cost_unique` (`amc_cost`);

--
-- Indexes for table `approve_drawings`
--
ALTER TABLE `approve_drawings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `approve_drawing_details`
--
ALTER TABLE `approve_drawing_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boqdetails`
--
ALTER TABLE `boqdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boqdetailsprod`
--
ALTER TABLE `boqdetailsprod`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boqdetailstype`
--
ALTER TABLE `boqdetailstype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boqtype`
--
ALTER TABLE `boqtype`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `boqtype_name_eng_unique` (`name_eng`),
  ADD UNIQUE KEY `boqtype_name_arabic_unique` (`name_arabic`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_category_name_unique` (`category_name`);

--
-- Indexes for table `categoriesbk`
--
ALTER TABLE `categoriesbk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_category_name_unique` (`category_name`);

--
-- Indexes for table `cctv_installations`
--
ALTER TABLE `cctv_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cleaning_installations`
--
ALTER TABLE `cleaning_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complains`
--
ALTER TABLE `complains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactuses`
--
ALTER TABLE `contactuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_management_systems`
--
ALTER TABLE `content_management_systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `electronics_installations`
--
ALTER TABLE `electronics_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees_title`
--
ALTER TABLE `employees_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimations`
--
ALTER TABLE `estimations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimationsizes`
--
ALTER TABLE `estimationsizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fire_installations`
--
ALTER TABLE `fire_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fire_maintenances`
--
ALTER TABLE `fire_maintenances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fire_supplies`
--
ALTER TABLE `fire_supplies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gas_installations`
--
ALTER TABLE `gas_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `g_maintinance_installations`
--
ALTER TABLE `g_maintinance_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_products`
--
ALTER TABLE `inspection_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_reports`
--
ALTER TABLE `inspection_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_report_products`
--
ALTER TABLE `inspection_report_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_types`
--
ALTER TABLE `inspection_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobcode`
--
ALTER TABLE `jobcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobcode_details`
--
ALTER TABLE `jobcode_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labourcost`
--
ALTER TABLE `labourcost`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `labourcost_labour_cost_unique` (`labour_cost`);

--
-- Indexes for table `labour_hours`
--
ALTER TABLE `labour_hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_requests`
--
ALTER TABLE `material_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `network_installations`
--
ALTER TABLE `network_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_partners`
--
ALTER TABLE `our_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_projects`
--
ALTER TABLE `our_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_services`
--
ALTER TABLE `our_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_modules`
--
ALTER TABLE `payment_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions_bk`
--
ALTER TABLE `permissions_bk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_1801366` (`role_id`),
  ADD KEY `permission_id_fk_1801366` (`permission_id`);

--
-- Indexes for table `profit`
--
ALTER TABLE `profit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profit_profit_unique` (`profit`);

--
-- Indexes for table `pro_forma_invoices`
--
ALTER TABLE `pro_forma_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pro_forma_invoice_items`
--
ALTER TABLE `pro_forma_invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_1801375` (`user_id`),
  ADD KEY `role_id_fk_1801375` (`role_id`);

--
-- Indexes for table `sanatization_installations`
--
ALTER TABLE `sanatization_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicetype`
--
ALTER TABLE `servicetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_type`
--
ALTER TABLE `service_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_tbl`
--
ALTER TABLE `status_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telecom_installations`
--
ALTER TABLE `telecom_installations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uae_location_lists`
--
ALTER TABLE `uae_location_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_complains`
--
ALTER TABLE `user_complains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `website_configs`
--
ALTER TABLE `website_configs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ac_installations`
--
ALTER TABLE `ac_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `amc`
--
ALTER TABLE `amc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `approve_drawings`
--
ALTER TABLE `approve_drawings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `approve_drawing_details`
--
ALTER TABLE `approve_drawing_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `boqdetails`
--
ALTER TABLE `boqdetails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `boqdetailsprod`
--
ALTER TABLE `boqdetailsprod`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `boqdetailstype`
--
ALTER TABLE `boqdetailstype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `boqtype`
--
ALTER TABLE `boqtype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `categoriesbk`
--
ALTER TABLE `categoriesbk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cctv_installations`
--
ALTER TABLE `cctv_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cleaning_installations`
--
ALTER TABLE `cleaning_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complains`
--
ALTER TABLE `complains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contactuses`
--
ALTER TABLE `contactuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `content_management_systems`
--
ALTER TABLE `content_management_systems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `electronics_installations`
--
ALTER TABLE `electronics_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employees_title`
--
ALTER TABLE `employees_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `estimations`
--
ALTER TABLE `estimations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `estimationsizes`
--
ALTER TABLE `estimationsizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fire_installations`
--
ALTER TABLE `fire_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fire_maintenances`
--
ALTER TABLE `fire_maintenances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fire_supplies`
--
ALTER TABLE `fire_supplies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gas_installations`
--
ALTER TABLE `gas_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `g_maintinance_installations`
--
ALTER TABLE `g_maintinance_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inspection_products`
--
ALTER TABLE `inspection_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `inspection_reports`
--
ALTER TABLE `inspection_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `inspection_report_products`
--
ALTER TABLE `inspection_report_products`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `inspection_types`
--
ALTER TABLE `inspection_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `jobcode`
--
ALTER TABLE `jobcode`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jobcode_details`
--
ALTER TABLE `jobcode_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `labourcost`
--
ALTER TABLE `labourcost`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `labour_hours`
--
ALTER TABLE `labour_hours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `material_requests`
--
ALTER TABLE `material_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `network_installations`
--
ALTER TABLE `network_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `our_partners`
--
ALTER TABLE `our_partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `our_projects`
--
ALTER TABLE `our_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `our_services`
--
ALTER TABLE `our_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payment_modules`
--
ALTER TABLE `payment_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;

--
-- AUTO_INCREMENT for table `permissions_bk`
--
ALTER TABLE `permissions_bk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT for table `profit`
--
ALTER TABLE `profit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pro_forma_invoices`
--
ALTER TABLE `pro_forma_invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pro_forma_invoice_items`
--
ALTER TABLE `pro_forma_invoice_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sanatization_installations`
--
ALTER TABLE `sanatization_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `servicetype`
--
ALTER TABLE `servicetype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `service_type`
--
ALTER TABLE `service_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `status_tbl`
--
ALTER TABLE `status_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `telecom_installations`
--
ALTER TABLE `telecom_installations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `uae_location_lists`
--
ALTER TABLE `uae_location_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=482;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user_complains`
--
ALTER TABLE `user_complains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `website_configs`
--
ALTER TABLE `website_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_id_fk_1801366` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_id_fk_1801366` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_id_fk_1801375` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_fk_1801375` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
