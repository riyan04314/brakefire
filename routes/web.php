<?php
//use URL;
Auth::routes();
Route::get('/', 'CmsController@index')->name('homepage');
Route::get('/home', 'CmsController@index')->name('home');
Route::get('/Search-Result/', 'CmsController@searchresult')->name('searchresult');
Route::get('contact-us', 'CmsController@contactus')->name('contactus');
Route::post('contact-us', 'CmsController@contactus')->name('contactus_sbmt');
Route::any('/carrier', 'HomepageController@carrier')->name('carrier');
Route::any('/bitlychk', 'CmsController@bitlychk')->name('bitlychk');

Route::any('/Our-Vision', 'CmsController@cmspage')->name('ourvision');
Route::any('/Our-Message', 'CmsController@cmspage')->name('ourmessage');
Route::any('/Elements-of-Distinction', 'CmsController@cmspage')->name('elementsofdistinction');
Route::any('/Downloads', 'CmsController@cmspage')->name('downloads');
Route::any('/Certification', 'CmsController@cmspage')->name('certification');
Route::any('/Services', 'CmsController@cmspage')->name('services');
Route::any('/Our-Services', 'CmsController@cmspage')->name('ourservices');
Route::any('/News-And-Events', 'CmsController@cmspage')->name('newsandevents');

Route::any('prodList/{service_type?}', 'HomepageController@prodList')->name('prodList');
///*******/
	Route::get('estimationsize/{id}/{sid?}', 'HomepageController@estimationsize')->name('estimationsize');
	Route::get('emiratescitylist/{emiratesid}', 'HomepageController@emiratescitylist')->name('emiratescitylist');

	Route::get('/complain', 'HomepageController@complain')->name('complain');
	Route::post('/complain', 'HomepageController@complain_submit')->name('complain_submit');
	
Route::group(['middleware' => ['auth']], function () {	});
	Route::get('/inquiry', 'HomepageController@inquiry')->name('inquiry');
	Route::post('/inquiry', 'HomepageController@inquiry_submit')->name('inquiry_submit');	

///*******/	


Route::get('/lang', function(){
		$locale = App::getLocale();
		if($locale == 'en'){
			App::setLocale('ar');
			echo 'AR';exit;
		}
		else{
			App::setLocale('en');
		}
		return redirect()->back();
	})->name('lang');


Route::group(['middleware' => ['auth']], function () {
	Route::any('/quotationDetails', 'HomepageController@quotationdetails')->name('quotationdetails');
	//Route::get('/inquiry', 'HomepageController@inquiry')->name('inquiry');
	//Route::post('/inquiry', 'HomepageController@inquiry_submit')->name('inquiry_submit');
	
	Route::get('/inspectionfrm/{id}', 'HomepageController@inspection_form')->name('inspection_form');
	Route::get('/inspectionForm/{id}', 'HomepageController@inspection_form')->name('inspectionForm');
	Route::post('/inspectionForm/{id}', 'HomepageController@inspection_form_submit')->name('inspectionFormSubmit');
	
	Route::get('getJobcodePrice/{id?}', 'HomepageController@getJobcodePrice')->name('getJobcodePrice');
	Route::get('/boqForm', 'HomepageController@boq_form')->name('boq_form');
	Route::post('/boqForm', 'HomepageController@boq_form_submit')->name('boq_form_submit');
	
	Route::get('boq/{id}/edit', 'HomepageController@boqEdit')->name('boq.edit');
	Route::delete('boq/destroy/{id}', 'HomepageController@massDestroy')->name('boq.destroy');
	
	Route::post('boqForm/media', 'HomepageController@storeMedia')->name('boqForm.storeMedia');
    Route::post('boqForm/ckmedia', 'HomepageController@storeCKEditorImages')->name('boqForm.storeCKEditorImages'); 	
	Route::post('inspectionForm/media', 'HomepageController@storeMedia')->name('inspectionForm.storeMedia');
    Route::post('inspectionForm/ckmedia', 'HomepageController@storeCKEditorImages')->name('inspectionForm.storeCKEditorImages'); 
});


/*

//Auth::routes(['register' => false]);
// Admin

//Route::redirect('/', '/login');


Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }
    return redirect()->route('admin.home');
});
*/


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
	Route::get('/dashbd', 'HomeController@index')->name('dashbd');
	Route::get('/BoqDetails/{id}', 'BoqController@BoqDetails')->name('BoqDetails');
	
	Route::get('/estimationList/{id}/{sid?}', 'EstimationsController@categoryEstimationList')->name('estimationList');
	
	Route::get('/customer', 'UsersController@customer')->name('customer');

    // Profits
    Route::delete('profits/destroy', 'ProfitController@massDestroy')->name('profits.massDestroy');
    Route::resource('profits', 'ProfitController');	
    // Banners
    Route::delete('banners/destroy', 'BannerController@massDestroy')->name('banners.massDestroy');
    Route::post('banners/media', 'BannerController@storeMedia')->name('banners.storeMedia');
    Route::post('banners/ckmedia', 'BannerController@storeCKEditorImages')->name('banners.storeCKEditorImages');
    Route::resource('banners', 'BannerController');	
    // Services
    Route::delete('services/destroy', 'ServicesController@massDestroy')->name('services.massDestroy');
    Route::post('services/media', 'ServicesController@storeMedia')->name('services.storeMedia');
    Route::post('services/ckmedia', 'ServicesController@storeCKEditorImages')->name('services.storeCKEditorImages');
    Route::resource('services', 'ServicesController');

    // Certificates
    Route::delete('certificates/destroy', 'CertificateController@massDestroy')->name('certificates.massDestroy');
    Route::post('certificates/media', 'CertificateController@storeMedia')->name('certificates.storeMedia');
    Route::post('certificates/ckmedia', 'CertificateController@storeCKEditorImages')->name('certificates.storeCKEditorImages');
    Route::resource('certificates', 'CertificateController');

    // Downloads
    Route::delete('downloads/destroy', 'DownloadsController@massDestroy')->name('downloads.massDestroy');
    Route::post('downloads/media', 'DownloadsController@storeMedia')->name('downloads.storeMedia');
    Route::post('downloads/ckmedia', 'DownloadsController@storeCKEditorImages')->name('downloads.storeCKEditorImages');
    Route::resource('downloads', 'DownloadsController');

    // Website Configs
    Route::delete('website-configs/destroy', 'WebsiteConfigController@massDestroy')->name('website-configs.massDestroy');
    Route::resource('website-configs', 'WebsiteConfigController');	
	
	
     // Servicetype
	Route::get('jobCode', 'BoqController@jobCode')->name('boq.jobCode');
	Route::get('boq/{id}/invoice', 'BoqController@boqInvoice')->name('boq.invoice');
	Route::delete('boq/destroy', 'BoqController@massDestroy')->name('boq.massDestroy');
	Route::get('boq/{id}/sentQuotation', 'BoqController@boqInvoice')->name('boq.sentQuotation');	
    Route::resource('boq', 'BoqController');
    Route::resource('quotation', 'BoqController');


	
    // Categories
    Route::delete('categories/destroy', 'CategoryController@massDestroy')->name('categories.massDestroy');
    Route::resource('categories', 'CategoryController');
	
    // Content Management Systems
    Route::delete('content-management-systems/destroy', 'ContentManagementSystemController@massDestroy')->name('content-management-systems.massDestroy');
    Route::post('content-management-systems/media', 'ContentManagementSystemController@storeMedia')->name('content-management-systems.storeMedia');
    Route::post('content-management-systems/ckmedia', 'ContentManagementSystemController@storeCKEditorImages')->name('content-management-systems.storeCKEditorImages');
    Route::resource('content-management-systems', 'ContentManagementSystemController');
	
     // Servicetype
    Route::delete('servicetype/destroy', 'ServiceTypeController@massDestroy')->name('servicetype.massDestroy');
    Route::resource('servicetype', 'ServiceTypeController');
 
    // Brands
    Route::delete('brands/destroy', 'BrandsController@massDestroy')->name('brands.massDestroy');
    Route::resource('brands', 'BrandsController');
    // Estimations
    Route::delete('estimations/destroy', 'EstimationsController@massDestroy')->name('estimations.massDestroy');
    Route::resource('estimations', 'EstimationsController');
    // Estimationsizes
    Route::delete('estimationsizes/destroy', 'EstimationsizesController@massDestroy')->name('estimationsizes.massDestroy');
    Route::resource('estimationsizes', 'EstimationsizesController');
    // Materials
    Route::get('materials/branded/create', 'MaterialsController@create')->name('materials.branded.create');
    Route::get('materials/nonBranded/create', 'MaterialsController@create')->name('materials.nonBranded.create');
    Route::get('materials/branded/{brand?}', 'MaterialsController@index')->name('materials.branded');
    Route::get('materials/nonBranded', 'MaterialsController@index')->name('materials.nonBranded');
    Route::delete('materials/destroy', 'MaterialsController@massDestroy')->name('materials.massDestroy');
    Route::resource('materials', 'MaterialsController');

	

	// Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Fire Installations
    Route::delete('fire-installations/destroy', 'FireInstallationController@massDestroy')->name('fire-installations.massDestroy');
    Route::post('fire-installations/media', 'FireInstallationController@storeMedia')->name('fire-installations.storeMedia');
    Route::post('fire-installations/ckmedia', 'FireInstallationController@storeCKEditorImages')->name('fire-installations.storeCKEditorImages');
    Route::resource('fire-installations', 'FireInstallationController');

    // Fire Maintenances
    Route::delete('fire-maintenances/destroy', 'FireMaintenanceController@massDestroy')->name('fire-maintenances.massDestroy');
    Route::post('fire-maintenances/media', 'FireMaintenanceController@storeMedia')->name('fire-maintenances.storeMedia');
    Route::post('fire-maintenances/ckmedia', 'FireMaintenanceController@storeCKEditorImages')->name('fire-maintenances.storeCKEditorImages');
    Route::resource('fire-maintenances', 'FireMaintenanceController');

    // Fire Supplies
    Route::delete('fire-supplies/destroy', 'FireSupplyController@massDestroy')->name('fire-supplies.massDestroy');
    Route::post('fire-supplies/media', 'FireSupplyController@storeMedia')->name('fire-supplies.storeMedia');
    Route::post('fire-supplies/ckmedia', 'FireSupplyController@storeCKEditorImages')->name('fire-supplies.storeCKEditorImages');
    Route::resource('fire-supplies', 'FireSupplyController');

    // Enquiries
    Route::post('enquiries/estimator', 'EnquiryController@assignEstimator')->name('enquiries.estimator');
    Route::post('enquiries/enq_reply', 'EnquiryController@enqReply')->name('enquiries.enq_reply');
    Route::delete('enquiries/destroy', 'EnquiryController@massDestroy')->name('enquiries.massDestroy');
    Route::post('enquiries/media', 'EnquiryController@storeMedia')->name('enquiries.storeMedia');
    Route::post('enquiries/ckmedia', 'EnquiryController@storeCKEditorImages')->name('enquiries.storeCKEditorImages');
    Route::get('enquiries/{serviceType?}', 'EnquiryController@index')->name('enquiries.serviceType');
	Route::resource('enquiries', 'EnquiryController');
	
	
	
    // Bookings
    Route::delete('bookings/destroy', 'BookingController@massDestroy')->name('bookings.massDestroy');
    Route::resource('bookings', 'BookingController');

    // Visits
    Route::delete('visits/destroy', 'VisitController@massDestroy')->name('visits.massDestroy');
    Route::post('visits/media', 'VisitController@storeMedia')->name('visits.storeMedia');
    Route::post('visits/ckmedia', 'VisitController@storeCKEditorImages')->name('visits.storeCKEditorImages');
    Route::resource('visits', 'VisitController');

    // Complains
    Route::delete('complains/destroy', 'ComplainController@massDestroy')->name('complains.massDestroy');
    Route::post('complains/media', 'ComplainController@storeMedia')->name('complains.storeMedia');
    Route::post('complains/ckmedia', 'ComplainController@storeCKEditorImages')->name('complains.storeCKEditorImages');
    Route::resource('complains', 'ComplainController');

    // Employees
    Route::delete('employees/destroy', 'EmployeeController@massDestroy')->name('employees.massDestroy');
    Route::post('employees/media', 'EmployeeController@storeMedia')->name('employees.storeMedia');
    Route::post('employees/ckmedia', 'EmployeeController@storeCKEditorImages')->name('employees.storeCKEditorImages');
    Route::resource('employees', 'EmployeeController');

    // Customers
    Route::delete('customers/destroy', 'CustomerController@massDestroy')->name('customers.massDestroy');
    Route::resource('customers', 'CustomerController');

    // Cctv Installations
    Route::delete('cctv-installations/destroy', 'CctvInstallationController@massDestroy')->name('cctv-installations.massDestroy');
    Route::post('cctv-installations/media', 'CctvInstallationController@storeMedia')->name('cctv-installations.storeMedia');
    Route::post('cctv-installations/ckmedia', 'CctvInstallationController@storeCKEditorImages')->name('cctv-installations.storeCKEditorImages');
    Route::resource('cctv-installations', 'CctvInstallationController');

    // Ac Installations
    Route::delete('ac-installations/destroy', 'AcInstallationController@massDestroy')->name('ac-installations.massDestroy');
    Route::post('ac-installations/media', 'AcInstallationController@storeMedia')->name('ac-installations.storeMedia');
    Route::post('ac-installations/ckmedia', 'AcInstallationController@storeCKEditorImages')->name('ac-installations.storeCKEditorImages');
    Route::resource('ac-installations', 'AcInstallationController');

    // Telecom Installations
    Route::delete('telecom-installations/destroy', 'TelecomInstallationController@massDestroy')->name('telecom-installations.massDestroy');
    Route::post('telecom-installations/media', 'TelecomInstallationController@storeMedia')->name('telecom-installations.storeMedia');
    Route::post('telecom-installations/ckmedia', 'TelecomInstallationController@storeCKEditorImages')->name('telecom-installations.storeCKEditorImages');
    Route::resource('telecom-installations', 'TelecomInstallationController');

    // Gas Installations
    Route::delete('gas-installations/destroy', 'GasInstallationController@massDestroy')->name('gas-installations.massDestroy');
    Route::post('gas-installations/media', 'GasInstallationController@storeMedia')->name('gas-installations.storeMedia');
    Route::post('gas-installations/ckmedia', 'GasInstallationController@storeCKEditorImages')->name('gas-installations.storeCKEditorImages');
    Route::resource('gas-installations', 'GasInstallationController');

    // Network Installations
    Route::delete('network-installations/destroy', 'NetworkInstallationController@massDestroy')->name('network-installations.massDestroy');
    Route::post('network-installations/media', 'NetworkInstallationController@storeMedia')->name('network-installations.storeMedia');
    Route::post('network-installations/ckmedia', 'NetworkInstallationController@storeCKEditorImages')->name('network-installations.storeCKEditorImages');
    Route::resource('network-installations', 'NetworkInstallationController');

    // Electronics Installations
    Route::delete('electronics-installations/destroy', 'ElectronicsInstallationController@massDestroy')->name('electronics-installations.massDestroy');
    Route::post('electronics-installations/media', 'ElectronicsInstallationController@storeMedia')->name('electronics-installations.storeMedia');
    Route::post('electronics-installations/ckmedia', 'ElectronicsInstallationController@storeCKEditorImages')->name('electronics-installations.storeCKEditorImages');
    Route::resource('electronics-installations', 'ElectronicsInstallationController');

    // G Maintinance Installations
    Route::delete('g-maintinance-installations/destroy', 'GMaintinanceInstallationController@massDestroy')->name('g-maintinance-installations.massDestroy');
    Route::post('g-maintinance-installations/media', 'GMaintinanceInstallationController@storeMedia')->name('g-maintinance-installations.storeMedia');
    Route::post('g-maintinance-installations/ckmedia', 'GMaintinanceInstallationController@storeCKEditorImages')->name('g-maintinance-installations.storeCKEditorImages');
    Route::resource('g-maintinance-installations', 'GMaintinanceInstallationController');

    // Sanatization Installations
    Route::delete('sanatization-installations/destroy', 'SanatizationInstallationController@massDestroy')->name('sanatization-installations.massDestroy');
    Route::post('sanatization-installations/media', 'SanatizationInstallationController@storeMedia')->name('sanatization-installations.storeMedia');
    Route::post('sanatization-installations/ckmedia', 'SanatizationInstallationController@storeCKEditorImages')->name('sanatization-installations.storeCKEditorImages');
    Route::resource('sanatization-installations', 'SanatizationInstallationController');

    // Cleaning Installations
    Route::delete('cleaning-installations/destroy', 'CleaningInstallationController@massDestroy')->name('cleaning-installations.massDestroy');
    Route::post('cleaning-installations/media', 'CleaningInstallationController@storeMedia')->name('cleaning-installations.storeMedia');
    Route::post('cleaning-installations/ckmedia', 'CleaningInstallationController@storeCKEditorImages')->name('cleaning-installations.storeCKEditorImages');
    Route::resource('cleaning-installations', 'CleaningInstallationController');
	
	
    // Events
    Route::delete('events/destroy', 'EventController@massDestroy')->name('events.massDestroy');
    Route::post('events/media', 'EventController@storeMedia')->name('events.storeMedia');
    Route::post('events/ckmedia', 'EventController@storeCKEditorImages')->name('events.storeCKEditorImages');
    Route::resource('events', 'EventController');

    // Teams
    Route::delete('teams/destroy', 'TeamController@massDestroy')->name('teams.massDestroy');
    Route::post('teams/media', 'TeamController@storeMedia')->name('teams.storeMedia');
    Route::post('teams/ckmedia', 'TeamController@storeCKEditorImages')->name('teams.storeCKEditorImages');
    Route::resource('teams', 'TeamController');	
	
    // Inspection Types
    Route::get('inspection-category-product/{category_id}', 'InspectionTypeController@categoryProduct')->name('inspectionProductList');
	Route::post('inspection-category-product/{category_id}', 'InspectionTypeController@categoryProductUpdate')->name('inspectionProductUpdate');
	Route::get('inspection-category', 'InspectionTypeController@index')->name('inspectionCategory');
	Route::delete('inspection-types/destroy', 'InspectionTypeController@massDestroy')->name('inspection-types.massDestroy');
    Route::resource('inspection-types', 'InspectionTypeController');
	
    // Inspection Reports
    Route::delete('inspection-reports/destroy', 'InspectionReportController@massDestroy')->name('inspection-reports.massDestroy');
    Route::resource('inspection-reports', 'InspectionReportController');	
	
    // Job Codes
    Route::delete('job-codes/destroy', 'JobCodeController@massDestroy')->name('job-codes.massDestroy');
    Route::resource('job-codes', 'JobCodeController');

    // Jobcode Details
	Route::get('/jobCodeDetails/{id}', 'JobcodeDetailsController@jobCodeDetails')->name('jobCodeDetails');
    Route::delete('jobcode-details/destroy', 'JobcodeDetailsController@massDestroy')->name('jobcode-details.massDestroy');
    Route::post('jobcodedetails/media', 'JobcodeDetailsController@storeMedia')->name('jobcodedetails.storeMedia');
    Route::post('jobcodedetails/ckmedia', 'JobcodeDetailsController@storeCKEditorImages')->name('jobcodedetails.storeCKEditorImages');    
	Route::resource('jobcode-details', 'JobcodeDetailsController');	

    // Payment Modules
    Route::delete('payment-modules/destroy', 'PaymentModuleController@massDestroy')->name('payment-modules.massDestroy');
    Route::resource('payment-modules', 'PaymentModuleController');	
	
    // Contactuses
	Route::get('contactuses/{id}/editPage', 'ContactUsController@editContactus')->name('contactuses_edit');
    Route::put('contactuses/{id}/updatePage', 'ContactUsController@updateContactus')->name('contactuses_update');
    Route::delete('contactuses/destroy', 'ContactUsController@massDestroy')->name('contactuses.massDestroy');
    Route::post('contactuses/media', 'ContactUsController@storeMedia')->name('contactuses.storeMedia');
    Route::post('contactuses/ckmedia', 'ContactUsController@storeCKEditorImages')->name('contactuses.storeCKEditorImages');
    Route::resource('contactuses', 'ContactUsController');
	
    // Pro Forma Invoices
    Route::get('send-pro-forma-invoice/pdf/{id}', 'ProFormaInvoiceController@sendProFormaInvoicePDF')->name('sendProFormaInvoicePDF');
	Route::get('pro-forma-invoices/pdf/{id}', 'ProFormaInvoiceController@InvoicePDF')->name('ProFormaInvoicePDF');
	Route::delete('pro-forma-invoices/destroy', 'ProFormaInvoiceController@massDestroy')->name('pro-forma-invoices.massDestroy');
    Route::resource('pro-forma-invoices', 'ProFormaInvoiceController');	
	
	
    // Approve Drawings
    Route::delete('approve-drawings/destroy', 'ApproveDrawingController@massDestroy')->name('approve-drawings.massDestroy');
    Route::resource('approve-drawings', 'ApproveDrawingController');

    // Drawing Details
    Route::delete('drawing-details/destroy', 'DrawingDetailsController@massDestroy')->name('drawing-details.massDestroy');
    Route::resource('drawing-details', 'DrawingDetailsController');

    // Material Requests
	Route::get('material-list/{jobcodeid}','MaterialRequestController@jobcodesMaterialList')->name('materialist');
    Route::delete('material-requests/destroy', 'MaterialRequestController@massDestroy')->name('material-requests.massDestroy');
    Route::resource('material-requests', 'MaterialRequestController');

    // Email Templates
    Route::delete('email-templates/destroy', 'EmailTemplateController@massDestroy')->name('email-templates.massDestroy');
    Route::post('email-templates/media', 'EmailTemplateController@storeMedia')->name('email-templates.storeMedia');
    Route::post('email-templates/ckmedia', 'EmailTemplateController@storeCKEditorImages')->name('email-templates.storeCKEditorImages');
    Route::resource('email-templates', 'EmailTemplateController');	
	
	Route::get('jobCode/BoqDetails/{id}', 'JobCodeController@jobCodeBoqDetails')->name('jobCodeBoqDetails');
    // Uae Location Lists
    
	Route::get('uae-location-lists/create', 'UaeLocationListController@create')->name('uae-location-lists.create');
	Route::get('uae-location-lists/{emiratesid}', 'UaeLocationListController@index')->name('uae-location-lists.listing');
	Route::delete('uae-location-lists/destroy', 'UaeLocationListController@massDestroy')->name('uae-location-lists.massDestroy');
    Route::resource('uae-location-lists', 'UaeLocationListController');	
	
    // Testimonials
    Route::delete('testimonials/destroy', 'TestimonialsController@massDestroy')->name('testimonials.massDestroy');
    Route::resource('testimonials', 'TestimonialsController');	
	
	
    // Our Services
    Route::delete('our-services/destroy', 'OurServicesController@massDestroy')->name('our-services.massDestroy');
    Route::post('our-services/media', 'OurServicesController@storeMedia')->name('our-services.storeMedia');
    Route::post('our-services/ckmedia', 'OurServicesController@storeCKEditorImages')->name('our-services.storeCKEditorImages');
    Route::resource('our-services', 'OurServicesController');

    // Our Projects
    Route::delete('our-projects/destroy', 'OurProjectsController@massDestroy')->name('our-projects.massDestroy');
    Route::resource('our-projects', 'OurProjectsController');
    // Our Projects
    Route::delete('carriers/destroy', 'CarrierController@massDestroy')->name('carriers.massDestroy');
    Route::resource('carriers', 'CarrierController');

    // Our Partners
    Route::delete('our-partners/destroy', 'OurPartnersController@massDestroy')->name('our-partners.massDestroy');
    Route::post('our-partners/media', 'OurPartnersController@storeMedia')->name('our-partners.storeMedia');
    Route::post('our-partners/ckmedia', 'OurPartnersController@storeCKEditorImages')->name('our-partners.storeCKEditorImages');
    Route::resource('our-partners', 'OurPartnersController');	
	
    // Sms
    Route::delete('sms/destroy', 'SmsController@massDestroy')->name('sms.massDestroy');
    Route::resource('sms', 'SmsController');	
	
    // User Complains
	Route::post('user-complains/estimator', 'UserComplainController@assignEstimator')->name('user-complains.estimator');
    Route::delete('user-complains/destroy', 'UserComplainController@massDestroy')->name('user-complains.massDestroy');
    Route::post('user-complains/media', 'UserComplainController@storeMedia')->name('user-complains.storeMedia');
    Route::post('user-complains/ckmedia', 'UserComplainController@storeCKEditorImages')->name('user-complains.storeCKEditorImages');
    Route::resource('user-complains', 'UserComplainController');	
	


    // Labour Hours
    Route::delete('labour-hours/destroy', 'LabourHourController@massDestroy')->name('labour-hours.massDestroy');
    Route::resource('labour-hours', 'LabourHourController');
	
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
